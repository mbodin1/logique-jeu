# Compiling

The prefered source is using `esy`.

## Using `esy`

```bash
# Installing esy
npm install esy
# Installing the dependencies and compiling
esy
```
The tests can be run with `esy test`.

## Using `opam`

```bash
opam install dune lwt_ppx js_of_ocaml js_of_ocaml-ppx js_of_ocaml-lwt tyxml tyxml-ppx js_of_ocaml-tyxml
dune build
```
The tests can be run with `dune runtest`.

# Running

## In the terminal

Once compiled, the console program is located at `_build/install/default/bin/logique-pirate.native`.
Option `--help` lists all possible options.

Example usage:
```bash
echo "True and False" | _build/install/default/bin/logique-pirate.native --no-symmetries --small-arcs 0.5 -o output.svg
```

## In the browser

Once built, the folder `_build/default/web` should contain a working `index.html`:

```bash
cd _build/default/web
python3 -m http.server 8000 > /dev/null &
firefox localhost:8000
```

# Formulas

The set of accepted formulas is meant to be a wild superset of subsets of LaTeX, Unicode, OCaml, and Coq notations.
The goal is to easily copy/paste formulas from different contexts.
The following formulas are for instance accepted:
- `True and (False or True)`
- `true && (false || true)`
- `~ (⊤ /\ ⊥) ⇔ \false ∧ ¬ \true`
- `forall x y, green (x) -> red (x)`
- `\neg \left\(\forall x y, Green \left(x\right) \longrightarrow \red{y}\right\)`
- `let A := True || False in A \/ ~ A`
- `let Inf x y = red x \/ (not red x /\ green y) \/ (blue x /\ blue y) in forall x y z, Inf x y -> Inf y z -> Inf x z`
- `Let Peirce A B be ((A → B) → A) → A in for all x, Peirce (Green x) False`

The full grammar can be found in the files [parser.mly](src/parser.mly) and [lexer.mll](src/lexer.mll).

