
open Js_of_ocaml
open Tyxml
open Js_of_ocaml_tyxml
open Tyxml_js
open Logique_pirate

let _ = Random.self_init ()

(* Appends in the provided node as the first child of elem. *)
let dom_insert_first elem node =
  Dom.insertBefore elem node elem##.firstChild

(* Given a string meant to be displayed for human, convert it into a name suitable for
  an HTML idenfier. *)
let html_name n =
  let char_list s = List.init (String.length s) (fun i -> s.[i]) in
  String.concat "" (List.map (function
      | ' ' | '_' -> "_"
      | c when (c >= 'a' && c <= 'z')
            || (c >= 'A' && c <= 'Z')
            || (c >= '0' && c <= '9')
        -> String.make 1 c
      | '<' -> "__lt__"
      | '>' -> "__gt__"
      | c -> Printf.sprintf "__%i__" (Char.code c)
    ) (char_list n))

let form =
  To_dom.of_form [%html
    "<form class = 'island'>"
    "</form>"
  ]

let options =
  To_dom.of_details [%html
    "<details>"
      "<summary>Options</summary>"
    "</details>"
  ]

let options_buttons =
  List.map (fun o ->
    let name = html_name (Options.name o) in
    let input =
      if Options.default o then
        [%html "<input type = 'checkbox' id = "name" checked />" ]
      else
        [%html "<input type = 'checkbox' id = "name" />" ] in
    let%html switch =
      "<label class = 'switch' for = "name">"
        "<span class = 'slider'></span>"
      "</label>" in
    let%html button =
      "<p>"
        "<label for = "name">"[Html.txt (Options.name o)]"</label>"
      "</p>" in
    let b = To_dom.of_p button in
    let switch = To_dom.of_label switch in
    let input = To_dom.of_input input in
    dom_insert_first switch input ;
    dom_insert_first b switch ;
    Dom.appendChild options b ;
    (o, input)) Options.all

let settings_buttons =
  List.map (fun (o, v) ->
    let name = html_name o in
    let%html button =
      "<p>"
        "<label for = "name">"[Html.txt (o ^ ": ")]"</label>"
      "</p>" in
    let%html input =
      "<input type = 'number' step = '0.5' id = "name" value = "(Printf.sprintf "%g" v)" />" in
    let b = To_dom.of_p button in
    let input = To_dom.of_input input in
    Dom.appendChild b input ;
    Dom.appendChild options b ;
    (o, input)) Position.default_settings

let formula =
  let name = "input_formula" in
  let formula =
    To_dom.of_span [%html
      "<span>"
        "<label for = "name">Input formula:</label>"
      "</span>"
    ] in
  let textarea =
    To_dom.of_textarea [%html
      "<textarea id = "name">"
        "exists x, green(x) or (for all y, green(y))"
      "</textarea>"
    ] in
  Dom.appendChild formula textarea ;
  Dom.appendChild form formula ;
  Dom.appendChild form options ;
  textarea

let output =
  let span = To_dom.of_span [%html "<span class = 'output'></span>"] in
  span

(** Remove all the child of a node. *)
let rec clear_node n =
  match Js.Opt.to_option n##.firstChild with
  | Some c ->
    ignore (n##removeChild c) ;
    clear_node n
  | None -> ()

(* A blob containing the image to be downloaded, that should be revoked when clearing the output. *)
let url_to_revoke = ref []

let clear_output () =
  List.iter (fun url -> Dom_html.window##._URL##revokeObjectURL url) !url_to_revoke ;
  url_to_revoke := [] ;
  clear_node output

let write_output message =
  let message =
    To_dom.of_span [%html
      "<span class = 'message'>"[Html.txt message]"</span>"
    ] in
  Dom.appendChild output message

(* Provided the name of a file, its content and a text, create a button with this text
  to download this file. *)
let download_button contentType name content message =
  let blob = File.blob_from_string ~contentType content in
  let url = Dom_html.window##._URL##createObjectURL blob in
  url_to_revoke := url :: !url_to_revoke ;
  let url = Js.to_string url in
  let%html button =
    "<a href = "url" download = "(Some name)">"
      "<input type = 'button' value = "message" />"
    "</a>" in
  To_dom.of_a button

(* Call the Option.apply function with the right parameters. *)
let generate () =
  let options =
    List.filter_map (fun (o, b) ->
      let checked = b##.checked in
      let checked = Js.to_bool checked in
      if checked then
        Some o
      else None) options_buttons in
  let settings =
    List.map (fun (o, b) ->
      let value = Js.parseFloat b##.value in
      (o, value)) settings_buttons in
  let f = formula##.value in
  let f = Js.to_string f in
  let img = Options.apply ~position:settings options (Lexing.from_string f) in
  let module Convert_js = Image.Convert(Tyxml_js_xml)(Tyxml_js.Svg) in
  let%html displayed_img =
    "<span class = 'pond'>"
      [Html.svg [Convert_js.to_image img]]
    "</span>" in
  let displayed_img = To_dom.of_span displayed_img in
  Dom.appendChild output displayed_img ;
  let module Svg_fix = struct
    include Tyxml.Svg
    let a_href = a_xlink_href (* A fix to make the resulting files openable with Inkscape. *)
    end in
  let module Convert_txt = Image.Convert(Tyxml_xml)(Svg_fix) in
  let img_txt = Convert_txt.to_image img in
  let file = Format.asprintf "%a@." (Svg_fix.pp ~indent:true ()) img_txt in
  Dom.appendChild output (download_button "image/svg+xml" "pirates.svg" file "Télécharger l’image") ;
  let file = Image.to_tikz img in
  Dom.appendChild output (download_button "text/x-tex" "pirates.tex" file "Télécharger le code TikZ (beta)")

let () =
  let%html button =
    "<input type = 'button' value = 'Générer' />" in
  let button = To_dom.of_input button in
  Dom.appendChild form button ;
  Dom.appendChild form output ;
  button##.onclick := Dom_html.handler (fun _ ->
    clear_output () ;
    (try generate ()
     with e ->
       write_output (Printf.sprintf "An error occured: %s." (Printexc.to_string e))) ;
    Js._false)

(* We only add the form at the very end to avoid reflow. *)
let () =
  Dom.appendChild Dom_html.document##.body form

