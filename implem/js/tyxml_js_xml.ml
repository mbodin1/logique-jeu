(* This file aims at providing an iterable interface to Tyxml_xml. *)

open Js_of_ocaml
open Js_of_ocaml_tyxml

include Tyxml_js.Xml

type separator = Space | Comma

type attrib_k =
  | Event of event_handler
  | MouseEvent of mouse_event_handler
  | KeyboardEvent of keyboard_event_handler
  | TouchEvent of touch_event_handler
  | Attr of Js.js_string Js.t option React.S.t

(* This needs to be done within the original module. *)
let convert_attrib : attrib -> aname * attrib_k = Obj.magic
let convert_back_attrib : aname * attrib_k -> attrib = Obj.magic

let aname (a : attrib) =
  let (name, _k) = convert_attrib a in
  name

type acontent =
  | AFloat of float
  | AInt of int
  | AStr of string
  | AStrL of separator * string list

let acontent (a : attrib) =
  let (_name, k) = convert_attrib a in
  match k with
  | Attr a ->
    (match React.S.value a with
     | None -> AStr ""
     | Some str -> AStr (Js.to_string str))
  | Event _
  | MouseEvent _
  | KeyboardEvent _
  | TouchEvent _
    -> failwith "acontent: Unable to deal with event handlers."

type econtent =
  | Empty
  | Comment of string
  | EncodedPCDATA of string
  | PCDATA of string
  | Entity of string
  | Leaf of ename * attrib list
  | Node of ename * attrib list * elt list

let content (e : elt) =
  match Dom.nodeType e with
  | Element element ->
    let tag = Js.to_string element##.tagName in
    let attribs =
      let attrs = element##.attributes in
      List.init attrs##.length (fun i ->
        let attr =
          match Js.Opt.to_option (attrs##item i) with
          | None -> assert false
          | Some a -> a in
        convert_back_attrib
          (Js.to_string attr##.name, Attr (React.S.const (Some attr##.value)))) in
    let children =
      let childs = element##.childNodes in
      List.init childs##.length (fun i ->
        match Js.Opt.to_option (childs##item i) with
        | None -> assert false
        | Some c -> c) in
    Node (tag, attribs, children)
  | Attr attr -> failwith (Printf.sprintf "Isolated attribute %s" (Js.to_string attr##.name))
  | Text txt -> PCDATA (Js.to_string txt##.data)
  | Other _node -> failwith "content: Unexpected node type."

