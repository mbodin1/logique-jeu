open Js_of_ocaml_tyxml
open Tyxml_js

include Xml_sigs.Iterable
  with type uri = Xml.uri
  and type event_handler = Xml.event_handler
  and type mouse_event_handler = Xml.mouse_event_handler
  and type keyboard_event_handler = Xml.keyboard_event_handler
  and type touch_event_handler = Xml.touch_event_handler
  and type attrib = Xml.attrib
  and type elt = Xml.elt

