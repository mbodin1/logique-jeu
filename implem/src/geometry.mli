
(* Cartesian coordinate. *)
type coordinate = {
  x : float ;
  y : float
}

(* Polar coordinate. *)
type polar_coordinate = {
  distance : float ;
  angle : float
}

(* Vector coordinate. *)
type vector = coordinate

(* A type for straight lines. *)
type line

(* A type for arc (circle segments). *)
type arc

(* Mainly for debugging or testing purposes.
  Check whether a coordinate is at ``normal'' distances from 0. *)
val test_normal : coordinate -> bool

(* True if no infinity of nan values are present. *)
val valid_vector : vector -> bool

(* Add a vector to a position. *)
val add_vector : coordinate -> vector -> coordinate

(* Square of a floating-point value. *)
val square : float -> float

(* Square of the distance between two points. *)
val sq_distance : coordinate -> coordinate -> float

(* Scalar product of two vectors. *)
val scalar_product : vector -> vector -> float

(* Square of the magnitude of a vector. *)
val sq_magnitude : vector -> float

(* Scale a vector by the provided factor. *)
val scale : vector -> float -> vector

(* The point in the middle of two points. *)
val middle : coordinate -> coordinate -> coordinate

(* Vector from the first point to the second. *)
val build_vector : coordinate -> coordinate -> vector

(* Orthogonally turn a vector counterclockwise. *)
val turn_vector_left : vector -> vector

(* Orthogonally turn a vector clockwise. *)
val turn_vector_right : vector -> vector

(* The opposite (U-turn) of a vector. *)
val opp_vector : vector -> vector

(* Build a line passing from the two provided coordinates.
  If the two points are identical, it will return one of the lines passing there. *)
val build_line : coordinate -> coordinate -> line

(* The line along this vector passing by this point.
  If the vector is null, then any line passing through this point may be returned. *)
val line_from_vector : vector -> coordinate -> line

(* Build the line parallel to the first line and passing to the provided point. *)
val parallel_by : line -> coordinate -> line

(* Build the line perpendicular to the first line and passing to the provided point. *)
val perpendicular_by : line -> coordinate -> line

(* Return the perpendicular bisector of the two points, that is the line whose points
 are all equidistant from the two provided points.
 If the two points are identical, it will return any line passing from these two points. *)
val equidistant : coordinate -> coordinate -> line

(* The orthogonal projection of the point onto the line. *)
val project : coordinate -> line -> coordinate

(* The intersection of two lines, if not parallel.
  If the two lines are the same, the function will return any point among them. *)
val intersection : line -> line -> coordinate option

(* Build an arc from two extremum points and its curvature (inverse of radius).
  There will actually be two possible such circles: the sign of the curvature will
  discriminate between them. *)
val build_arc : coordinate -> coordinate -> float -> arc

(* State whether the curve between these two points makes sense. *)
val valid_arc : arc -> bool

(* Past this comment, functions tend to take more time than the functions above this point. *)

(* Convert a polar coordinate (expressed as radian) into cartesian ones. *)
val polar : polar_coordinate -> coordinate

(* Reverse operation than polar. *)
val to_polar : coordinate -> polar_coordinate

(* Normalise a vector, that is return with the same direction but a unit length. *)
val normalise : vector -> vector

(* Given a starting point, a vector indicating a direction, and a distance to move,
  return the target point.
  If the distance is negative, then the target is behind the start. *)
val move_along_direction : coordinate -> vector -> float -> coordinate

(* Locate the center of the circle of an arc. *)
val get_arc_center : arc -> coordinate

(* Given an arc as well as a percentage of the length of this arc,
  return the point at this position. *)
val point_on_arc : arc -> float -> coordinate

(* Turn a vector by this many radians, counterclockwise. *)
val turn : vector -> float -> vector

