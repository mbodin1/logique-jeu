
type cost_change =
  | Dont_know
  | Cost_change of float
  | Approximation of float

let multiply_cost_change weight = function
  | Dont_know -> Dont_know
  | Cost_change delta -> Cost_change (weight *. delta)
  | Approximation delta -> Approximation (weight *. delta)


type parameters = {
  temperature_init : float ;
  temperature_min : float ;
  lambda : int -> float ;
  factor : float -> int -> float
}

(* Default parameters. *)
let default = {
  lambda = (fun nb_failure -> if nb_failure < 15 then 0.99 else 0.98) ;
  temperature_init = 140. ;
  temperature_min = 0.01 ;
  factor = fun temperature nb_failure -> temperature /. (Float.of_int (10 * nb_failure) +. 100.)
}

module List = struct
  include List

  let rec fold_left4 f init l1 l2 l3 l4 =
    match l1, l2, l3, l4 with
    | [], [], [], [] -> init
    | x1 :: l1, x2 :: l2, x3 :: l3, x4 :: l4 ->
      fold_left4 f (f init x1 x2 x3 x4) l1 l2 l3 l4
    | _ -> invalid_arg "fold_left4"
end

let sfsimulate ?(parameters = default) costs change init =
  let rec aux temperature nb_failure (best_state, best_costs) (current_state, current_costs) =
    if temperature < parameters.temperature_min then best_state
    else
      let temperature = temperature *. parameters.lambda nb_failure in
      let factor = parameters.factor temperature nb_failure in
      let (new_state, cost_changes) = change factor current_state in
      let (new_costs, current_costs, diff, diff_with_best) =
        let (new_costs, current_costs, diff, diff_with_best) =
          List.fold_left4 (fun (new_costs, current_costs, diff, diff_with_best)
                               current_cost best_cost cost ->
            let update_diff_with_best delta =
              match current_cost with
              | Some c -> diff_with_best +. (c +. delta) -. best_cost
              | None ->
                (* We need to provide an approximation of the difference with the best
                  nevertheless. The goal is to avoid missing potential better candidates. *)
                diff_with_best +. max 0. delta in function
            | Dont_know ->
              (* Not knowing the cost of the change forces the computation of both the current
                and new costs.  The current cost might have already been computed. *)
              let current_cost =
                match current_cost with
                | Some c -> c
                | None -> cost current_state in
              let new_cost = cost new_state in
              (Some new_cost :: new_costs,
               Some current_cost :: current_costs,
               diff +. new_cost -. current_cost,
               diff_with_best +. new_cost -. best_cost)
            | Cost_change delta ->
              (* If we only know the delta, we do not force any computation,
                but this means that we might not be able to compare with our best state. *)
              let new_cost = Option.map (fun c -> c +. delta) current_cost in
              (new_cost :: new_costs,
               current_cost :: current_costs,
               diff +. delta,
               update_diff_with_best delta)
            | Approximation delta ->
              (* We have an approximation of the cost change, but we can't use it to infer
                the new cost, to avoid adding errors. *)
              (None :: new_costs,
               current_cost :: current_costs,
               diff +. delta,
               update_diff_with_best delta)
            ) ([], [], 0., 0.) current_costs best_costs costs cost_changes in
        (List.rev new_costs, List.rev current_costs, diff, diff_with_best) in
      let (best, new_costs) =
        (* We check whether we might want to switch our best viewed state.
          As we force to know all of the costs of the best seen state, we might force the
          computation of costs. *)
        if diff_with_best >= 0. || not (Float.is_finite diff_with_best) then
          ((best_state, best_costs), new_costs)
        else
          let new_costs =
            let new_costs =
              List.fold_left2 (fun new_costs cost -> function
                | Some new_cost -> new_cost :: new_costs
                | None -> cost new_state :: new_costs) [] costs new_costs in
            List.rev new_costs in
          let best =
            let sum costs = List.fold_left (+.) 0. costs in
            if sum new_costs < sum best_costs then
              (new_state, new_costs)
            else (best_state, best_costs) in
          (best, List.map (fun c -> Some c) new_costs) in
      (* We can now choose our next step. *)
      let nb_failure = if diff > 0. then 1 + nb_failure else 0 in
      let (new_state, new_costs) =
        if diff <= 0.
          || Random.float 1. < exp (-. diff /. temperature) then
          (new_state, new_costs)
        else (current_state, current_costs) in
      aux temperature nb_failure best (new_state, new_costs) in
  let init_costs = List.map (fun cost -> cost init) costs in
  aux parameters.temperature_init 0 (init, init_costs)
    (init, List.map (fun c -> Some c) init_costs)

let efsimulate ?(parameters = default) cost change init =
  let change factor state =
    let (state, delta) = change factor state in
    (state, [delta]) in
  sfsimulate ~parameters [cost] change init

let fsimulate ?(parameters = default) cost change init =
  let change factor state = (change factor state, Dont_know) in
  efsimulate ~parameters cost change init

let esimulate ?(parameters = default) cost change init =
  efsimulate ~parameters cost (fun _ -> change) init

let ssimulate ?(parameters = default) costs change init =
  sfsimulate ~parameters costs (fun _ -> change) init

let cfsimulate ?(parameters = default) costs costs_change change init =
  assert (List.length costs = List.length costs_change) ;
  let change factor state =
    let (state', change) = change factor state in
    (state', List.map (fun f -> f state change) costs_change) in
  sfsimulate ~parameters costs change init

let csimulate ?(parameters = default) costs costs_change change init =
  cfsimulate ~parameters costs costs_change (fun _ -> change) init

(* For simulate, we provide a simpler version. *)
let simulate ?(parameters = default) cost change init =
  let rec aux temperature best_state best_cost current_state current_cost =
    if temperature < parameters.temperature_min then best_state
    else
      let temperature = temperature *. parameters.lambda 0 in
      let new_state = change current_state in
      let new_cost = cost new_state in
      let delta = current_cost -. new_cost in
      let (best_state, best_cost) =
        if new_cost <= best_cost then (new_state, new_cost)
        else (best_state, best_cost) in
      let (new_state, new_cost) =
        if delta <= 0.
           || Random.float 1. < exp (-. delta /. temperature) then
          (new_state, new_cost)
        else (current_state, current_cost) in
      aux temperature best_state best_cost new_state new_cost in
  aux parameters.temperature_init init (cost init) init (cost init)

(* We then test that the simpler version of simulate behaves indeed like
  the more general case. *)
let%test "compare simulate simple" =
  let cost x = (sin (1. /. (x +. 0.03))) /. (x +. 10.) in
  let change x = Float.rem (x +. Float.pi) 10. in
  let init = 0.1 in
  let test (simulate : ?parameters:_ -> _) =
    Random.init 0 ;
    simulate cost change init in
  let simulate' ?(parameters = { default with lambda = fun _ -> default.lambda 0 })
      cost change init =
    fsimulate ~parameters cost (fun _ -> change) init in
  test simulate = test simulate'

(* As the function sfsimulate relies on a lot of list reversing, we might
  want to test that indeed the lists don't get mixed.
  We thus provide this test with several criteriums, but in which the change
  function is trivial (it directly goes into the right value). *)
let%test "simulate trivial change" =
  let costa = function
    | true -> 1.
    | false -> -1. in
  let costb = function
    | true -> -10.
    | false -> 10. in
  let change _b = (true, [Dont_know; Dont_know]) in
  ssimulate [costa; costb] change false

