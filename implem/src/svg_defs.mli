
(* The implementation of this file is given in the dune file.
  It imports the content of SVG images in the images/ folder into TyXML. *)

module Data (Xml : Xml_sigs.NoWrap) (Svg : Svg_sigs.Make(Xml).T) : sig

  (* All the below elements into one large g element. *)
  val all : [> Svg_types.g ] Svg.elt list

  (* The g element and its dimensions (width and height). *)
  type image = Svg_types.g Svg.elt * (float * float)

  (* A shed cell, for thieves to lead. *)
  val shed : image

  (* A buoy cell, for pirates to lead. *)
  val buoy : image

  (* A brown cross, making thieves win. *)
  val thieves_win : image

  (* A blue cross, making pirates win. *)
  val pirates_win : image

  (* Arrow head. *)
  val arrow : image

end

