
(* The expected sizes of different images/nodes. *)

(* Size of a node. *)
let node = 10.

(* Width of an arc. *)
let arc = 1.

(* Width of the smallest wave. *)
let wave_unit = 0.5

(* Size of a winning cross. *)
let cross = 7.

(* Size of a flag. *)
let flag = 6.

(* Size of a flag symbol. *)
let symbol = 1.8

(* Size of a signal. *)
let signal = 9.

(* Size of an arrow head. *)
let head = 3. *. arc

