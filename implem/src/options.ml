
open Functors

type 'a endo = 'a -> 'a

(* Action associated to the compilation option, each specified by its compilation step. *)
type action =
  | AFormula of Formula.tformula endo
  | AGraph of Ngraph.t endo
  | APositionInit of (Ngraph.t -> Ngraph.tposition)
  | APosition of (Position.settings -> Ngraph.tposition endo)
  | AIsland of (Position.settings -> Ngraph.tisland endo)
  | AImage of Image.t endo

(* Positive actions are disabled by default, and can be enabled.
   Negative actions are enabled by default, and sending the option disable it. *)
type 'action aprop = {
  taction : 'action ;
  tname : string ;
  tpositive : bool ;
  tdefault : bool
}

type t = action aprop

let map_prop f l =
  List.map (fun o -> { o with taction = f o.taction }) l

let name o = o.tname

let default o = o.tdefault

let make ?(positive = true) ?(default = false) name a = {
  taction = a ;
  tname = name ;
  tpositive = positive ;
  tdefault = default
}

let all_formula = [
    make "not" (fun f -> Formula.Not f) ;
    make "quantifier elim" Typer.quantifier_elim ;
    make "coalesce" Typer.coalesce ;
    make "clean" Typer.remove_trivial ;
    make "uncut" Typer.uncut ;
    make ~default:true "unfold" Typer.unfold
  ]

let all_graph = [
    make ~default:true "remove trivial conditions" Graphify.remove_trivial_conditions ;
    make "split conditions" (Graphify.split_conditions true) ;
    make "chains" Graphify.chain ;
    make ~positive:false ~default:true "symmetries" Graphify.remove_symmetries ;
    make ~default:true "left to right" (Graphify.force_direction true) ;
    make ~default:true "start at left" (Graphify.start_direction true) ;
    make "separate" Graphify.separate_subformulas ;
    make ~default:true "unique ends" (Graphify.merge_true_false true) ;
    make "unique subs" (Graphify.merge_identical false true) ;
    make "strong unique subs" (Graphify.merge_identical true false) ;
    make ~default:true "remove trivial nodes" Graphify.remove_trivial_nodes ;
    make "remove mergable quantifier nodes" Graphify.merge_quantifier_node ;
    make "alternate" Graphify.alternate
  ]

let all_init_position = [
    make "circle" Position.Node.circle ;
    make "line" Position.Node.line ;
    make ~default:true "tree" Position.Node.tree
  ]

let all_position = [
    make ~default:true ~positive:false "no positionning" (fun s -> Position.Node.optimise ~settings:s)
  ]

let all_island = [
    make ~positive:false "isolated" (fun s -> Position.Island.optimise ~settings:s) ;
    make "no island" (fun _settings g -> Position.Island.empty g.Ngraph.igraph)
  ]

let all_image = [
    make ~default:true "whiten" Image.whiten ;
    make ~default:true "whiten background" Image.whiten_background
  ]

let all = List.concat [
    map_prop (fun f -> AFormula f) all_formula ;
    map_prop (fun f -> AGraph f) all_graph ;
    map_prop (fun f -> APositionInit f) all_init_position ;
    map_prop (fun f -> APosition f) all_position ;
    map_prop (fun f -> AIsland f) all_island ;
    map_prop (fun f -> AImage f) all_image
  ]

(* Names used in other places of the program that shouldn't clash with option names. *)
let reserved_names = ["input_formula"]

let%test "disjoint options" =
  let only_letter_ascii f =
    let char_list s = List.init (String.length s) (fun i -> s.[i]) in
    String.concat "" (List.map (function
      | c when (c >= 'a' && c <= 'z')
            || (c >= 'A' && c <= 'Z')
            || (c >= '0' && c <= '9')
        -> String.make 1 c
      | _ -> "") (char_list f)) in
  let names = List.map (fun a -> a.tname) all in
  let names = names @ List.map fst Position.default_settings in
  let names = reserved_names @ names in
  let names = List.map only_letter_ascii names in
  List.length names = List.length (List.sort_uniq compare names)

let%test "exist default positionning option" =
  List.count (fun o -> o.tdefault) all_init_position = 1

(* Return a formula from the string input. *)
let parse lexbuf =
  lexbuf.Lexing.lex_curr_p <- {
      Lexing.pos_fname = "" ;
      Lexing.pos_lnum = 1 ;
      Lexing.pos_bol = 0 ;
      Lexing.pos_cnum = 0
    } ;
  try Parser.main Lexer.read lexbuf with
  | Parser.Error ->
    failwith (Printf.sprintf "Error: Parser error at %s." (Lexer.current_position lexbuf))
  | Lexer.SyntaxError msg ->
    failwith (Printf.sprintf "Error: Lexer error at %s: %s" (Lexer.current_position lexbuf) msg)
  | e ->
    failwith (Printf.sprintf "Error during parsing at %s: %s." (Lexer.current_position lexbuf)
      (Printexc.to_string e))

let apply ?(position = Position.default_settings) options lexbuf =
  let options = List.fold_left (fun s o -> SSet.add o.tname s) SSet.empty options in
  let apply_options all f =
    let actions =
      List.filter_map (fun o ->
        let b = SSet.mem o.tname options in
        let b = if o.tpositive then b else not b in
        if b then
          Some o.taction
        else None) all in
    List.fold_left (fun f a -> a f) f actions in
  let apply_options_position default_settings all g =
    let all =
      let existing = SSet.of_list (List.map fst default_settings) in
      let position = List.filter (fun (name, _weigth) -> SSet.mem name existing) position in
      map_prop (fun f -> f position) all in
    apply_options all g in
  let f = parse lexbuf in
  let f = Typer.typer f in
  let f = Typer.map snd snd snd f in
  let f = apply_options all_formula f in
  let g = Graphify.build f in
  let g = apply_options all_graph g in
  let g = Graphify.garbage_collector g in
  let g =
    let f =
      match List.find_opt (fun o -> SSet.mem o.tname options) all_init_position with
      | Some o -> o.taction
      | None ->
        match List.find_opt (fun o -> o.tdefault) all_init_position with
        | Some o -> o.taction
        | None -> assert false in
    f g in
  let g = apply_options_position Position.Node.default_settings all_position g in
  let g = Position.Island.isolated g in
  let g = apply_options_position Position.Island.default_settings all_island g in
  let i = Image.of_graph g in
  let i = apply_options all_image i in
  i

(* Test the provided expression. *)
let test f =
  let options = List.filter default all in
  let f = Lexing.from_string f in
  Random.init 42 ; (* We fix the seed to avoid non-deterministic tests. *)
  ignore (apply options f)

let%test_unit "apply on True" =
  test "True"

let%test_unit "apply on true and (false or true)" =
  test "true and (false or true)"

let%test_unit "apply on True && (True || False)" =
  test "True && (True || False)"

let%test_unit "apply on ((((((((((False))))))))))" =
  test "((((((((((False))))))))))"

let%test_unit "apply on ~ (⊤ /\\ ⊥) ⇔ \\false ∧ ¬ \\true" =
  test "~ (⊤ /\\ ⊥) ⇔ \\false ∧ ¬ \\true"

let%test_unit "apply on forall x y, green (x) -> red(x)" =
  test "forall x y, green (x) -> red(x)"

let%test_unit "apply on \\neg (\\forall x y, green \\left(x\\right) \\longrightarrow red {y})" =
  test "\\neg \\left\\(\\forall x y, green \\left(x\\right) \\longrightarrow red {y}\\right\\)"

let%test_unit "apply on exists x, green(x) or (for all y, green(y))" =
  test "exists x, green(x) or (for all y, green(y))"

