
(* Map over the information of a formula. *)
val map : ('a1 -> 'a2) -> ('b1 -> 'b2) -> ('c1 -> 'c2) ->
          ('a1, 'b1, 'c1) Formula.t -> ('a2, 'b2, 'c2) Formula.t

(* Check that the formula is typable, and aggregate the computed types.
  Raise invalid_arg otherwise. *)
val typer : ('a * Formula.tvar option, 'b, 'c) Formula.t ->
            ('a * Formula.tvar, 'b * Formula.tpred, 'c * Formula.tpformula) Formula.t

(* Print out the formula, for debugging purposes. *)
val show : ('a, 'b, 'c) Formula.t -> string

(* The free variables of a formula. *)
val free_variables : ('a, 'b, 'c) Formula.t -> 'a Formula.variable Seq.t * 'c Formula.variable Seq.t

(* All variables appearing in a formula. *)
val all_variables : ('a, 'b, 'c) Formula.t -> 'a Formula.variable Seq.t * 'c Formula.variable Seq.t

(* Use associativity to coalesce nested ands and ors.
  For instance, And [And [f1; f2]; f3] is replaced by And [f1; f2; f3]. *)
val coalesce : ('a, 'b, 'c) Formula.t -> ('a, 'b, 'c) Formula.t

(* Remove trivial ands and ors.
  For instance And [True; f; f] is replaced by f. *)
val remove_trivial : ('a, 'b, 'c) Formula.t -> ('a, 'b, 'c) Formula.t

(* Move all Not as deep as possible. *)
val uncut : ('a, 'b, 'c) Formula.t -> ('a, 'b, 'c) Formula.t

(* Remove all LetIn. *)
val unfold : ('a, 'b, 'c) Formula.t -> ('a, 'b, 'c) Formula.t

(* Eliminate all quantifiers. *)
val quantifier_elim : (Formula.tvar, 'b, 'c) Formula.t -> (Formula.tvar, 'b, 'c) Formula.t

