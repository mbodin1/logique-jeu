
(* To position all the nodes, this module relies on several heuristics.
  This type lists all their names and respective weight, including some heuristics whose
  default weight is zero. *)
type settings = (string * float) list

(* Position of nodes. *)
module Node : sig

(* Position all the nodes in circle. *)
val circle : Ngraph.t -> Ngraph.tposition

(* Position all the nodes in a line. *)
val line : Ngraph.t -> Ngraph.tposition

(* Position all the nodes in a tree if possible. *)
val tree : Ngraph.t -> Ngraph.tposition

(* The default settings. *)
val default_settings : settings

(* Optimise the position of each node according to the constraints.
  Its settings can be changed (absent heuristics will not be considered and unknown
  heuristic names will yield error). *)
val optimise : ?settings : settings -> Ngraph.tposition -> Ngraph.tposition

end

(* Position of islands. *)
module Island : sig

(* No island are added. *)
val empty : Ngraph.tposition -> Ngraph.tisland

(* Place each thieves node into its own island. *)
val isolated : Ngraph.tposition -> Ngraph.tisland

val default_settings : settings

val optimise : ?settings : settings -> Ngraph.tisland -> Ngraph.tisland

end

(* The concatenation of Node.default_settings and Island.default_settings. *)
val default_settings : settings

