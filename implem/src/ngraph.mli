
open Functors

(* Identifier of nodes. *)
type id = int

(* Identifiers for variables. *)
type var = int

(* The kind of each node. *)
type kind =
  | And
  | Or
  | True
  | False
  | Forall of var * Formula.tvar (* This variable of this type has to be chosen. *)
  | Exists of var * Formula.tvar

(* Condition to be able to follow a transition. *)
type condition =
  | MustHave of var * Formula.predicate_val
  | CantHave of var * Formula.predicate_val

(* Constraints on the positionning of nodes. *)
type node_constraint =
  | Symmetric of id (* This node is symmetrical with another node, relative to the horizontal axis. *)
  | LeftOf of id (* This node is left of another. *)
  | Below of id (* This node is below of another. *)

(* TODO: Add node clusters with their associated constraints. *)

module GGraph = Graph.Pack.Digraph

(* Informations attached to a node. *)
type node_info = {
  vertex : GGraph.V.t (* Vertex in the graph. *) ;
  subformula : Formula.tformula (* The represented subformula *) ;
  kind : kind (* The properties of the node. *) ;
  constraints : node_constraint list (* The constraints applying to this node. *)
}

(* Structure expliciting the constraints of the graph. *)
type t = {
  start : id (* The starting node. *) ;
  last_free_id : id (* This identifier and any higher than this are unused. *) ;
  var_names : string IMap.t (* The names of the used variables. *) ;
  graph : GGraph.t (* The graph of all nodes pointing to the other nodes. *) ;
  arc_conditions : condition list IIMap.t (* Conditions to be able to take this transition. *) ;
  infos : node_info IMap.t (* Information attached to each node. *)
}

(* The same type, but augmented with node positions.
  A curvature of 0. is a straight line, a curvature of 2. /. distance is a circle
  whose center is between both endings of the arc. Curvature can't exceed this
  maximum curvature.
  A positive curvature is right of the vector, a negative one is left. *)
type tposition = {
  pgraph : t ;
  positions : Geometry.coordinate IMap.t ;
  arcs : (float * float list) IIMap.t (* Curvature of the arc and position of each condition. *)
}

(* We represent islands by a close loop of coordinates, each associated with the curvature
  from the previous point of the island.
  Each island is supposed to have at least two nodes. *)
type island = (float * Geometry.coordinate) list

(* As tposition, but augmented with island positions.
  Islands are assumed to behave like the SVG fill-rule=evenodd value: an island occuring
  within another island actually represents a pond. *)
type tisland = {
  igraph : tposition ;
  islands : island list (* List of all island. *)
}

