
(* Parameters of the simulation. *)
type parameters = {
  temperature_init : float (* Initial temperature. *) ;
  temperature_min : float (* The simulation ends when reaching this temperature. *) ;
  lambda : int -> float (* Coefficient between 0. and 1. at which the temperature decreases, depending on the number of previously failed attemps since the last increase. *) ;
  factor : float -> int -> float (* The factor provided to fsimulate depending on the temperature and the number of attemps since the last better state. *)
}

(* Given a cost function, and a function randomly changing the current state,
  try to minimise the cost function.
  The parameters of the simulation are facultative: default parameters will be used
  if needed. *)
val simulate :
  ?parameters : parameters ->
  ('state -> float) ->
  ('state -> 'state) ->
  'state ->
  'state

(* Same, but the change function takes an additionnal factor argument as a
  floating-point number.
  If this number is 1., it is expected to do as before.
  If it is between 0. and 1., it is expected to change less.
  If it is more than 1., it is expected to change more. *)
val fsimulate :
  ?parameters : parameters ->
  ('state -> float) ->
  (float -> 'state -> 'state) ->
  'state ->
  'state

(* Usually the cost function is costly to compute (typically quadratic), whilst changes
  are typically local, and thus their associated cost can be less costly to compute
  (typically linear).
  We now enable the possibility for the change function to provide us with *)
type cost_change =
  | Dont_know (* Computing the cost would basically amount to calling the cost function. *)
  | Cost_change of float (* Computing the cost difference is easier when we know the change, and here it is. *)
  | Approximation of float (* Computing the cost difference costs a lot, but computing an approximation is easier. *)

(* Multiply an approximation of the cost by a given factor. *)
val multiply_cost_change : float -> cost_change -> cost_change

(* Like simulate, but the change function returns a cost change computation. *)
val esimulate :
  ?parameters : parameters ->
  ('state -> float) ->
  ('state -> 'state * cost_change) ->
  'state ->
  'state

(* Like esimulate, but with the change factor of fsimulate. *)
val efsimulate :
  ?parameters : parameters ->
  ('state -> float) ->
  (float -> 'state -> 'state * cost_change) ->
  'state ->
  'state

(* Cost functions are often sums of several cost functions.
  Some local change could yield easier computations in some parts of it, but not all.
  Thus this function takes a list of individual cost functions, and each change should provide a list of cost change
  (each corresponding to the corresponding cost function).
  The size of all cost lists and cost_change list must match. *)
val ssimulate :
  ?parameters : parameters ->
  ('state -> float) list ->
  ('state -> 'state * cost_change list) ->
  'state ->
  'state

(* Same, but with the factor of fsimulate. *)
val sfsimulate :
  ?parameters : parameters ->
  ('state -> float) list ->
  (float -> 'state -> 'state * cost_change list) ->
  'state ->
  'state

(* A convenient variation of ssimulate, in which the changes are explicitely given a type.
  An additionnal argument of cost function from the changes is added. *)
val csimulate :
  ?parameters : parameters ->
  ('state -> float) list ->
  ('state -> 'change -> cost_change) list -> (* Takes the state before applying the change. *)
  ('state -> 'state * 'change) ->
  'state ->
  'state

(* Same, but with the factor of fsimulate. *)
val cfsimulate :
  ?parameters : parameters ->
  ('state -> float) list ->
  ('state -> 'change -> cost_change) list ->
  (float -> 'state -> 'state * 'change) ->
  'state ->
  'state

