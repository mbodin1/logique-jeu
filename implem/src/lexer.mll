{ (** Module Lexer. **)

  open Lexing
  open Parser

  exception SyntaxError of string

  let next_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      { pos with pos_bol = lexbuf.lex_curr_pos ; pos_lnum = 1 + pos.pos_lnum }

  let current_position lexbuf =
    let pos = lexbuf.lex_curr_p in
    let file =
      if pos.pos_fname <> "" then Printf.sprintf "file `%s' " pos.pos_fname else "" in
    Printf.sprintf "%sline %i, character %i" file pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)
}

let space = ' ' | '\t'
let newline = '\n' | '\r' | "\r\n"

let letter = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9']

let ident = (letter | digit | '_')*

let strctn = [^ '\n' '\r' '"']*

rule read = parse

  | '\\'? ("True" | "true" | "TRUE") | "⊤" | "T" | "1" | "■" | "\\top"            { TRUE }
  | '\\'? ("False" | "false" | "FALSE") | "⊥" | "F" | "0" | "□" | "\\bot"         { FALSE }

  | '\\'? "for" '_'? "all" | "for" space+ "all" | "∀"                             { FORALL }
  | '\\'? "exists" | "∃"                                                          { EXISTS }

  | "and" | "And" | "AND" | "/\\" | "∧" | "&" | "&&" | "\\land" | "\\wedge"       { AND }
  | "or" | "Or" | "OR" | "\\/" | "∨" | "|" | "||" | "\\lor" | "\\vee"             { OR }
  | '~' | "¬" | "￢" | "!" | "not" | "Not" | "NOT" | "\\neg" | "\\not" | "\\lnot" { NEG }

  | "->" | "→" | "=>" | "⇒" | "\\impl" | "\\to"
  | '\\' ("Long" | "long")? ('R' | 'r') "ightarrow"                               { IMPL }

  | "<->" | "↔" | "<=>" | "⇔" | "≡" | "iff" | "Iff" | "IFF"
  | "\\equiv" | "\\iff"
  | '\\' ("Long" | "long")? ("Left" |"left") ('R'|'r') "ightarrow"                { EQUIV }

  | '\\'? ("Red" | "red" | "RED") | "🔴" | "🟥"                 { RED }
  | '\\'? ("Green" | "green" | "GREEN") | "🟢" | "🟩"           { GREEN }
  | '\\'? ("Blue" | "blue" | "BLUE") | "🔵" | "🟦"              { BLUE }

  | (("\\left"?) as l) space* (('\\'? ('(' | '[' | '{')) as p)  { OPAR (l ^ p) }
  | (("\\right"?) as r) space* (('\\'? (')' | ']' | '}')) as p) { CPAR (r ^ p) }
  | ','                                                         { COMMA }

  | "let" | "Let" | "LET"                                       { LET }
  | "=" | ":=" | "be" | "Be" | "BE"                             { BE }
  | "in" | "In" | "IN"                                          { IN }

  | ":"                                                         { COLON }
  | "BOOL" | "bool" | "Bool"                                    { BOOL }
  | "COLOR" | "color" | "Color"                                 { COLOR }

  | (['a'-'z'] ident) as id (* Lower-case identifier. *)        { LIDENT id }
  | (['A'-'Z'] ident) as id (* Upper-case identifier. *)        { UIDENT id }

  | space+                                                      { read lexbuf }
  | "(*"                                                        { comment lexbuf ; read lexbuf }

  | newline                                                     { next_line lexbuf ; read lexbuf }
  | eof                                                         { EOF }
  | _                                                           { raise (SyntaxError ("Unexpected char: `" ^
                                                                    lexeme lexbuf ^ "'.")) }

and comment = parse         

  | "(*"                    { comment lexbuf ; comment lexbuf }
  | "*)"                    { () }

  | newline                 { next_line lexbuf ; comment lexbuf }
  | eof                     { raise (SyntaxError "Incomplete comment") }
  | _                       { comment lexbuf }

