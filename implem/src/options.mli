
(* Compilation options. *)
type t

(* The list of all possible options available to the user. *)
val all : t list

(* A display name for these options. *)
val name : t -> string

(* Whether this option should be enabled by default in the interface. *)
val default : t -> bool

(* Perform the compilation from the provided options, reading from the buffer. *)
val apply : ?position : Position.settings -> t list -> Lexing.lexbuf -> Image.t

