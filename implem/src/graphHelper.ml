
open Ngraph

type side = bool

let side_of_kind = function
  | Or | True | Exists _ -> true
  | And | False | Forall _ -> false

let to_intermediate_kind s =
  if s then Or else And

let inverse_condition = function
  | MustHave (x, p) -> CantHave (x, p)
  | CantHave (x, p) -> MustHave (x, p)

