%{ (** Module Parser. **)

open Functors

(*type loc = unit*) (* TODO: location information. *)

(* Build a variable with dummy type. *)
let make_fvar ?(loc = ()) x = {
  Formula.var_type = loc ;
  Formula.var_name = x
}

let make_var ?(t = None) ?(loc = ()) x = {
  Formula.var_type = (loc, t) ;
  Formula.var_name = x
}

(* Build a predicate with dummy type. *)
let make_pred p = {
  Formula.pred_type = () ;
  Formula.pred_val = p
}

let implication f1 f2 =
  Formula.(Or [Not f1; f2])

module SSSet = Set.Make (Pair (String) (String))

let accepted_parentheses l r =
  let all_accepted =
    let add s (l, r) =
      List.fold_left (fun s p ->
        let l = p ^ l in
        let r = p ^ r in
        let s = SSSet.add (l, r) s in
        let s = SSSet.add ("\\left" ^ l, "\\right" ^ r) s in
        s) s [""; "\\"] in
    let l = [("(", ")") ; ("[", "]") ; ("{", "}")] in
    List.fold_left add SSSet.empty l in
  let remove_space =
    Str.global_replace (Str.regexp "[ \t\n\r]") "" in
  let l = remove_space l in
  let r = remove_space r in
  SSSet.mem (l, r) all_accepted

%}

%token          EOF
%token<string>  OPAR CPAR
%token          COMMA
%token          TRUE FALSE
%token          FORALL EXISTS
%token          AND OR NEG
%token          IMPL EQUIV
%token          LET BE IN
%token          RED GREEN BLUE
%token          COLON BOOL COLOR
%token<string>  LIDENT UIDENT

%nonassoc   COMMA
%nonassoc   IN
%left       EQUIV
%left       IMPL

%start<(unit * Formula.tvar option, unit, unit) Formula.t> main

%%

main:
  | f = formula; EOF  { f }

formula:
  | f1 = formula; IMPL; f2 = formula    { implication f1 f2 }
  | f1 = formula; EQUIV; f2 = formula   { Formula.And [implication f1 f2; implication f2 f1] }
  | FORALL; xs = LIDENT+; t = type_info; COMMA; f = formula
    { List.fold_right (fun x f -> Formula.Forall (make_var ~t x, f)) xs f }
  | EXISTS; xs = LIDENT+; t = type_info; COMMA; f = formula
    { List.fold_right (fun x f -> Formula.Exists (make_var ~t x, f)) xs f }
  | LET; x = uvar; args = farg_names*; BE; f1 = formula; IN; f2 = formula
    { Formula.LetIn (x, List.map (Either.map ~left:make_var ~right:make_fvar) args, f1, f2) }
  | f = formula_or                      { f }

formula_or:
  | f = formula_and; OR; fs = separated_nonempty_list(OR, formula_and) { Formula.Or (f :: fs) }
  | f = formula_and                                                    { f }

formula_and:
  | f = formula_base; AND; fs = separated_nonempty_list(AND, formula_base) { Formula.And (f :: fs) }
  | f = formula_base                                                       { f }

formula_base:
  | f = formula_litteral                                              { f }
  | f = parenthesised (formula)                                       { f }
  | NEG; f = formula_base                                             { Formula.Not f }
  | x = uvar; args = variable_arguments                               { Formula.Variable (x, args) }
  | p = predicate; args = parenthesised (separated_list (COMMA, lvar))
  | p = predicate; args = lvar+                                       { Formula.Predicate (p, args) }

formula_litteral:
  | TRUE                          { Formula.True }
  | FALSE                         { Formula.False }
  | x = uvar                      { Formula.Variable (x, []) }
  | p = predicate                 { Formula.Predicate (p, []) }

(* Arguments of a parameterised formula. *)
variable_arguments:
  | args = variable_argument+ { List.concat args }

variable_argument:
  | args = parenthesised (separated_list (COMMA, farg))   { args }
  | arg = farg_unprotected                                { [arg] }

(* Argument of a parameterised formula. *)
farg:
  | x = lvar    { Either.Left x }
  | f = formula { Either.Right f }

(* Same as farg, but without the protective parentheses. *)
farg_unprotected:
  | x = lvar                { Either.Left x }
  | f = formula_litteral    { Either.Right f }

(* Argument names of a parameterised formula. *)
farg_names:
  | x = LIDENT  { Either.Left x }
  | x = UIDENT  { Either.Right x }

uvar:
  | x = UIDENT  { make_fvar x }

lvar:
  | x = LIDENT  { make_var x }

predicate:
  | p = predicate_val { make_pred p }

predicate_val:
  | c = color { Formula.PColor c }

color:
  | RED   { Formula.Red }
  | GREEN { Formula.Green }
  | BLUE  { Formula.Blue }

(* A possible type annotation. *)
type_info:
  | empty         { None }
  | COLON; BOOL   { Some Formula.Bool }
  | COLON; COLOR  { Some Formula.Color }

parenthesised (payload):
  | l = OPAR; p = payload; r = CPAR
    { if not (accepted_parentheses l r) then
        invalid_arg (Printf.sprintf "Parenthesis %s closed with %s." l r) ;
      p }

%inline empty: { () }

