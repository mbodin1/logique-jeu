
(* Three-valued colors. *)
type color =
  | Red
  | Green
  | Blue

(* Predicates are just about checking whether a variable has a given color. *)
type predicate_val =
  | PColor of color

(* Predicate parameterised by a type. *)
type 't predicate = {
  pred_type : 't ;
  pred_val : predicate_val
}

type 't variable = {
  var_type : 't ;
  var_name : string
}

(* The type of accepted formula from the user.
  They are parameterised by the type of variables, predicates, and formula variables. *)
type ('var_t, 'pred_t, 'fvar_t) t =
  | True
  | False
  | And of ('var_t, 'pred_t, 'fvar_t) t list
  | Or of ('var_t, 'pred_t, 'fvar_t) t list
  | Predicate of 'pred_t predicate * 'var_t variable list
  | Not of ('var_t, 'pred_t, 'fvar_t) t
  | Forall of 'var_t variable * ('var_t, 'pred_t, 'fvar_t) t
  | Exists of 'var_t variable * ('var_t, 'pred_t, 'fvar_t) t
  | Variable of 'fvar_t variable * ('var_t variable, ('var_t, 'pred_t, 'fvar_t) t) Either.t list (* Parameterised formula variable and its arguments. *)
  | LetIn of 'fvar_t variable * ('var_t variable, 'fvar_t variable) Either.t list * ('var_t, 'pred_t, 'fvar_t) t
           * ('var_t, 'pred_t, 'fvar_t) t (* let P a b c = Phi in Psi. *)

(* Types of variables. *)
type tvar =
  | Color
  | Bool

(* Types of predicates, given by the types of its arguments. *)
type tpred = tvar list

(* Type of the parameter of a parameterised formula (in a let-in). *)
type tparam =
  | Formula
  | Var of tvar

(* Types of parameterised formula, as the types of its arguments. *)
type tpformula = tparam list


(* The type of typed formula. *)
type tformula = (tvar, tpred, tpformula) t

