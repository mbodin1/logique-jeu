
open Functors
open Ngraph
open Geometry

(* Safe distance between two nodes. *)
let node_radius = 2. *. Sizes.node *. sqrt 2.

(* Safe distance between two arcs. *)
let arc_width = 2. *. Sizes.arc

(* For the simulated annealing to work, infinite values should be avoided.
  This constant state the maximum cost to appear in places where infinite values
  may appear (typically because of a division). *)
let max_cost = 1e10

(* Distance that points hops when randomly changing the state. *)
let hop_distance = 3. *. Sizes.node

(* Same than hop_distance, but for an island's node. *)
let hop_distance_island = Sizes.node

(* Change in curvature when changing an arc. *)
let curvature_change = 1.

(* Same than curvature_change, but for an island's arc. *)
let curvature_change_island = 1.


(* Returns a random element from an array. *)
let random_array a =
  assert (Array.length a <> 0) ;
  a.(Random.int (Array.length a))

(* Returns a random element from a list. *)
let random_list l =
  let len = List.length l in
  assert (len <> 0) ;
  List.nth l (Random.int len)


type settings = (string * float) list

(* Caching some useful informations for the cost evaluation.
  This cache won't evolve during computation: it's typically reverse maps.
  The reverse map of constraints is such that if id is associated to the constraint Symmetric id',
  then id' is mapped to (id, Symmetric id'). *)
type 'a with_cache = {
  ct : 'a (* Base content. *) ;
  reverse_constraint : (id * node_constraint) list IMap.t (* The reverse map of constraints. *)
}

(* We define a criterium as a cost function and a weight. *)
type ('state, 'change) criterium = {
  name : string (* Name of the heuristic, for the user to update it. *) ;
  cost_function : 'state -> float (* Cost function of a position. *) ;
  cost_change : 'state -> 'change -> Annealing.cost_change (* Cost function of a change. *) ;
  weight : float (* Multiplicative factor of the two fonctions above. *)
}

(* Given the list of default settings and the list of actually provided settings,
  filter the criteriums for the simulated annealing and provide the associated
  costs and costs_change functions. *)
let get_criteriums criteriums default_settings settings =
  let criteriums =
    let existing = SSet.of_list (List.map fst default_settings) in
    let given = SSet.of_list (List.map fst settings) in
    let unknown = SSet.diff given existing in
    if not (SSet.is_empty unknown) then
      invalid_arg ("Unknown criteriums: " ^ String.concat ", " (SSet.to_list unknown))
    else
      let settings = SMap.of_list settings in
      List.filter_map (fun c ->
        Option.bind (SMap.find_opt c.name settings) (fun w ->
          if w = 0. then None
          else Some { c with weight = w })) criteriums in
  (List.map (fun c g -> c.weight *. c.cost_function g) criteriums,
   List.map (fun c g change ->
     Annealing.multiply_cost_change c.weight (c.cost_change g change)) criteriums)


module Node = struct

(* Perform the actual positionning of regular_conditions. *)
let regular_conditions_from_conds conds =
  let n = List.length conds in
  List.init n (fun i -> Float.of_int (1 + i) /. Float.of_int (n + 1))

(* Placing each conditions regularly along the arc. *)
let regular_conditions id1 id2 g =
  match IIMap.find_opt (id1, id2) g.arc_conditions with
  | None -> assert false
  | Some conds -> regular_conditions_from_conds conds

let circle g =
  let num = IMap.cardinal g.infos in
  let increment = 2. *. Float.pi /. Float.of_int num in
  let radius =
    let p1 = polar { distance = 1. ; angle = 0. } in
    let p2 = polar { distance = 1. ; angle = increment } in
    let d = sqrt (sq_distance p1 p2) in
    let d = max 1e-4 d in
    node_radius /. d in
  let (positions, _num) =
    IMap.fold (fun id _info (positions, n) ->
      let pos =
        polar {
          distance = radius ;
          angle = increment *. Float.of_int n -. Float.pi /. 2.
        } in
      let positions = IMap.add id pos positions in
      (positions, n + 1)) g.infos (IMap.empty, 0) in
  let arcs =
    GGraph.fold_edges (fun v1 v2 arcs ->
      let id1 = GGraph.V.label v1 in
      let id2 = GGraph.V.label v2 in
      let curvature =
        if id1 + 1 = id2 then
          -1. /. radius (* We draw the arc centered at the circle center. *)
        else if id2 + 1 = id1 then
          1. /. radius
        else 0. in
      let conds = regular_conditions id1 id2 g in
      IIMap.add (id1, id2) (curvature, conds) arcs) g.graph IIMap.empty in
  {
    pgraph = g ;
    positions ;
    arcs
  }

let line g =
  let size = IMap.cardinal g.infos in
  (* An array storing lists of pairs of (int * bool), each representing a link from/to
    this array's index (from in the first element, to in the second).
    The boolean indicates a side (top/bottom). *)
  let data = Array.make size ([], []) in
  (* A map associating for each node its index within the data array. *)
  let (indices, _free_index) =
    IMap.fold (fun id _info (indices, free_index) ->
      assert (not (IMap.mem id indices)) ;
      (IMap.add id free_index indices, free_index + 1)) g.infos (IMap.empty, 0) in
  let fetch id =
    match IMap.find_opt id indices with
    | None -> assert false
    | Some i -> i in
  let data =
    GGraph.fold_edges (fun v1 v2 data ->
      let id1 = GGraph.V.label v1 in
      let id2 = GGraph.V.label v2 in
      let i1 = fetch id1 in
      let i2 = fetch id2 in
      let dir =
        let (i1, i2) = if i1 > i2 then (i2, i1) else (i1, i2) in
        let count =
          (* All the elements strictly between i1 and i2. *)
          let between =
            if i1 = i2 then []
            else List.init (i2 - i1 - 1) (fun i -> i + i1 + 1) in
          List.fold_left (fun count i ->
            let (f, t) = data.(i) in
            (* We extract all the arrows coming or going outside the range. *)
            let l = List.filter (fun (i', _dir) -> i' < i1 || i' > i2) (f @ t) in
            List.fold_left (fun count (_i, dir) ->
              let d = if dir then 1 else -1 in
              count + d) count l) 0 between in
        if count > 0 then
          false (* More crossing in the top (true) direction than bottom. *)
        else if count < 0 then
          true
        else Random.bool () in
      data.(i1) <- (let (f, t) = data.(i1) in (f, (i2, dir) :: t)) ;
      data.(i2) <- (let (f, t) = data.(i2) in ((i1, dir) :: f, t)) ;
      data) g.graph data in
  let pos i = Float.of_int i *. node_radius in
  {
    pgraph = g ;
    positions =
      IMap.map (fun i -> { x = pos i ; y = 0. }) indices ;
    arcs =
      GGraph.fold_edges (fun v1 v2 arcs ->
        let id1 = GGraph.V.label v1 in
        let id2 = GGraph.V.label v2 in
        let i1 = fetch id1 in
        let i2 = fetch id2 in
        let dir =
          let (_f, t) = data.(i1) in
          match List.find_opt (fun (i, _dir) -> i = i2) t with
          | None -> assert false
          | Some (_i2, dir) -> dir in
        let curve = 1. /. (pos i2 -. pos i1) in
        let curve = if dir then curve else -.curve in
        let conds = regular_conditions id1 id2 g in
        IIMap.add (id1, id2) (curve, conds) arcs) g.graph IIMap.empty
  }

(* An internal type for the tree positionning function to represent trees. *)
type tree =
  | TNode of Ngraph.id * tree list

(* Recognise a node without any child. *)
let is_leaf = function
  | TNode (_, []) -> true
  | _ -> false

let tree g =
  (* Convert the graph into a tree. *)
  let rec to_tree seen id =
    let info =
      match IMap.find_opt id g.infos with
      | None -> assert false
      | Some info -> info in
    let succs = GGraph.succ g.graph info.vertex in
    let succs = List.map GGraph.V.label succs in
    let succs = List.filter (fun id -> not (ISet.mem id seen)) succs in
    let seen = ISet.union seen (ISet.of_list succs) in
    let (l, seen) =
      List.fold_left (fun (l, seen) id ->
        let (t, seen) = to_tree seen id in
        (t :: l, seen)) ([], seen) succs in
    (TNode (id, l), seen) in
  let (t, _seen) = to_tree (ISet.singleton g.start) g.start in
  let origin = { x = 0. ; y = 0. } in
  (* Position the provided subtree starting from the origin,
    returning the positions of each subnodes, as well as the total width. *)
  let rec positionnate = function
    | TNode (id, l) ->
      let l = List.map (fun t -> (is_leaf t, positionnate t)) l in
      let margin = node_radius /. 2. in
      let current_margin = if l = [] then margin else 0. in
      let width =
        List.fold_left (fun width (leaf, (_pos, w)) ->
          width +. (if leaf then margin else 0.) +. w) current_margin l in
      let positions = IMap.empty in
      let positions = IMap.add id origin positions in
      let shift = (current_margin -. width) /. 2. in
      let (positions, _current_x) =
        List.fold_left (fun (positions, x) (leaf, (pos, w)) ->
          let m = if leaf then margin /. 2. else 0. in
          let pos =
            IMap.map (fun p ->
              add_vector p { x = x +. m +. w /. 2. ; y = node_radius }) pos in
          let positions =
            IMap.union (fun _id _p _p' -> assert false) positions pos in
          (positions, x +. 2. *. m +. w)) (positions, shift) l in
      (positions, width) in
  let (positions, _width) = positionnate t in
  {
    pgraph = g ;
    positions ;
    arcs = IIMap.map (fun conds -> (0., regular_conditions_from_conds conds)) g.arc_conditions
  }


(* The changes we are considering to the state of positions. *)
type change =
  | Node of id * vector (* Position change onto the node id. *)
  | Arc of (id * id) * float (* Change of curvature of the arc between the two nodes. *)

(* The state of the simulated annealing. *)
type state = tposition with_cache

(* Constructor for criterium. *)
let build name weight (cost : state -> float) cost_change = {
    name ;
    weight ;
    cost_function = cost ;
    cost_change
  }

(* Given a constraint, get the list of ids appearing in it. *)
let id_appearing_in = function
  | Symmetric id -> [id]
  | LeftOf id -> [id]
  | Below id -> [id]

(* Fetch the constraints of id that mention id'. *)
let fetch_constraints g id id' =
  let info =
    match IMap.find_opt id g.ct.infos with
    | None -> assert false
    | Some info -> info in
  List.filter (fun c -> List.mem id' (id_appearing_in c)) info.constraints

(* Initialise the cache. *)
let init_cache g =
  let reverse_constraint =
    IMap.fold (fun id info m ->
      (* For each id, fetch the id' appearing within its associated constraints. *)
      List.fold_left (fun m c ->
        let ids' = id_appearing_in c in
        let ids' = List.sort_uniq compare ids' in
        List.fold_left (fun m id' ->
          IMap.add_to_list id' (id, c) m) m ids') m info.constraints) g.pgraph.infos IMap.empty in
  {
    ct = g ;
    reverse_constraint
  }

(* Wrap the non-moving parts of the state (that are the cache and the graph without positionning.) *)
let stationnary g = {
    ct = g.ct.pgraph ;
    reverse_constraint = g.reverse_constraint
  }

(* Fold over the unordered pairs of id/values within a map.
  Takes the associated Map.fold and Map.split module as argument. *)
let fold_diagonal fold split f m acc =
  fold (fun id v acc ->
    (* As we consider unordered pairs, we only consider the elements from this point. *)
    let (_lower_m, data, m) = split id m in
    assert (data <> None) ;
    fold (fun id' v' acc ->
      f (id, v) (id', v') acc) m acc) m acc

(* Adding it into IMap and IIMap. *)
module IMap = struct
  include IMap
  let fold_diagonal = fold_diagonal fold split
end
module IIMap = struct
  include IIMap
  let fold_diagonal = fold_diagonal fold split
end

(* Fold over all the arcs starting from node id. *)
let fold_arcs_starting g id acc f =
  let info =
    match IMap.find_opt id g.ct.pgraph.infos with
    | None -> assert false
    | Some info -> info in
  let vertex = info.vertex in
  let succs = GGraph.succ g.ct.pgraph.graph vertex in
  List.fold_left (fun acc vertex_succ ->
    let id_succ = GGraph.V.label vertex_succ in
    f acc id_succ) acc succs

(* Fold over all the arcs ending from node id. *)
let fold_arcs_ending g id acc f =
  let info =
    match IMap.find_opt id g.ct.pgraph.infos with
    | None -> assert false
    | Some info -> info in
  let vertex = info.vertex in
  let preds = GGraph.pred g.ct.pgraph.graph vertex in
  List.fold_left (fun acc vertex_pred ->
    let id_pred = GGraph.V.label vertex_pred in
    f acc id_pred) acc preds

(* Criterium that apply to all (unordered) pairs of nodes, ignoring its arcs.
  The cost_pair function takes the id and position of two distinct nodes. *)
let build_node_pair name weight cost_pair =
  let cost_pair g id_pos id_pos' = cost_pair (stationnary g) id_pos id_pos' in
  let cost_function g =
    IMap.fold_diagonal (fun (id, pos) (id', pos') acc ->
      acc +. cost_pair g (id, pos) (id', pos')) g.ct.positions 0. in
  let cost_change g = function
    | Node (id, d) ->
      let old_pos =
        match IMap.find_opt id g.ct.positions with
        | None -> assert false
        | Some pos -> pos in
      let new_pos = add_vector old_pos d in
      (* Itering over all nodes except the current one. *)
      let positions = IMap.remove id g.ct.positions in
      Annealing.Cost_change
        (IMap.fold (fun id' pos' acc ->
          acc +. cost_pair g (id', pos') (id, new_pos)
              -. cost_pair g (id', pos') (id, old_pos)) positions 0.)
    | Arc _ -> Annealing.Cost_change 0. in
  build name weight cost_function cost_change

(* Criterium that apply to specific (unordered) pairs of nodes, ignoring all arcs.
  The associated function states which node is associated to another. It must be
  symmetric and its elements must be unique in the returning list.
  The cost_pair function takes the id and position of two distinct nodes. *)
let build_node_pair_specific name weight associated cost_pair =
  let associated g id = associated (stationnary g) id in
  let cost_pair g id_pos id_pos' = cost_pair (stationnary g) id_pos id_pos' in
  let cost_function g =
    IMap.fold (fun id pos acc ->
      let ids' = associated g id in
      let ids' = List.filter (fun id' -> id <= id') ids' in
      List.fold_left (fun acc id' ->
        let pos' =
          match IMap.find_opt id' g.ct.positions with
          | None -> assert false
          | Some pos -> pos in
        acc +. cost_pair g (id, pos) (id', pos')) acc ids') g.ct.positions 0. in
  let cost_change g = function
    | Node (id, d) ->
      let old_pos =
        match IMap.find_opt id g.ct.positions with
        | None -> assert false
        | Some pos -> pos in
      let new_pos = add_vector old_pos d in
      let ids' = associated g id in
      Annealing.Cost_change
        (List.fold_left (fun acc id' ->
          if id = id' then
            (acc +. cost_pair g (id, new_pos) (id, new_pos)
                 -. cost_pair g (id, old_pos) (id, old_pos))
          else
            let pos' =
              match IMap.find_opt id' g.ct.positions with
              | None -> assert false
              | Some pos -> pos in
            (acc +. cost_pair g (id', pos') (id, new_pos)
                 -. cost_pair g (id', pos') (id, old_pos))) 0. ids')
    | Arc _ -> Annealing.Cost_change 0. in
  build name weight cost_function cost_change

(* Criterium that rely on the maximum and minimum values of a projection of nodes,
   ignoring arcs.
  The cost_min_max function takes the minimum and maximum values of the proj function
  over all nodes. *)
let build_node_min_max name weight proj cost_min_max =
  let proj g id pos = proj (stationnary g) id pos in
  let cost_min_max g minv maxv = cost_min_max (stationnary g) minv maxv in
  let cost_function g =
    let (minv, maxv) =
      let acc =
        match IMap.choose_opt g.ct.positions with
        | None -> assert false
        | Some (id, pos) ->
          let v = proj g id pos in
          (v, v) in
      IMap.fold (fun id pos (minv, maxv) ->
        (min minv (proj g id pos),
         max maxv (proj g id pos))) g.ct.positions acc in
    cost_min_max g minv maxv in
  let cost_change g = function
    | Arc _ -> Annealing.Cost_change 0.
    | Node (id, d) ->
      let pos =
        match IMap.find_opt id g.ct.positions with
        | None -> assert false
        | Some pos -> pos in
      let ppos = proj g id pos in
      let pos' = add_vector pos d in
      let ppos' = proj g id pos' in
      (* To avoid refetching all nodes, we try with the nodes just before and after (if
        they exist): if nodes are randomized, there is a good chance to have the current
        node between the other two. Of course, they are usually not randomized, but ordered
        in a way that this makes this probability worth trying it first anyway, before the
        more costly operation. *)
      let fetch id' = IMap.find_opt id' g.ct.positions in
      let id1 = id - 1 in
      let id2 = id + 1 in
      match fetch id1, fetch id2 with
      | Some pos1, Some pos2 ->
        let ppos1 = proj g id1 pos1 in
        let ppos2 = proj g id2 pos2 in
        if (ppos1 < ppos && ppos < ppos2 && ppos1 < ppos' && ppos' < ppos2)
           || (ppos1 > ppos && ppos > ppos2 && ppos1 > ppos' && ppos' > ppos2) then
          (* The current projection is in between the two other coordinates, and stays this
            way after the change: we know that the minimum and maximum values did not change. *)
          Annealing.Cost_change 0.
        else Annealing.Dont_know
      | _, _ -> Annealing.Dont_know in
  build name weight cost_function cost_change

(* Same than build_node_min_max, but with a list of projections. *)
let build_node_min_max_list name weight projs cost_min_max =
  let projs = List.map (fun f g id pos -> f (stationnary g) id pos) projs in
  let cost_min_max g min_max_l = cost_min_max (stationnary g) min_max_l in
  let cost_function g =
    let min_max_l =
      let acc =
        match IMap.choose_opt g.ct.positions with
        | None -> assert false
        | Some (id, pos) ->
          List.map (fun f ->
            let v = f g id pos in
            (v, v)) projs in
      IMap.fold (fun id pos min_max_l ->
        List.map2 (fun (minv, maxv) f ->
          (min minv (f g id pos),
           max maxv (f g id pos))) min_max_l projs) g.ct.positions acc in
    cost_min_max g min_max_l in
  let cost_change g = function
    | Arc _ -> Annealing.Cost_change 0.
    | Node (id, d) ->
      let pos =
        match IMap.find_opt id g.ct.positions with
        | None -> assert false
        | Some pos -> pos in
      let ppos = List.map (fun f -> f g id pos) projs in
      let pos' = add_vector pos d in
      let ppos' = List.map (fun f -> f g id pos') projs in
      (* To avoid refetching all nodes, we try with the nodes just before and after (if
        they exist): if nodes are randomized, there is a good chance to have the current
        node between the other two (in all projections), sparing the large computation above. *)
      let fetch id' = IMap.find_opt id' g.ct.positions in
      let id1 = id - 1 in
      let id2 = id + 1 in
      match fetch id1, fetch id2 with
      | Some pos1, Some pos2 ->
        let ppos1 = List.map (fun f -> f g id1 pos1) projs in
        let ppos2 = List.map (fun f -> f g id2 pos2) projs in
        if List.for_all4 (fun ppos ppos' ppos1 ppos2 ->
              (ppos1 < ppos && ppos < ppos2 && ppos1 < ppos' && ppos' < ppos2)
              || (ppos1 > ppos && ppos > ppos2 && ppos1 > ppos' && ppos' > ppos2)
            ) ppos ppos' ppos1 ppos2 then
          (* No maximum or minimum has been changed. *)
          Annealing.Cost_change 0.
        else Annealing.Dont_know
      | _, _ -> Annealing.Dont_know in
  build name weight cost_function cost_change

(* Criterium that apply to all arcs, ignoring all node changes, including when touching its
  endings. *)
let build_arc_only name weight cost_arc =
  let cost_arc g id1 id2 curve = cost_arc (stationnary g) id1 id2 curve in
  let cost_function g =
    IIMap.fold (fun (id1, id2) (curve, _conds) acc ->
      acc +. cost_arc g id1 id2 curve) g.ct.arcs 0. in
  let cost_change g = function
    | Arc ((id1, id2), dcurve) ->
      let curve =
        match IIMap.find_opt (id1, id2) g.ct.arcs with
        | None -> assert false
        | Some (curve, _conds) -> curve in
      let curve' = curve +. dcurve in
      Annealing.Cost_change
        (cost_arc g id1 id2 curve'
         -. cost_arc g id1 id2 curve)
    | Node (_id, _d) -> Annealing.Cost_change 0. in
  build name weight cost_function cost_change

(* Criterium that apply to all arcs, ignoring node change except when touching its endings.
  The cost_arc function gets the identifiers and position of both endings, as well as the
  curvature. *)
let build_arc name weight cost_arc =
  let cost_arc g id1 id2 curve = cost_arc (stationnary g) id1 id2 curve in
  let id_pos g id =
    match IMap.find_opt id g.ct.positions with
    | None -> assert false
    | Some pos -> (id, pos) in
  let cost_arc_pos g id1 id2 curve =
    cost_arc g (id_pos g id1) (id_pos g id2) curve in
  let cost_function g =
    IIMap.fold (fun (id1, id2) (curve, _conds) acc ->
      acc +. cost_arc_pos g id1 id2 curve) g.ct.arcs 0. in
  let cost_change g = function
    | Arc ((id1, id2), dcurve) ->
      let curve =
        match IIMap.find_opt (id1, id2) g.ct.arcs with
        | None -> assert false
        | Some (curve, _conds) -> curve in
      let curve' = curve +. dcurve in
      Annealing.Cost_change
        (cost_arc_pos g id1 id2 curve'
         -. cost_arc_pos g id1 id2 curve)
    | Node (id, d) ->
      let old_pos =
        match IMap.find_opt id g.ct.positions with
        | None -> assert false
        | Some pos -> pos in
      let new_pos = add_vector old_pos d in
      let get_curve id1 id2 =
        match IIMap.find_opt (id1, id2) g.ct.arcs with
        | None -> assert false
        | Some (curve, _conds) -> curve in
      let cost_arc (id1, pos1) (id2, pos2) =
        cost_arc g (id1, pos1) (id2, pos2) (get_curve id1 id2) in
      Annealing.Cost_change
        (fold_arcs_starting g id 0. (fun acc id_succ ->
          assert (id <> id_succ) ;
          let id_pos_succ = id_pos g id_succ in
          acc +. cost_arc (id, new_pos) id_pos_succ
              -. cost_arc (id, old_pos) id_pos_succ)
         +. fold_arcs_ending g id 0. (fun acc id_pred ->
              assert (id <> id_pred) ;
              let id_pos_pred = id_pos g id_pred in
              acc +. cost_arc id_pos_pred (id, new_pos)
                  -. cost_arc id_pos_pred (id, old_pos))) in
  build name weight cost_function cost_change

(* Criterium that apply to all (unordered) pairs of arcs, ignoring node change
  except when touching their endings.
  The cost_arc function gets the identifiers and position of both endings as well as the
  curvature, for both arcs. *)
let build_arc_pair name weight cost_pair =
  let cost_pair g arc1 arc2 = cost_pair (stationnary g) arc1 arc2 in
  let id_pos g id =
    match IMap.find_opt id g.ct.positions with
    | None -> assert false
    | Some pos -> (id, pos) in
  let cost_pair_pos g (id1, id2, curve) (id1', id2', curve') =
    cost_pair g (id_pos g id1, id_pos g id2, curve) (id_pos g id1', id_pos g id2', curve') in
  let cost_function g =
    IIMap.fold_diagonal (fun ((id1, id2), (curve, _conds)) ((id1', id2'), (curve', _conds')) acc ->
      acc +. cost_pair_pos g (id1, id2, curve) (id1', id2', curve')) g.ct.arcs 0. in
  let cost_change g = function
    | Arc ((id1, id2), dcurve) ->
      let old_curve =
        match IIMap.find_opt (id1, id2) g.ct.arcs with
        | None -> assert false
        | Some (curve, _conds) -> curve in
      let new_curve = old_curve +. dcurve in
      let id1_pos = id_pos g id1 in
      let id2_pos = id_pos g id2 in
      (* Itering over all the arcs except the current one. *)
      let arcs = IIMap.remove (id1, id2) g.ct.arcs in
      Annealing.Cost_change
        (IIMap.fold (fun (id1', id2') (curve', _conds') acc ->
          let arc' = (id_pos g id1', id_pos g id2', curve') in
          acc +. cost_pair g arc' (id1_pos, id2_pos, new_curve)
              -. cost_pair g arc' (id1_pos, id2_pos, old_curve)) arcs 0.)
    | Node (id, d) ->
      let old_pos =
        match IMap.find_opt id g.ct.positions with
        | None -> assert false
        | Some pos -> pos in
      let new_pos = add_vector old_pos d in
      let get_curve id1 id2 =
        match IIMap.find_opt (id1, id2) g.ct.arcs with
        | None -> assert false
        | Some (curve, _conds) -> curve in
      (* Update id_pos to take this change into account.
        If the boolean before is true, then id is associated to old_pos, otherwise to new_pos. *)
      let id_pos before id' =
        if id' = id then
          (id', if before then old_pos else new_pos)
        else id_pos g id' in
      let changed_arcs =
        let acc = [] in
        let acc =
          fold_arcs_starting g id acc (fun acc id_succ ->
            assert (id <> id_succ) ;
            let id_pos_succ = id_pos true id_succ in
            (((id, old_pos), id_pos_succ), ((id, new_pos), id_pos_succ)) :: acc) in
        let acc =
          fold_arcs_ending g id acc (fun acc id_pred ->
            assert (id <> id_pred) ;
            let id_pos_pred = id_pos true id_pred in
            ((id_pos_pred, (id, old_pos)), (id_pos_pred, (id, new_pos))) :: acc) in
        acc in
      (* Itering over all the identified arcs, times all the other arcs, except the current one. *)
      Annealing.Cost_change
        (List.fold_left (fun acc (((id1, pos1), (id2, pos2)), ((id1', pos1'), (id2', pos2'))) ->
            assert (id1 = id1' && id2 = id2') ;
            let curve = get_curve id1 id2 in
            let arc_before = ((id1, pos1), (id2, pos2), curve) in
            let arc_after = ((id1, pos1'), (id2, pos2'), curve) in
            let arcs = IIMap.remove (id1, id2) g.ct.arcs in
            IIMap.fold (fun (id1', id2') (curve', _conds') acc ->
              acc +. cost_pair g arc_after (id_pos false id1', id_pos false id2', curve')
                  -. cost_pair g arc_before (id_pos true id1', id_pos true id2', curve')) arcs acc)
          0. changed_arcs) in
  build name weight cost_function cost_change

(* Criterium that apply to all pairs of arcs and node, except the arc and its endings.
  The cost_arc_node function gets the identifiers and position of both endings of the arc
  as well as its curvature, as well as the identifier and position of the node. *)
let build_arc_node name weight cost_arc_node =
  let cost_arc_node g arc id_pos = cost_arc_node (stationnary g) arc id_pos in
  let id_pos g id =
    match IMap.find_opt id g.ct.positions with
    | None -> assert false
    | Some pos -> (id, pos) in
  let cost_function g =
    IIMap.fold (fun (id1, id2) (curve, _conds) acc ->
      let arc = (id_pos g id1, id_pos g id2, curve) in
      let positions = IMap.remove id1 g.ct.positions in
      let positions = IMap.remove id2 positions in
      IMap.fold (fun id pos acc -> acc +. cost_arc_node g arc (id, pos))
        positions acc) g.ct.arcs 0. in
  let cost_change g = function
    | Node (id, d) ->
      let old_pos =
        match IMap.find_opt id g.ct.positions with
        | None -> assert false
        | Some pos -> pos in
      let new_pos = add_vector old_pos d in
      (* We first consider all arcs and their interaction with the node. *)
      let diff_arcs =
        IIMap.fold (fun (id1, id2) (curve, _conds) acc ->
          if id1 = id || id2 = id then acc
          else (
            let arc = (id_pos g id1, id_pos g id2, curve) in
            acc +. cost_arc_node g arc (id, new_pos)
                -. cost_arc_node g arc (id, old_pos)
          )) g.ct.arcs 0. in
      (* We then consider all arcs connected to this node, that thus changed, and their interaction
        with all the other nodes. *)
      let diff_local_arcs =
        let get_curve id1 id2 =
          match IIMap.find_opt (id1, id2) g.ct.arcs with
          | None -> assert false
          | Some (curve, _conds) -> curve in
        let id_pos before id' =
          if id' = id then
            (id', if before then old_pos else new_pos)
          else id_pos g id' in
        let changed_arcs =
          let acc = [] in
          let acc =
            fold_arcs_starting g id acc (fun acc id_succ ->
              assert (id <> id_succ) ;
              let id_pos_succ = id_pos true id_succ in
              (((id, old_pos), id_pos_succ), ((id, new_pos), id_pos_succ)) :: acc) in
          let acc =
            fold_arcs_ending g id acc (fun acc id_pred ->
              assert (id <> id_pred) ;
              let id_pos_pred = id_pos true id_pred in
              ((id_pos_pred, (id, old_pos)), (id_pos_pred, (id, new_pos))) :: acc) in
          acc in
        List.fold_left (fun acc (((id1, pos1), (id2, pos2)), ((id1', pos1'), (id2', pos2'))) ->
            assert (id1 = id1' && id2 = id2') ;
            let curve = get_curve id1 id2 in
            let arc_before = ((id1, pos1), (id2, pos2), curve) in
            let arc_after = ((id1, pos1'), (id2, pos2'), curve) in
            let positions = IMap.remove id1 g.ct.positions in
            let positions = IMap.remove id2 positions in
            IMap.fold (fun id pos acc ->
              (* As we have removed the endings of the current arc in the map, we don't need to
                worry about any coordinate change. *)
              acc +. cost_arc_node g arc_after (id, pos)
                  -. cost_arc_node g arc_before (id, pos)) positions acc)
          0. changed_arcs in
      Annealing.Cost_change (diff_arcs +. diff_local_arcs)
    | Arc ((id1, id2), dcurve) ->
      let old_curve =
        match IIMap.find_opt (id1, id2) g.ct.arcs with
        | None -> assert false
        | Some (curve, _conds) -> curve in
      let new_curve = old_curve +. dcurve in
      let id1_pos = id_pos g id1 in
      let id2_pos = id_pos g id2 in
      (* Itering over all the nodes except the ending of the arc. *)
      let positions = IMap.remove id1 g.ct.positions in
      let positions = IMap.remove id2 positions in
      Annealing.Cost_change
        (IMap.fold (fun id pos acc ->
          acc +. cost_arc_node g (id1_pos, id2_pos, new_curve) (id, pos)
              -. cost_arc_node g (id1_pos, id2_pos, old_curve) (id, pos)) positions 0.) in
  build name weight cost_function cost_change

(* Given a² and b², return an upper bound of (a + b)² which is fast to compute. *)
let add_sq_major a2 b2 =
  (* (a + b)² = a² + b² + 2ab.
    We also have 2ab = 2 sqrt(a²b²) ≤ a² + b²
    (geometric mean is always smaller than arithmetic mean). *)
  2. *. (a2 +. b2)

(* Same, but for three values. *)
let _add_sq_major3 a2 b2 c2 =
  (* (a + b + c)² = a² + b² + c² + 2ab + 2bc + 2ca
    and 2ab ≤ a² + b², 2bc ≤ b² + c², 2ca ≤ c² + a². *)
  3. *. (a2 +. b2 +. c2)

(* The function add_sq_major above works well when the values are relatively close
  to each others.
  If one of the value is little compared to the other, the approximation becomes poor.
  Instead, it may be better to slightly move the points.
  The coordinate move_towards_at_least r d pos is close to pos, but at a distance at least
  d less than r than pos.
  It is meant to be used for small distances. *)
let move_towards_at_least r d pos =
  assert (d >= 0.) ;
  let apply rx r =
    if Float.abs (rx -. r) <= d then rx
    else
      if r > rx then r -. d
      else r +. d in {
    x = apply r.x pos.x ;
    y = apply r.y pos.y
  }

(* Same, but the return is away from r. *)
let move_away_at_least r d pos =
  assert (d >= 0.) ;
  let apply rx r =
    if r > rx then r +. d
    else r -. d in {
    x = apply r.x pos.x ;
    y = apply r.y pos.y
  }

let%test "move towards diminishes distance" =
  let a = polar { distance = 12. ; angle = 42. } in
  let b = polar { distance = 24. ; angle = 18. } in
  let b' = move_towards_at_least a arc_width b in
  sq_distance b b' >= square arc_width
  && sq_distance a b' < sq_distance a b

let%test "move away increases distance" =
  let a = polar { distance = 2. ; angle = 46. } in
  let b = polar { distance = 1. ; angle = 17. } in
  let b' = move_away_at_least a arc_width b in
  sq_distance b b' >= square arc_width
  && sq_distance a b' > sq_distance a b

(* How much a node intersect an arc.
  This function is meant to be called a large number of time, so we first try
  to check whether we can easily disprove the intersection without doing costly
  computations. *)
let how_much_intersect (pos1, pos2, curve) pos =
  let c = middle pos1 pos2 in
  let sq_r = sq_distance c pos1 in
  let pos_closer = move_towards_at_least c arc_width c in
  (* We first check the distance of the node to the circle englobbing the arc.
    If is larger than its radius plus the radius of a node, plus the arc width,
    then they definitively don't intersect. *)
  let d_low = sq_distance c pos_closer in
  if d_low >= add_sq_major sq_r (square node_radius) then 0.
  else
    let l = equidistant pos1 pos2 in
    let proj = project (move_towards_at_least c node_radius pos_closer) l in
    let vect_dir = turn_vector_right (build_vector pos1 pos2) in
    if curve *. scalar_product vect_dir (build_vector c proj) < 0. then
      0. (* The node is in the wrong half of the arc. *)
    else
      (* We filtered out simple cases: we now have to compute the exact values. *)
      let r = sqrt sq_r in
      let sq_out_d = sq_distance c pos in
      let max_dist = r +. arc_width +. node_radius in
      if sq_out_d >= square max_dist then 0.
      else
        let out_d = sqrt sq_out_d in
        let i =
          if curve = 0. then
            (* The curve is actually a straight line. *)
            let proj = project pos l in
            let sq_d = sq_distance c proj in
            if sq_d >= square (node_radius +. arc_width) then 0.
            else ((node_radius +. arc_width) -. sqrt sq_d)
          else
            let radius = Float.abs (1. /. curve) in
            let curve_center =
              let d_c = sqrt (sq_r +. radius) in
              let v = scale (normalise vect_dir) d_c in
              let v = if curve < 0. then opp_vector v else v in
              add_vector c v in
            let d = sq_distance pos curve_center in
            let max_distance = radius +. node_radius +. arc_width in
            let min_distance = Float.max 0. (radius -. node_radius -. arc_width) in
            if d >= square max_distance || d <= square min_distance then 0.
            else
              let d = sqrt d in
              max (Float.abs (d -. max_distance)) (Float.abs (d -. min_distance)) in
        max i (out_d -. max_dist)

(* Whether two arcs intersect.
  As for how_much_intersect, it first tries to easily disprove the intersection
  before doing heavy computations. *)
let arc_intersect (pos1, pos2, curve) (pos1', pos2', curve') =
  (* Middle of pos1/pos2 and pos1'/pos2'. *)
  let c = middle pos1 pos2 in
  let c' = middle pos1' pos2' in
  (* Square of the radius of the two englobing circles. *)
  let sq_r = sq_distance c pos1 in
  let sq_r' = sq_distance c' pos1' in
  (* Square of the distance between c and c'.
    To account for arc width, the actually move c' slightly towards c in the computation:
    if they are still at a safe distance, then they would still be when accounting the arc
    width in the distance. *)
  let dc_low =
    let c' = move_towards_at_least c arc_width c' in
    sq_distance c c' in
  if dc_low >= add_sq_major sq_r sq_r' then
    (* We then have dist(c, c') ≥ dist (id1, id2) / 2 + dist (id1', id2') / 2,
      That is, the circles of respective centers c and c' and diameters d and d'
      do not overlap, even when adding the arc_width margin.
      As the arcs themselves are always within these circles, these two arcs can't overlap. *)
    false
  else
    let check_plausible c pos1 pos2 pos1' pos2' =
      let l = equidistant pos1 pos2 in
      let vect_dir = turn_vector_right (build_vector pos1 pos2) in
      let p1 = project (move_towards_at_least c arc_width pos1') l in
      let p2 = project (move_towards_at_least c arc_width pos2') l in
      let s1 = scalar_product p1 vect_dir in
      let s2 = scalar_product p2 vect_dir in
      if s1 *. s2 <= 0. then true
      else
        true (* TODO *) in
    if check_plausible c pos1 pos2 pos1' pos2'
       && check_plausible c' pos1' pos2' pos1 pos2 then (
      (* Plausibility checks passed: we now have to do the actual computations, however
        costly they are. *)
      if curve = 0. && curve' = 0. then (
        (* Both are straight lines. *)
        let l1 = build_line pos1 pos2 in
        let l2 = build_line pos1' pos2' in
        match intersection l1 l2 with
        | None ->
          (* Parallel lines: we just need to check whether they are at a distance at least
            arc_width appart. *)
          let p = project pos1 l2 in
          sq_distance pos1 p <= square arc_width
        | Some c ->
          (* Intersection: we need to check whether the intersection is within the vectors. *)
          let is_within p1 p2 p =
            let v = build_vector p1 p2 in
            scalar_product v (build_vector p1 p)
            >= -. arc_width *. sqrt (sq_magnitude v) in
          is_within pos1 pos2 c && is_within pos1' pos2' c
      ) else if curve <> 0. && curve' <> 0. then (
        let arc = Geometry.build_arc pos1 pos2 curve in
        let arc' = Geometry.build_arc pos1' pos2' curve' in
        if valid_arc arc && valid_arc arc' then (
          let c = get_arc_center arc in
          let c' = get_arc_center arc' in
          if sq_distance c c' > square arc_width then false
          else
            true (* TODO: Checking the angles with trigonometry. *)
        ) else false (* The arcs are invalid here. *)
      ) else (
        let wlog pos1 pos2 pos1' pos2' curve' =
          (* We can assume that it is pos1/pos2 which is straight. *)
          let arc' = Geometry.build_arc pos1' pos2' curve' in
          if valid_arc arc' then (
            let c' = get_arc_center arc' in
            let line = build_line pos1 pos2 in
            let p = project c' line in
            if sq_distance c' p > square arc_width then false
            else
              true (* TODO: We need to check the angles. *)
          ) else false (* Invalid arc. *) in
        if curve = 0. then wlog pos1 pos2 pos1' pos2' curve'
        else (
          assert (curve' = 0.) ;
          wlog pos1' pos2' pos1 pos2 curve
        )
      )
    ) else false

(* All criteriums we are considering here. *)
let criteriums = [
    (* Criteriums that ensure that the map is well-formed. *)
    (* Curvature should make sense. *)
    build_arc "meaningful curvature" 1000. (fun _g (_id1, pos1) (_id2, pos2) curvature ->
      if curvature <> 0. && not (valid_arc (Geometry.build_arc pos1 pos2 curvature)) then max_cost
      else 0.) ;
    (* Repulsion when too close. *)
    build_node_pair "node repulsion" 1000. (fun _g (id1, pos1) (id2, pos2) ->
      assert (id1 <> id2) ;
      let d = sq_distance pos1 pos2 in
      (* If we reach the threshold of the node radius, we increase the perceived distance. *)
      let d = if d > square node_radius then d *. 4. else d in
      let d = d /. (square node_radius) in
      min max_cost (1. /. d)) ;
    (* Respect of node constraints. *)
    build_node_pair_specific "constraints" 800. (fun g id ->
      let ids' =
        match IMap.find_opt id g.reverse_constraint with
        | Some ids -> List.map fst ids
        | None -> [] in
      List.sort_uniq compare ids') (fun g (id1, pos1) (id2, pos2) ->
        let grade_constraints (id, pos) (id', pos') =
          let cs = fetch_constraints g id id' in
          List.fold_left (fun acc c ->
            acc +.
              match c with
              | Symmetric id'' ->
                assert (id'' = id') ;
                (* This value should be as close as possible to 0. for them to be symmetrical. *)
                square (pos.y +. pos'.y)
              | LeftOf id'' ->
                assert (id'' = id') ;
                if pos.x <= pos'.x then 0.
                else square (pos.x -. pos'.x)
              | Below id'' ->
                assert (id'' = id') ;
                if pos.y <= pos'.y then 0.
                else square (pos.y -. pos'.y)
            ) 0. cs in
        grade_constraints (id1, pos1) (id2, pos2)
        +. grade_constraints (id2, pos2) (id1, pos1)) ;
    (* Criteriums that ensure that the map is readable. *)
    (* Avoid nodes intersecting arcs. *)
    build_arc_node "avoid arcs intersecting nodes" 20.
      (fun _g ((_id1, pos1), (_id2, pos2), curve) (_id, pos) ->
        how_much_intersect (pos1, pos2, curve) pos) ;
    (* Avoid intersecting arcs. *)
    build_arc_pair "avoid arc intersection" 2.
      (fun _g ((_id1, pos1), (_id2, pos2), curve) ((_id1', pos1'), (_id2', pos2'), curve') ->
        if arc_intersect (pos1, pos2, curve) (pos1', pos2', curve') then 1.
        else 0.) ;
    (* Keep the curvature of arcs low. *)
    build_arc_only "low curvature" 2. (fun _g _id1 _id2 curvature -> curvature) ;
    (* Keep the length of arcs small. *)
    build_arc "small arcs" 2. (fun _g (_id1, pos1) (_id2, pos2) curvature ->
      (* The exact value of the length of the arc is
          2 *. asin (curvature *. sqrt (sq_distance pos1 pos2) /. 2.) /. curvature.
          (or sqrt (sq_distance pos1 pos2) when curvature = 0.)
        This is however very expensive computationnaly. So we instead use the
        linear approximation:
          sqrt (sq_distance pos1 pos2) *. (1. +. curvature *. (Float.pi /. 2. - 1.))
        We furthermore square the result as a heuristic cost (small changes are fine,
        but large ones are not). *)
        sq_distance pos1 pos2 *. square (1. +. curvature *. (Float.pi /. 2. -. 1.))
      ) ;
    (* Criteriums that ensure that the map is printable. *)
    (* Keep the bounding box small along the horizontal axis. *)
    build_node_min_max "compress horizontally" 0.5 (fun _g _id { x ; _ } -> x)
      (fun _g minx maxx -> maxx -. minx) ;
    (* Keep the bounding box small along the vertical axis. *)
    build_node_min_max "compress vertically" 0.5 (fun _g _id { y ; _ } -> y)
      (fun _g miny maxy -> maxy -. miny) ;
    (* Nodes close to each other. *)
    build_node_pair "node close" 0. (fun _g (id1, pos1) (id2, pos2) ->
      assert (id1 <> id2) ;
      sq_distance pos1 pos2) ;
    (* Keep the ratio of the bbox square. *)
    build_node_min_max_list "square bbox" 0.
      [(fun _g _id { x ; _ } -> x); (fun _g _id { y ; _ } -> y)]
      (fun _g -> function
        | [(minx, maxx); (miny, maxy)] ->
          let sizex = maxx -. minx in
          let sizey = maxy -. miny in
          assert (sizex >= 0. && sizey >= 0.) ;
          square (sizex -. sizey)
        | _ -> assert false)
  ]

let default_settings =
  List.map (fun c -> (c.name, c.weight)) criteriums

(* Propose a change. *)
let change all_nodes =
  let random_node () = random_array all_nodes in
  let random_arc g =
    let id = random_node () in
    let info =
      match IMap.find_opt id g.pgraph.infos with
      | None -> assert false
      | Some info -> info in
    let vertex = info.vertex in
    let succs = GGraph.succ g.pgraph.graph vertex in
    if succs = [] then None
    else
      let vertex' = random_list succs in
      let id' = GGraph.V.label vertex' in
      Some (id, id') in
  let rec build = function
    | [] -> []
    | (0, _f) :: l -> build l
    | (n, f) :: l -> f :: build ((n - 1, f) :: l) in
  let functions =
    (* Moving a random node. *)
    let move_node factor _g =
      let full_angle = 2. *. Float.pi in
      let hop_distance = factor *. hop_distance in
      let diff =
        polar {
          distance = Random.float hop_distance ;
          angle = Random.float full_angle
        } in
      Some (Node (random_node (), diff)) in
    (* Changing the curvature of a random arc. *)
    let change_curvature factor g =
      Option.bind (random_arc g) (fun (id, id') ->
        let fetch id =
          match IMap.find_opt id g.positions with
          | None -> assert false
          | Some pos -> pos in
        let pos = fetch id in
        let pos' = fetch id' in
        let curve =
          match IIMap.find_opt (id, id') g.arcs with
          | None -> assert false
          | Some (curve, _) -> curve in
        let diff = factor *. curvature_change *. (Random.float 2. -. 1.) in
        let curve' = curve +. diff in
        if curve' = 0. || (valid_arc (Geometry.build_arc pos pos' curve')) then
          Some (Arc ((id, id'), diff))
        else None) in
    (* Set the curvature of an arc to exactly 0. *)
    let reset_curvature _factor g =
      Option.bind (random_arc g) (fun (id, id') ->
        match IIMap.find_opt (id, id') g.arcs with
        | None -> assert false
        | Some (f, _conds) ->
          if f = 0. then
            None (* It is already 0. *)
          else Some (Arc ((id, id'), -.f))) in
    (* Set exact symmetries. *)
    let set_symmetry _factor g =
      let id = random_node () in
      let info =
        match IMap.find_opt id g.pgraph.infos with
        | None -> assert false
        | Some info -> info in
      let fetch id =
        match IMap.find_opt id g.positions with
        | None -> assert false
        | Some pos -> pos in
      let pos = fetch id in
      if info.constraints = [] then None
      else
        match random_list info.constraints with
        | Symmetric id' ->
          let pos' = fetch id' in
          Some (Node (id, { x = pos'.x -. pos.x ; y = -.pos'.y -. pos.y }))
        | LeftOf id' ->
          let pos' = fetch id' in
          if pos.x <= pos'.x then None
          else Some (Node (id, { x = pos'.x -. pos.x ; y = 0. }))
        | Below id' ->
          let pos' = fetch id' in
          if pos.y <= pos'.y then None
          else Some (Node (id, { x = 0. ; y = pos'.y -. pos.y })) in
    build [
        (5, move_node) ;
        (2, change_curvature) ;
        (1, reset_curvature) ;
        (2, set_symmetry)
      ] in
  let functions = Array.of_list functions in
  let rec aux factor g =
    let f = random_array functions in
    match f factor g with
    | None -> aux factor g
    | Some change -> change in
  aux

(* The optimisation is based on a simulated annealing. *)
let optimise ?(settings = default_settings) g =
  let (costs, costs_change) = get_criteriums criteriums default_settings settings in
  let all_nodes = Array.of_list (List.map fst (IMap.bindings g.positions)) in
  let change = change all_nodes in
  let apply g = function
    | Node (id, d) ->
      let change_pos = function
        | None -> assert false
        | Some pos -> Some (add_vector pos d) in
      { g with positions = IMap.update id change_pos g.positions }
    | Arc (ids, d) ->
      let change_arc = function
        | None -> assert false
        | Some (curvature, conds) -> Some (curvature +. d, conds) in
      { g with arcs = IIMap.update ids change_arc g.arcs } in
  let change factor g =
    let change = change factor g.ct in
    ({ g with ct = apply g.ct change }, change) in
  let g = init_cache g in
  (Annealing.cfsimulate costs costs_change change g).ct

end

module Island = struct

let empty g = {
  igraph = g ;
  islands = []
}

(* Create a circle island around a position. *)
let create_island ?(radius = node_radius *. 0.5) center =
  let p1 = add_vector center { x = -.radius ; y = 0. } in
  let p2 = add_vector center { x = radius ; y = 0. } in
  let curve = 1. /. radius in
  [(curve, p1); (curve, p2)]

let isolated g = {
  igraph = g ;
  islands =
    List.filter_map (fun (id, coords) ->
      let info =
        match IMap.find_opt id g.pgraph.infos with
        | None -> assert false
        | Some info -> info in
      match info.kind with
      | And | False | Forall _ ->
        Some (create_island coords)
      | Or | True | Exists _ -> None) (IMap.bindings g.positions)
}

(* All criteriums we are considering here. *)
let criteriums = [
    (* TODO *)
    (* Restrict the number of island node. *)
    (* Keep arc curvature small. *)
    (* Avoid intersection between islands. *)
    (* Make sure that all thieves nodes are within an island and pirates nodes outside, with a margin. *)
    (* Two consecutive arcs should have a small angle at their junctions. *)
  ]

let default_settings =
  List.map (fun c -> (c.name, c.weight)) criteriums

(* Node island identifiers. *)
type iid = {
  island_id : int (* Index of the island within g.islands. *) ;
  node_id : int (* Index of the coordinate within the island list. *)
}

(* Fetch the node coordinate iid, or raise an exception if invalid. *)
let get_iid g iid =
  let island = List.nth g.islands iid.island_id in
  let (_curvature, coords) = List.nth island iid.node_id in
  coords

(* Same as get_iid, but also return the previous and next coordinates,
  as well as the curvatures between them. *)
let get_iid_neighbour g iid =
  let island = List.nth g.islands iid.island_id in
  match iid.node_id with
  | 0 ->
    let (curve, coord) = List.hd island in
    let (curve_next, coord_next) = List.hd (List.tl island) in
    (snd (List.last island), curve, coord, curve_next, coord_next)
  | n ->
    match List.drop (n - 1) island with
    | (_curve_prev, coord_prev) :: (curve, coord) :: (curve_next, coord_next) :: _ ->
      (coord_prev, curve, coord, curve_next, coord_next)
    | (_curve_prev, coord_prev) :: (curve, coord) :: [] ->
      let (curve_next, coord_next) = List.hd island in
      (coord_prev, curve, coord, curve_next, coord_next)
    | _ -> assert false

(* Retrieve a random valid iid from a tisland. *)
let random_iid g =
  let island_id = Random.int (List.length g.islands) in
  let island = List.nth g.islands island_id in
  let node_id = Random.int (List.length island) in
  { island_id ; node_id }

type change =
  | IslandNode of iid * vector (* Position change onto the island node idd. *)
  | IslandArc of iid * float (* Change of curvature associated to the node. *)
  | AddNode of iid (* Add a node just after the provided node. The node is placed halfway in the arc that follows. *)
  | RemoveNode of iid (* Removing the provided island node. *)
  | CreateIsland of coordinate (* Create a new island around this position. *)
  | RemoveIsland of int (* Remove the n-th island. *)
  | MergeIslands of int * int (* Merge both islands. *)

(* The optimisation is based on a simulated annealing. *)
let optimise ?(settings = default_settings) g =
  let (costs, costs_change) = get_criteriums criteriums default_settings settings in
  (* Return a random position from the node positions of the (non-island) graph. *)
  let random_node_coord =
    let all_coords = Array.of_list (List.map snd (IMap.bindings g.igraph.positions)) in
    fun () -> random_array all_coords in
  let change =
    let rec build = function
      | [] -> []
      | (0, _f) :: l -> build l
      | (n, f) :: l -> f :: build ((n - 1, f) :: l) in
    (* Moving a random node. *)
    let move_node factor g =
      if g.islands = [] then None
      else
        let full_angle = 2. *. Float.pi in
        let hop_distance = factor *. hop_distance_island in
        let diff =
          polar {
            distance = Random.float hop_distance ;
            angle = Random.float full_angle
          } in
        Some (IslandNode (random_iid g, diff)) in
    (* Change the curvature of a random arc. *)
    let change_curvature factor g =
      if g.islands = [] then None
      else
        let iid = random_iid g in
        let amount = factor *. curvature_change_island *. (Random.float 2. -. 1.) in
        let island = List.nth g.islands iid.island_id in
        let previous =
          if iid.node_id = 0 then snd (List.last island)
          else snd (List.nth island (iid.node_id - 1)) in
        let (curve, next) = List.nth island iid.node_id in
        let curve' = curve +. amount in
        if curve' = 0. || (valid_arc (Geometry.build_arc previous next curve')) then
          Some (IslandArc (iid, amount))
        else None in
    (* Add a random node. *)
    let add_node _factor g =
      if g.islands = [] then None
      else (
        let iid = random_iid g in
        let island = List.nth g.islands iid.island_id in
        let previous =
          if iid.node_id = 0 then snd (List.last island)
          else snd (List.nth island (iid.node_id - 1)) in
        let (_curve, next) = List.nth island iid.node_id in
        if previous = next then None
        else Some (AddNode iid)
      ) in
    (* Remove a random node. *)
    let remove_node _factor g =
      if g.islands = [] then None
      else
        let iid = random_iid g in
        let island = List.nth g.islands iid.island_id in
        (* We only remove nodes if the corresponding island has at least 3 nodes. *)
        if List.compare_length_with island 2 > 0 then
          Some (RemoveNode iid)
        else None in
    (* Add an island around a random node. *)
    let add_island _factor _g =
      Some (CreateIsland (random_node_coord ())) in
    (* Remove an island. *)
    let remove_island _factor g =
      if g.islands = [] then None
      else Some (RemoveIsland (Random.int (List.length g.islands))) in
    (* Merge two islands. *)
    let merge_islands _factor g =
      let nb_islands = List.length g.islands in
      if nb_islands = 0 then None
      else
        let island1 = Random.int nb_islands in
        let island2 = Random.int nb_islands in
        if island1 = island2 then None
        else Some (MergeIslands (island1, island2)) in
    let functions =
      build [
          (5, move_node) ;
          (3, change_curvature) ;
          (2, add_node) ;
          (2, remove_node) ;
          (1, add_island) ;
          (1, remove_island) ;
          (1, merge_islands)
        ] in
    let functions = Array.of_list functions in
    let rec aux factor g =
      let f = random_array functions in
      match f factor g with
      | None -> aux factor g
      | Some change -> change in
    aux in
  let apply g = function
    | IslandNode (iid, d) ->
      let update (curve, pos) = (curve, add_vector pos d) in
      let update_island =
        List.apply_to_nth update iid.node_id in
      { g with islands = List.apply_to_nth update_island iid.island_id g.islands }
    | IslandArc (iid, d) ->
      let update (curve, pos) = (curve +. d, pos) in
      let update_island =
        List.apply_to_nth update iid.node_id in
      { g with islands = List.apply_to_nth update_island iid.island_id g.islands }
    | AddNode iid ->
      let update_island island =
        let previous =
          if iid.node_id = 0 then snd (List.last island)
          else snd (List.nth island (iid.node_id - 1)) in
        let (curve, next) = List.nth island iid.node_id in
        assert (previous <> next) ;
        let pos =
          let arc = Geometry.build_arc previous next curve in
          if curve = 0. || not (valid_arc arc) then
            middle previous next
          else point_on_arc arc 0.5 in
        List.add_at_nth (curve, pos) iid.node_id island in
      { g with islands = List.apply_to_nth update_island iid.island_id g.islands }
    | RemoveNode iid ->
      let update_island =
        List.remove_nth iid.node_id in
      { g with islands = List.apply_to_nth update_island iid.island_id g.islands }
    | CreateIsland coords ->
      { g with islands = create_island coords :: g.islands }
    | RemoveIsland id ->
      { g with islands = List.remove_nth id g.islands }
    | MergeIslands (id1, id2) ->
      let island1 = List.nth g.islands id1 in
      let island2 = List.nth g.islands id2 in
      let islands =
        assert (id1 <> id2) ;
        let islands = List.remove_nth (max id1 id2) g.islands in
        let islands = List.remove_nth (min id1 id2) islands in
        islands in
      let island =
        (* TODO: Determine the closest points of both islands to swap them. *)
        island1 @ island2 in
      { g with islands = island :: islands } in
  let change factor g =
    let change = change factor g in
    (apply g change, change) in
  Annealing.cfsimulate costs costs_change change g

end

let%test "disjoint heuristics" =
  let node_names = List.map (fun c -> c.name) Node.criteriums in
  let island_names = List.map (fun c -> c.name) Island.criteriums in
  let names = node_names @ island_names in
  List.length names = List.length (List.sort_uniq compare names)

let default_settings = Node.default_settings @ Island.default_settings

