
(* The degree of precision that we force the tests to have. *)
let epsilon = 1e-6

(* We use a slightly modified version of the square root as there happens
  to be slightly negative values that sometimes pop-up. *)
let sqrt v =
  if v < 0. then
    if v > -.epsilon then 0.
    else invalid_arg (Printf.sprintf "Geometry.sqrt: %g" v)
  else sqrt v

type coordinate = {
  x : float ;
  y : float
}

type polar_coordinate = {
  distance : float ;
  angle : float
}

type vector = coordinate

type line =
  | Vertical of float (* The x coordinate of the line. *)
  | NonVertical of { a : float ; b : float } (* Line of the form y = ax + b. *)

type arc = {
  start : coordinate ;
  target : coordinate ;
  curve : float
}

let test_normal p =
  let check v =
    Float.abs v < 1. /. epsilon in
  check p.x && check p.y

let valid_vector v =
  Float.is_finite v.x && Float.is_finite v.y

(* Normalise an angle between 0. and 2. *. Float.pi. *)
let normalise_angle a =
  let full = 2. *. Float.pi in
  let a = Float.rem a full in
  let a = if a < 0. then a +. full else a in
  assert (a >= 0. && a < full) ;
  a

(* Normalise the angle of a polar coordinate. *)
let _normalise_polar p = { p with angle = normalise_angle p.angle }

let add_positions c1 c2 = {
  x = c1.x +. c2.x ;
  y = c1.y +. c2.y
}

let add_vector = add_positions

let square v = v *. v

let scalar_product v1 v2 =
  v1.x *. v2.x +. v1.y *. v2.y

let sq_magnitude v =
  scalar_product v v

let sq_distance c1 c2 =
  square (c1.x -. c2.x) +. square (c1.y -. c2.y)

let scale v factor = {
  x = v.x *. factor ;
  y = v.y *. factor
}

let middle c1 c2 =
  let average a b = (a +. b) /. 2. in {
    x = average c1.x c2.x ;
    y = average c1.y c2.y
  }

let build_vector c1 c2 = {
  x = c2.x -. c1.x ;
  y = c2.y -. c1.y
}

let opp_vector v = {
  x = -. v.x ;
  y = -. v.y
}

let turn_vector_left v = {
  x = -. v.y ;
  y = v.x
}

let turn_vector_right v =
  opp_vector (turn_vector_left v)

let line_from_vector v c =
  if v.x = 0. then
    Vertical c.x
  else
    let a = v.y /. v.x in
    let b = c.y -. c.x *. a in
    NonVertical { a ; b }

let build_line c1 c2 =
  line_from_vector (build_vector c1 c2) c1

let parallel_by l c =
  match l with
  | Vertical _x -> Vertical c.x
  | NonVertical { a ; _ } ->
    NonVertical { a ; b = c.y -. a *. c.x }

let perpendicular_by l c =
  let l' =
    match l with
    | Vertical _x -> NonVertical { a = 0. ; b = 0. }
    | NonVertical { a ; _ } ->
      if a = 0. then Vertical 0.
      else NonVertical { a = 1. /. a ; b = 0. } in
  parallel_by l' c

let equidistant c1 c2 =
  perpendicular_by (build_line c1 c2) (middle c1 c2)

let project c = function
  | Vertical x -> { c with x }
  | NonVertical { a ; b } ->
    (* Let's shift of (0, -b). *)
    let c' = { c with y = c.y -. b } in
    (* Director vector. *)
    let d = { y = a ; x = 1. } in
    let p = scale d (scalar_product d c' /. sq_magnitude d) in
    (* Shift back. *)
    { p with y = p.y +. b }

let intersection l1 l2 =
  match l1, l2 with
  | Vertical x1, Vertical x2 ->
    if x1 = x2 then Some { x = x1 ; y = 0. }
    else None
  | NonVertical { a ; b }, Vertical x | Vertical x, NonVertical { a ; b } ->
    Some { x ; y = a *. x +. b }
  | NonVertical { a = a1 ; b = b1 }, NonVertical { a = a2 ; b = b2 } ->
    if a1 = a2 then
      if b1 = b2 then
        Some { x = 0. ; y = b1 }
      else None
    else
      let x = -. (b1 -. b2) /. (a1 -. a2) in
      let y = a1 *. x +. b1 in
      Some { x ; y }

let build_arc start target curve = { start ; target ; curve }

let valid_arc arc =
  (arc.curve <> 0. && Float.is_finite arc.curve)
  && (valid_vector arc.start && valid_vector arc.target)
  && (square (1. /. arc.curve) >= sq_distance arc.start arc.target /. 4.)


(* The functions before this comment should avoid using sqrt or trigonometric functions. *)

let polar { distance ; angle } = {
  x = distance *. cos angle ;
  y = distance *. sin angle
}

let to_polar v = {
  distance = sqrt (sq_magnitude v) ;
  angle =
    if v.x = 0. then
      if v.y >= 0. then
        Float.pi /. 2.
      else -.Float.pi /. 2.
    else
      let a = atan (v.y /. v.x) in
      if v.x > 0. then a else (a +. Float.pi)
}

let normalise v =
  scale v (1. /. sqrt (sq_magnitude v))

let move_along_direction p v dist =
  let v = normalise v in
  let v = scale v dist in
  add_vector p v

let turn v rad =
  let c = cos rad in
  let s = sin rad in {
    x = v.x *. c -. v.y *. s ;
    y = v.x *. s +. v.y *. c
  }

let get_arc_center arc =
  assert (valid_arc arc) ;
  let p1 = arc.start in
  let p2 = arc.target in
  let curve = arc.curve in
  let m = middle p1 p2 in
  assert (Float.abs (sq_distance p1 p2 -. 4. *. sq_distance m p1) <= epsilon) ;
  let v = build_vector p1 p2 in
  let v =
    if curve > 0. then
      turn_vector_right v
    else turn_vector_left v in
  let v = normalise v in
  (* Distance to move from the middle of the two points to find the center,
    using the Pythagorean theorem. *)
  let dist = sqrt (square (1. /. curve) -. sq_distance m p1) in
  assert (Float.is_finite dist) ;
  add_vector m (scale v dist)

let point_on_arc arc percentage =
  let c = get_arc_center arc in
  let p1 = arc.start in
  let p2 = arc.target in
  let v1 = build_vector c p1 in
  let v2 = build_vector c p2 in
  let v1 = to_polar v1 in
  let v2 = to_polar v2 in
  let a = v1.angle +. normalise_angle (v2.angle -. v1.angle) *. percentage in
  let v = polar { v1 with angle = a } in
  add_vector c v


let%test "turning square" =
  (* We build a square from a random vector, and check whether we come back to
    the starting point. *)
  let p0 = polar { distance = 10. ; angle = 18. } in
  let v = polar { distance = 5. ; angle = 42. } in
  let p1 = add_vector p0 v in
  let v = turn_vector_left v in
  let p2 = add_vector p1 v in
  let v = turn_vector_left v in
  let p3 = add_vector p2 v in
  let v = turn_vector_left v in
  let p4 = add_vector p3 v in
  sq_distance p0 p4 < epsilon

let%test "turning rectangle" =
  (* This is a similar test, but based on the perpendicular_by and parallel_by
    constructions. *)
  let p0 = polar { distance = 16. ; angle = 64. } in
  let v = polar { distance = 37. ; angle = 29. } in
  let p1 = add_vector p0 v in
  let l01 = build_line p0 p1 in
  let l23 = parallel_by l01 { x = 13. ; y = 14. } in
  let l34 = perpendicular_by l23 p0 in
  let p4 = intersection l34 l01 in
  match p4 with
  | None -> false
  | Some p4 -> sq_distance p0 p4 < epsilon

let%test "intersection at origin" =
  (* We consider two lines with a zero y-intercept: they intercept themselves at the origin. *)
  let l1 = NonVertical { a = 42. ; b = 0. } in
  let l2 = NonVertical { a = 18. ; b = 0. } in
  let orig = { x = 0. ; y = 0. } in
  match intersection l1 l2 with
  | None -> false
  | Some p -> sq_distance orig p < epsilon

let%test "project on x-axis" =
  let axis = build_line { x = 0. ; y = 0. } { x = 18. ; y = 0. } in
  let p = polar { distance = 42. ; angle = 64. } in
  let proj = project p axis in
  sq_distance proj { p with y = 0. } < epsilon

let%test "build_line symmetric" =
  let a = polar { distance = 72. ; angle = 27. } in
  let b = polar { distance = 15. ; angle = 73. } in
  match build_line a b, build_line b a with
  | Vertical x, Vertical x' -> square (x -. x') < epsilon
  | NonVertical { a ; b }, NonVertical { a = a' ; b = b' } ->
    square (a -. a') < epsilon
    && square (b -. b') < epsilon
  | _, _ -> false

let%test "distance middle" =
  (* The middle is equidistant from both points. *)
  let a = polar { distance = 42. ; angle = 18. } in
  let b = polar { distance = 12. ; angle = 74. } in
  let m = middle a b in
  Float.abs (sq_distance a m -. sq_distance b m) < epsilon

let%test "get_arc_center distance" =
  let test arc =
    let c = get_arc_center arc in
    Float.abs (sq_distance c arc.start -. sq_distance c arc.target) < epsilon in
  List.for_all (fun (p1, p2, curve) -> test (build_arc p1 p2 curve)) [
      ({ x = 2. ; y = 3. }, { x = 3. ; y = 2. }, 1.) ;
      ({ x = 2. ; y = 3. }, { x = 3. ; y = 2. }, 0.01) ;
      ({ x = 20. ; y = 1. }, { x = 18. ; y = 2. }, 0.5)
    ]

let%test "point_on_arc distance" =
  let test arc pos =
    let c = get_arc_center arc in
    let p = point_on_arc arc pos in
    Float.abs (sq_distance c p -. sq_distance c arc.target) < epsilon in
  List.for_all (fun pos ->
    List.for_all (fun (p1, p2, curve) -> test (build_arc p1 p2 curve) pos) [
        ({ x = 2. ; y = 3. }, { x = 3. ; y = 2. }, 1.) ;
        ({ x = 2. ; y = 3. }, { x = 3. ; y = 2. }, 0.01) ;
        ({ x = 20. ; y = 1. }, { x = 18. ; y = 2. }, 0.5)
      ]) [0.5 ; 0.1; 0.9]

let%test "polar to_polar" =
  List.for_all (fun p -> sq_distance p (polar (to_polar p)) < epsilon) [
      { x = 42. ; y = 12. } ;
      { x = 0. ; y = 13. } ;
      { x = 1. ; y = 13. } ;
      { x = -5. ; y = 0. } ;
      { x = -5. ; y = -1. } ;
      { x = 7. ; y = -42. }
    ]

