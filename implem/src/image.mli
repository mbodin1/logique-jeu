
(* Inner type to store the main informations before converting it into svg. *)
type t

(* Convert the graph into an image. *)
val of_graph : Ngraph.tisland -> t

(* Add a white transparent layer onto the svg image. *)
val whiten : t -> t

(* As whiten, but only on the background image. *)
val whiten_background : t -> t


(* Produce TikZ code representing the map. *)
val to_tikz : t -> string

(* The module is parameterised by an implementation of Svg signatures in TyXML
  (typically Tyxml itself or Tyxml_js). *)
module Convert (Xml : Xml_sigs.NoWrap) (Svg : Svg_sigs.Make(Xml).T) : sig

(* A type to represent SVG images. *)
type svg = Svg_types.svg Svg.elt

(* Create the final image. *)
val to_image : t -> svg

end

