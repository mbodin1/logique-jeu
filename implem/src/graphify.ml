
open Functors

module GGraph = Graph.Pack.Digraph

module SMap = Map.Make (String)

(* An environment to associated each variable to an identifier. *)
module Env = struct

  type t = int SMap.t * string IMap.t * int

  let empty : t = (SMap.empty, IMap.empty, 0)

  let add : t -> _ -> int * t = fun (m, m_inverse, id) x ->
    let m = SMap.add x.Formula.var_name id m in
    let m_inverse = IMap.add id x.Formula.var_name m_inverse in
    let env' = (m, m_inverse, id + 1) in
    (id, env')

  let get : t -> _ -> int = fun (m, _, _) x ->
    match SMap.find_opt x m with
    | None -> assert false
    | Some id -> id

  let reverse : t -> string IMap.t =
    fun (_, m, _) -> m

end

(* Book a new id. *)
let new_id g =
  let id = g.Ngraph.last_free_id in
  (id, { g with Ngraph.last_free_id = id + 1 })

(* Add a node in the graph with the provided informations. *)
let add_node g id info =
  GGraph.add_vertex g.Ngraph.graph info.Ngraph.vertex ;
  { g with Ngraph.infos = IMap.add id info g.Ngraph.infos }

(* Smart constructor for infos. *)
let infos id subformula kind constraints =
  let open Ngraph in {
    vertex = GGraph.V.create id ;
    subformula ;
    kind ;
    constraints
  }

let build f =
  (* An empty graph. *)
  let empty = Ngraph.{
    start = -1 ;
    last_free_id = 0 ;
    var_names = IMap.empty ;
    graph = GGraph.create () ;
    arc_conditions = IIMap.empty ;
    infos = IMap.empty
  } in
  (* Add the nodes corresponding to an subformula into the graph.
     Also return its entry point as an id, and the corresponding vertex. *)
  let rec inner positivity env g f =
    let (id, g) = new_id g in
    let infos ?(constraints = []) k =
      let f = if positivity then f else Formula.Not f in
      infos id f k constraints in
    let add_node_vertex g id info = (add_node g id info, info.Ngraph.vertex) in
    (* Connect two points provided their vetrices and id. *)
    let connect g ?(conds = []) (v1, id1) (v2, id2) =
      GGraph.add_edge g.Ngraph.graph v1 v2 ;
      let g =
        { g with Ngraph.arc_conditions =
                   IIMap.add (id1, id2) conds g.Ngraph.arc_conditions } in
      g in
    (* Make a node pointing to several subformulas. *)
    let make_node_pointing env id g fs info =
      let (env, vertices, g) =
        List.fold_left (fun (env, vertices, g) f ->
          let (env, id', vertex, g) = inner positivity env g f in
          (env, (vertex, id') :: vertices, g)) (env, [], g) fs in
      let (g, vertex) = add_node_vertex g id info in
      let g =
        List.fold_left (fun g v_id' -> connect g (vertex, id) v_id') g vertices in
      (env, id, vertex, g) in
    match f, positivity with
    | Formula.True, true | Formula.False, false ->
      let info = infos Ngraph.True in
      (env, id, info.Ngraph.vertex, add_node g id info)
    | Formula.False, true | Formula.True, false ->
      let info = infos Ngraph.False in
      (env, id, info.Ngraph.vertex, add_node g id info)
    | Formula.And fs, true | Formula.Or fs, false ->
      make_node_pointing env id g fs (infos Ngraph.And)
    | Formula.Or fs, true | Formula.And fs, false ->
      make_node_pointing env id g fs (infos Ngraph.Or)
    | Formula.Forall (x, f'), true | Formula.Exists (x, f'), false ->
      let (x_id, env) = Env.add env x in
      make_node_pointing env id g [f'] (infos (Ngraph.Forall (x_id, x.Formula.var_type)))
    | Formula.Exists (x, f'), true | Formula.Forall (x, f'), false ->
      let (x_id, env) = Env.add env x in
      make_node_pointing env id g [f'] (infos (Ngraph.Exists (x_id, x.Formula.var_type)))
    | Formula.Not f, _ -> inner (not positivity) env g f
    | Formula.Predicate (p, xs), _ ->
      (match p.Formula.pred_val, xs with
       | PColor _, [x] ->
         let x_id = Env.get env x.Formula.var_name in
         let (env, id_true, vertex_true, g) = inner positivity env g Formula.True in
         let (env, id_false, vertex_false, g) = inner positivity env g Formula.False in
         let (g, vertex) = add_node_vertex g id (infos Ngraph.Or) in
         let g =
           connect g ~conds:[Ngraph.MustHave (x_id, p.Formula.pred_val)]
             (vertex, id) (vertex_true, id_true) in
         let g =
           connect g ~conds:[Ngraph.CantHave (x_id, p.Formula.pred_val)]
             (vertex, id) (vertex_false, id_false) in
         (env, id, vertex, g)
       | _, _ -> failwith "Graphify.build: Unknown predicate")
    | _ -> failwith "Graphify.build: not yet implemented" (* TODO: Formula.Variable and Formula.LetIn. *) in
  let (env, id, _vertex, g) = inner true Env.empty empty f in
  { g with Ngraph.start = id ; Ngraph.var_names = Env.reverse env }

let filter_constraint p g =
  { g with Ngraph.infos =
      IMap.map (fun info ->
        { info with Ngraph.constraints =
            List.filter p info.Ngraph.constraints }) g.Ngraph.infos }

let remove_symmetries =
  filter_constraint (function
    | Ngraph.Symmetric _ -> false
    | _ -> true)

(* Replace an identifier by another one in a constraint. *)
let replace_id_constraint id id' =
  let replace id0 =
    if id0 = id then id' else id0 in function
  | Ngraph.Symmetric id -> Ngraph.Symmetric (replace id)
  | Ngraph.LeftOf id -> Ngraph.LeftOf (replace id)
  | Ngraph.Below id -> Ngraph.Below (replace id)

(* Merge the nodes provided by their id and infos.
  The second one will be removed. *)
let merge_nodes_infos ?(check_infos = true) g (id1, info1) (id2, info2) =
  let graph = g.Ngraph.graph in
  let vertex1 = info1.Ngraph.vertex in
  let vertex2 = info2.Ngraph.vertex in
  let succs2 = GGraph.succ graph vertex2 in
  let preds2 = GGraph.pred graph vertex2 in
  (* Adding the pre and post transitions of id2 to id1. *)
  let graph =
    List.fold_left (fun graph v ->
      if vertex1 <> v then
        GGraph.add_edge graph vertex1 v ;
      graph) graph succs2 in
  let graph =
    List.fold_left (fun graph v ->
      if v <> vertex1 then
      GGraph.add_edge graph v vertex1 ;
      graph) graph preds2 in
  let arc_conditions = g.Ngraph.arc_conditions in
  let arc_conditions =
    List.fold_left (fun arcs v ->
      let id = GGraph.V.label v in
      let arc =
        match IIMap.find_opt (id2, id) arc_conditions with
        | None -> assert false
        | Some arc -> arc in
      if id1 = id then arcs
      else IIMap.add (id1, id) arc arcs) arc_conditions succs2 in
  let arc_conditions =
    List.fold_left (fun arcs v ->
      let id = GGraph.V.label v in
      let arc =
        match IIMap.find_opt (id, id2) arc_conditions with
        | None -> assert false
        | Some arc -> arc in
      if id = id1 then arcs
      else IIMap.add (id, id1) arc arcs) arc_conditions preds2 in
  (* Removing the node from the graph. *)
  let graph = GGraph.remove_vertex graph vertex2 ; graph in
  let arc_conditions =
    List.fold_left (fun arcs v ->
      let id = GGraph.V.label v in
      IIMap.remove (id2, id) arcs) arc_conditions succs2 in
  let arc_conditions =
    List.fold_left (fun arcs v ->
      let id = GGraph.V.label v in
      IIMap.remove (id, id2) arcs) arc_conditions preds2 in
  let g = { g with Ngraph.graph = graph ; Ngraph.arc_conditions = arc_conditions } in
  (* Merging the infos into info1. *)
  let info1 =
    let info1 =
      let kind =
        if check_infos then
          let open Ngraph in
          match info1.kind, info2.kind with
          | Forall (x1, t1), Forall (x2, t2) ->
            assert (x1 = x2 && t1 = t2) ;
            Forall (x1, t1)
          | Exists (x1, t1), Exists (x2, t2) ->
            assert (x1 = x2 && t1 = t2) ;
            Exists (x1, t1)
          | Forall (x, t), And | And, Forall (x, t) -> Forall (x, t)
          | Exists (x, t), Or | Or, Exists (x, t) -> Exists (x, t)
          | k1, k2 when k1 = k2 -> k1
          | _, _ -> assert false
        else info1.kind in
      { info1 with Ngraph.kind = kind } in
    (* Combining both sets of constraints. *)
    let constraints = info1.Ngraph.constraints @ info2.Ngraph.constraints in
    (* Removing constraints that no longer make sense. *)
    let constraints =
      List.filter (function
        | Ngraph.Symmetric id -> id <> id2
        | Ngraph.LeftOf id -> id <> id2
        | Ngraph.Below id -> id <> id2) constraints in
    { info1 with Ngraph.constraints = constraints } in
  let g =
    { g with Ngraph.infos =
               let infos = g.Ngraph.infos in
               let infos = IMap.remove id2 infos in
               let infos = IMap.add id1 info1 infos in
               infos } in
  (* Replacing all occurence of id2 by id1 in the node constraints. *)
  let g =
    { g with Ngraph.infos =
        IMap.map (fun info ->
          { info with Ngraph.constraints =
              List.map (replace_id_constraint id2 id1) info.Ngraph.constraints })
          g.Ngraph.infos } in
  g

(* Merge the nodes provided by their id. *)
let merge_nodes ?(check_infos = true) g id1 id2 =
  let fetch id =
    match IMap.find_opt id g.Ngraph.infos with
    | Some info -> info
    | None -> assert false in
  merge_nodes_infos ~check_infos g (id1, fetch id1) (id2, fetch id2)

let rec merge_all_nodes_infos ?(check_infos = true) g = function
  | idinf1 :: idinf2 :: ids ->
    let g = merge_nodes_infos ~check_infos g idinf1 idinf2 in
    merge_all_nodes_infos g (idinf1 :: ids)
  | [_] | [] -> g

(* Merge all the provided nodes in the graph. *)
let _merge_all_nodes g ids =
  let get id =
    match IMap.find_opt id g.Ngraph.infos with
    | Some info -> (id, info)
    | None -> assert false in
  merge_all_nodes_infos g (List.map get ids)

let merge_true_false nots g =
  let simpl = if nots then Typer.uncut else fun f -> f in
  (* Getting all the nodes that are either representing True or False. *)
  let (trues, falses) =
    IMap.fold (fun id info (trues, falses) ->
      match simpl info.Ngraph.subformula with
      | Formula.True -> ((id, info) :: trues, falses)
      | Formula.False -> (trues, (id, info) :: falses)
      | _ -> (trues, falses)) g.Ngraph.infos ([], []) in
  let g = merge_all_nodes_infos g trues in
  let g = merge_all_nodes_infos g falses in
  g

(* Mapping formulas. *)
module FMap =
  Map.Make (struct
    type t = Formula.tformula
    let compare : t -> t -> int = compare
  end)

let merge_identical nots base g =
  let simpl = if nots then Typer.uncut else fun f -> f in
  let formula_nodes =
    IMap.fold (fun id info formula_nodes ->
        let f = simpl info.Ngraph.subformula in
        let perform =
          if base then
            match f with
            | True | False -> false
            | _ -> true
          else true in
        if perform then
          FMap.add_to_list f (id, info) formula_nodes
        else formula_nodes)
      g.Ngraph.infos FMap.empty in
  FMap.fold (fun _f idinfs g -> merge_all_nodes_infos g idinfs) formula_nodes g

(* Add a constraint to an information map. *)
let add_constraints infos id cons =
  IMap.update id (function
    | None -> assert false
    | Some info ->
      Some { info with Ngraph.constraints = cons :: info.Ngraph.constraints }) infos

let force_direction dir g =
  let infos =
    GGraph.fold_edges (fun v1 v2 infos ->
      let id1 = GGraph.V.label v1 in
      let id2 = GGraph.V.label v2 in
      let (left, right) = if dir then (id1, id2) else (id2, id1) in
      add_constraints infos left (Ngraph.LeftOf right)) g.Ngraph.graph g.Ngraph.infos in
  { g with Ngraph.infos = infos }

let start_direction dir g =
  let start_id = g.Ngraph.start in
  let infos =
    GGraph.fold_vertex (fun v infos ->
      let id = GGraph.V.label v in
      if id = start_id then infos
      else
        let (left, right) = if dir then (start_id, id) else (id, start_id) in
        add_constraints infos left (Ngraph.LeftOf right)) g.Ngraph.graph g.Ngraph.infos in
  { g with Ngraph.infos = infos }

let separate_subformulas g =
  let graph = g.Ngraph.graph in
  let rec aux infos id above =
    let info =
      match IMap.find_opt id infos with
      | Some info -> info
      | None -> assert false in
    let info =
      match above with
      | None -> info
      | Some id' ->
        { info with Ngraph.constraints =
            Ngraph.Below id' :: info.Ngraph.constraints } in
    let infos = IMap.add id info infos in
    let vertex = info.Ngraph.vertex in
    let succs = GGraph.succ graph vertex in
    let rec iter_succs infos above = function
      | v :: succs ->
        let id' = GGraph.V.label v in
        let infos = aux infos id' above in
        iter_succs infos (Some id') succs
      | [] -> infos in
    iter_succs infos above succs in
  { g with Ngraph.infos = aux g.Ngraph.infos g.Ngraph.start None }

let remove_trivial_conditions g =
  let get_info id =
    match IMap.find_opt id g.Ngraph.infos with
    | Some info -> info
    | None -> assert false in
  let get_side id =
    GraphHelper.side_of_kind (get_info id).Ngraph.kind in
  let finish id =
    match (get_info id).Ngraph.kind with
    | Ngraph.True | Ngraph.False -> true
    | _ -> false in
  let f (id1, id2) conds =
    if conds = [] || (get_side id1 <> get_side id2 && finish id2) then []
    else conds in
  { g with arc_conditions = IIMap.mapi f g.Ngraph.arc_conditions }

(* Remove the provided node. *)
let remove_node g id =
  let info =
    match IMap.find_opt id g.Ngraph.infos with
    | None -> assert false
    | Some info -> info in
  let vertex = info.Ngraph.vertex in
  let succs = GGraph.succ g.Ngraph.graph vertex in
  let succs = List.map GGraph.V.label succs in
  let preds = GGraph.pred g.Ngraph.graph vertex in
  let preds = List.map GGraph.V.label preds in
  let g = { g with Ngraph.infos = IMap.remove id g.Ngraph.infos } in
  let g = GGraph.remove_vertex g.Ngraph.graph vertex ; g in
  let arc_conditions = g.Ngraph.arc_conditions in
  let arc_conditions =
    List.fold_left (fun arc_conditions id' ->
      IIMap.remove (id, id') arc_conditions) arc_conditions succs in
  let arc_conditions =
    List.fold_left (fun arc_conditions id' ->
      IIMap.remove (id', id) arc_conditions) arc_conditions preds in
  { g with Ngraph.arc_conditions = arc_conditions }

let garbage_collector g =
  let rec reachable seen id =
    if ISet.mem id seen then
      seen
    else
      let info =
        match IMap.find_opt id g.Ngraph.infos with
        | None -> assert false
        | Some info -> info in
      let vertex = info.Ngraph.vertex in
      let succs = GGraph.succ g.Ngraph.graph vertex in
      let succs = List.map GGraph.V.label succs in
      List.fold_left reachable (ISet.add id seen) succs in
  let reachable = reachable ISet.empty g.Ngraph.start in
  let unreachable =
    let all = ISet.of_list (List.map fst (IMap.bindings g.Ngraph.infos)) in
    ISet.diff all reachable in
  ISet.fold (fun id g -> remove_node g id) unreachable g

let remove_trivial_nodes g =
  let nodes_to_be_merged =
    IMap.fold (fun id info nodes_to_be_merged ->
      match info.Ngraph.kind with
      | Ngraph.And | Ngraph.Or ->
        let vertex = info.Ngraph.vertex in
        let succs = GGraph.succ g.Ngraph.graph vertex in
        (match succs with
         | [vertex'] ->
           let id' = GGraph.V.label vertex' in
           let conds =
             match IIMap.find_opt (id, id') g.Ngraph.arc_conditions with
             | None -> assert false
             | Some conds -> conds in
           if conds = [] then (id', id) :: nodes_to_be_merged
           else nodes_to_be_merged
         | _ -> nodes_to_be_merged)
      | _ -> nodes_to_be_merged) g.Ngraph.infos [] in
  List.fold_left (fun g (id1, id2) ->
    merge_nodes ~check_infos:false g id1 id2) g nodes_to_be_merged

let merge_quantifier_node g =
  let nodes_to_be_merged =
    IMap.fold (fun id info nodes_to_be_merged ->
      match info.Ngraph.kind with
      | Ngraph.Forall _ | Ngraph.Exists _ ->
        let vertex = info.Ngraph.vertex in
        let succs = GGraph.succ g.Ngraph.graph vertex in
        (match succs with
         | [vertex'] ->
           let id' = GGraph.V.label vertex' in
           let info' =
             match IMap.find_opt id' g.Ngraph.infos with
             | None -> assert false
             | Some info -> info in
           let kind_ok =
             match info'.Ngraph.kind with
             | Ngraph.And | Ngraph.Or -> true
             | _ -> false in
           let conds =
             match IIMap.find_opt (id, id') g.Ngraph.arc_conditions with
             | None -> assert false
             | Some conds -> conds in
           if conds = [] && kind_ok
              && GraphHelper.side_of_kind info.Ngraph.kind
                 = GraphHelper.side_of_kind info'.Ngraph.kind then
             (id, id') :: nodes_to_be_merged
           else nodes_to_be_merged
         | _ -> nodes_to_be_merged)
      | _ -> nodes_to_be_merged) g.Ngraph.infos [] in
  List.fold_left (fun g (id1, id2) ->
    merge_nodes ~check_infos:false g id1 id2) g nodes_to_be_merged

let split_conditions add_sink g =
  let arcs = g.Ngraph.arc_conditions in
  let f (id1, id2) conds g =
    match conds with
    | [] | [_] -> g
    | _ ->
      let (g, connect_to_sink) =
        if add_sink then
          let (id_sink, g) = new_id g in
          let info_sink = infos id_sink Formula.False Ngraph.False [] in
          let g = add_node g id_sink info_sink in
          (g, fun g id vertex cond ->
            let cond = GraphHelper.inverse_condition cond in
            let g =
              GGraph.add_edge g.Ngraph.graph vertex info_sink.vertex ;
              g in
            let g =
              { g with Ngraph.arc_conditions =
                         IIMap.add (id_sink, id) [cond] g.Ngraph.arc_conditions } in
            g
          )
        else (g, fun g _id _vertex _cond -> g) in
      let g =
        { g with Ngraph.arc_conditions = IIMap.remove (id1, id2) g.Ngraph.arc_conditions } in
      let get_info id =
        match IMap.find_opt id g.Ngraph.infos with
        | Some info -> info
        | None -> assert false in
      let info1 = get_info id1 in
      let info2 = get_info id2 in
      let kind =
        let open Ngraph in
        match info1.kind with
        | True | Or | Exists _ -> Or
        | False | And | Forall _ -> And in
      let rec aux f id_prev vertex_prev g = function
        | [] -> assert false
        | [cond] ->
          let g =
            GGraph.add_edge g.Ngraph.graph vertex_prev info2.Ngraph.vertex ;
            g in
          let g =
            { g with Ngraph.arc_conditions =
                       IIMap.add (id_prev, id2) [cond] g.Ngraph.arc_conditions } in
          g
        | cond :: conds ->
          let (id, g) = new_id g in
          let type_of_pred = function
            | Formula.PColor _ -> Formula.Color in
          let of_predicate p =
            Formula.{
              pred_type = [type_of_pred p] ;
              pred_val = p
            } in
          let make_var t x =
            let x =
              match IMap.find_opt x g.Ngraph.var_names with
              | None -> assert false
              | Some x -> x in
            Formula.{
              var_type = t ;
              var_name = x
            } in
          let to_formula = function
            | Ngraph.MustHave (x, p) ->
              Formula.Predicate (of_predicate p, [make_var (type_of_pred p) x])
            | Ngraph.CantHave (x, p) ->
              Formula.Not (Formula.Predicate (of_predicate p, [make_var (type_of_pred p) x])) in
          let f = Formula.And [ f ; to_formula cond ] in
          let info = infos id f kind info1.Ngraph.constraints in
          let g = add_node g id info in
          let g =
            GGraph.add_edge g.Ngraph.graph vertex_prev info.Ngraph.vertex ;
            g in
          let g =
            { g with Ngraph.arc_conditions =
                       IIMap.add (id_prev, id) [cond] g.Ngraph.arc_conditions } in
          let g = connect_to_sink g id info.Ngraph.vertex cond in
          aux f id info.Ngraph.vertex g conds in
      aux info1.Ngraph.subformula id1 info1.Ngraph.vertex g conds in
  IIMap.fold f arcs g

let chain g =
  let open GraphHelper in
  let open Ngraph in
  (* Given a node id and info, return the list of all its predecessors, with the associated
    node infos and arc conditions. *)
  let pred_info graph arc_conditions id info =
    let vertex = info.vertex in
    let preds = GGraph.pred graph vertex in
    List.map (fun v ->
      let id' = GGraph.V.label v in
      let info' =
        match IMap.find_opt id' g.infos with
        | Some info' -> info'
        | None -> assert false in
      let conds =
        match IIMap.find_opt (id', id) arc_conditions with
        | Some conds -> conds
        | None -> assert false in
      (id', info', conds)) preds in
  let (graph, arc_conditions) =
    IMap.fold (fun id1 info1 (graph, arc_conditions) ->
      match info1.kind with
      | True | False ->
        let preds = pred_info graph arc_conditions id1 info1 in
        (* We only consider predecessors of the opposide side without quantifier. *)
        let preds_ok =
          List.filter (fun (_id2, info2, _conds2) ->
            List.mem info2.kind [And; Or]
            && side_of_kind info1.kind = side_of_kind info2.kind) preds in
        (* For each of them we try to find a proper chain above. *)
        List.fold_left (fun (graph, arc_conditions) (id2, info2, conds2) ->
          let preds = pred_info graph arc_conditions id2 info2 in
          let preds_ok =
            List.filter (fun (_id3, info3, conds3) ->
              List.mem info3.kind [And; Or]
              && side_of_kind info2.kind <> side_of_kind info3.kind
              && conds3 = []) preds in
          let preds_ok =
            List.filter_map (fun (id3, info3, conds3) ->
              let vertex3 = info3.vertex in
              let succs = GGraph.succ graph vertex3 in
              let succs = List.map (fun v -> GGraph.V.label v) succs in
              let succs = List.filter ((<>) id2) succs in
              match succs with
              | [] -> None
              | id4 :: _ -> Some (id3, info3, conds3, id4)) preds_ok in
          (* We have the chain id3 -> id2 -> id1.
            The side of id2 is the same than id1, but not the same than id3. *)
          let all = List.length preds = List.length preds_ok in
          if all then (
            let candidates =
              List.sort_uniq compare
                (List.map (fun (_id3, _info3, _conds3, id4) -> id4) preds_ok) in
            match candidates with
            | [id4] ->
              let info4 =
                match IMap.find_opt id4 g.infos with
                | None -> assert false
                | Some info -> info in
              (* We can now remove the original connection, as well as id3 -> id4. *)
              let graph = GGraph.remove_edge graph info2.vertex info1.vertex ; graph in
              let arc_conditions = IIMap.remove (id2, id1) arc_conditions in
              let (graph, arc_conditions) =
                List.fold_left (fun (graph, arc_conditions) (id3, info3, _conds3, _id4) ->
                  let graph = GGraph.remove_edge graph info3.vertex info4.vertex ; graph in
                  let arc_conditions = IIMap.remove (id3, id4) arc_conditions in
                  (graph, arc_conditions)) (graph, arc_conditions) preds_ok in
              (* We can reroute the link to the candidate node. *)
              let graph = GGraph.add_edge graph info2.vertex info4.vertex ; graph in
              let arc_conditions = IIMap.add (id2, id4) conds2 arc_conditions in
              (graph, arc_conditions)
            | _ -> (graph, arc_conditions)
          ) else (graph, arc_conditions)) (graph, arc_conditions) preds_ok
      | _ -> (graph, arc_conditions)) g.infos (g.graph, g.arc_conditions) in
  { g with graph ; arc_conditions }

(* Force the alternance of sides in the graph. *)
let alternate g =
  let get_info id =
    match IMap.find_opt id g.Ngraph.infos with
    | Some info -> info
    | None -> assert false in
  let arcs_to_divide =
    IIMap.fold (fun (id1, id2) conds l ->
      let get_side id = GraphHelper.side_of_kind (get_info id).Ngraph.kind in
      let side1 = get_side id1 in
      let side2 = get_side id2 in
      if side1 = side2 then
        (id1, id2, conds) :: l
      else l) g.Ngraph.arc_conditions [] in
  List.fold_left (fun g (id1, id2, conds) ->
    let info1 = get_info id1 in
    let info2 = get_info id2 in
    let (id, g) = new_id g in
    let kind = GraphHelper.(to_intermediate_kind (not (side_of_kind info2.Ngraph.kind))) in
    let info =
      { info2 with Ngraph.vertex = GGraph.V.create id ; Ngraph.kind = kind } in
    let g = add_node g id info in
    (* Removing the old transition. *)
    let g =
      GGraph.remove_edge g.Ngraph.graph info1.Ngraph.vertex info2.Ngraph.vertex ;
      g in
    let g = { g with Ngraph.arc_conditions = IIMap.remove (id1, id2) g.Ngraph.arc_conditions } in
    (* Adding the two new transitions. *)
    let g =
      GGraph.add_edge g.Ngraph.graph info1.Ngraph.vertex info.Ngraph.vertex ;
      g in
    let g = { g with Ngraph.arc_conditions = IIMap.add (id1, id) conds g.Ngraph.arc_conditions } in
    let g =
      GGraph.add_edge g.Ngraph.graph info.Ngraph.vertex info2.Ngraph.vertex ;
      g in
    let g = { g with Ngraph.arc_conditions = IIMap.add (id, id2) [] g.Ngraph.arc_conditions } in
    g) g arcs_to_divide

