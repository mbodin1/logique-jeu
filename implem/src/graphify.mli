
(* Build the graph from the formula. *)
val build : Formula.tformula -> Ngraph.t

(* Remove all symmetry constraints from a graph. *)
val remove_symmetries : Ngraph.t -> Ngraph.t

(* Make sure that there is only one node for True and one node for False.
  Providing a true boolean forces the unfolding of Not first. *)
val merge_true_false : bool -> Ngraph.t -> Ngraph.t

(* Merge nodes representing the same subformula.
  Similarly to merge_true_false, the boolean states whether we should deal with Not.
  The second boolean states whether we should ignore True and False. *)
val merge_identical : bool -> bool -> Ngraph.t -> Ngraph.t

(* Make sure that all path goes to the right (true boolean) or to the left (false boolean). *)
val force_direction : bool -> Ngraph.t -> Ngraph.t

(* Make sure that the starting position is the leftmost (true boolean) or the rightmost (false boolean). *)
val start_direction : bool -> Ngraph.t -> Ngraph.t

(* Subformulas are vertically separated. *)
val separate_subformulas : Ngraph.t -> Ngraph.t

(* Remove conditions on transition making the other player win. *)
val remove_trivial_conditions : Ngraph.t -> Ngraph.t

(* Remove nodes with only one exit. *)
val remove_trivial_nodes : Ngraph.t -> Ngraph.t

(* If a quantifier is followed by an and/or of the same side, then merge them. *)
val merge_quantifier_node : Ngraph.t -> Ngraph.t

(* Split transitions with several conditions.
  The boolean states whether an additional transition to the other player goal is added in between,
  to insist that these are conjunctions. *)
val split_conditions : bool -> Ngraph.t -> Ngraph.t

(* Chain conjunctions of conditions, replacing and/or nodes. *)
val chain : Ngraph.t -> Ngraph.t

(* Force the alternance of sides in the graph. *)
val alternate : Ngraph.t -> Ngraph.t

(* Remove isolated nodes. *)
val garbage_collector : Ngraph.t -> Ngraph.t

