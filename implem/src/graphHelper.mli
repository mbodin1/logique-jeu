
(* Generic fucntions to help manipulating the graphs of Ngraph. *)

(* A player side (pirate or thief). *)
type side = bool

(* Get the side from a node kind. *)
val side_of_kind : Ngraph.kind -> side

(* Produce And or Or depending on the side. *)
val to_intermediate_kind : side -> Ngraph.kind

(* Negate a condition (must becomes can't). *)
val inverse_condition : Ngraph.condition -> Ngraph.condition

