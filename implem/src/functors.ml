
(* This file instantiates some functors, and as such is not associated with an interface file. *)

(* We also extend List a little. *)
module List = struct
  include List

  let rec for_all4 p l1 l2 l3 l4 =
    match l1, l2, l3, l4 with
    | [], [], [], [] -> true
    | x1 :: l1, x2 :: l2, x3 :: l3, x4 :: l4 ->
      p x1 x2 x3 x4 && for_all4 p l1 l2 l3 l4
    | _ -> invalid_arg "for_all4"

  (* The last element of a list. *)
  let rec last = function
    | [] -> failwith "last: empty list"
    | x :: [] -> x
    | _ :: l -> last l

  (* Drop the n-th first elements and returning the n-th tail of the list. *)
  let rec drop n l =
    if n = 0 then l
    else
      match l with
      | [] -> failwith "drop: expected more elements."
      | _ :: l -> drop (n - 1) l

  (* Return a list identical except for the n-th element that has been applied the function f. *)
  let rec apply_to_nth f n l =
    match n, l with
    | _, [] -> failwith "apply_to_nth: not enough elements."
    | 0, x :: l -> f x :: l
    | n, x :: l -> x :: apply_to_nth f (n - 1) l

  (* Remove the n-th element of a list. *)
  let rec remove_nth n l =
    match n, l with
    | _, [] -> failwith "remove_nth: expected more elements"
    | 0, _ :: l -> l
    | n, x :: l -> x :: remove_nth (n - 1) l

  (* Add an element at the n-th place of a list. The previously n-th element is not (n+1)-th. *)
  let rec add_at_nth x n l =
    if n = 0 then x :: l
    else
      match l with
      | [] -> failwith "add_at_nth: expected more elements"
      | y :: l -> y :: add_at_nth x (n - 1) l

  (* Return the number of element satisfying the predicate. *)
  let rec count p = function
    | [] -> 0
    | x :: l ->
      if p x then 1 + count p l
      else count p l

  (* Remove an element from the list. *)
  let rem x l = List.filter ((<>) x) l

end

(* Integer ordering *)
module Integer = struct
  type t = int
  let compare : t -> t -> int = compare
end

(* Pairs of ordered type, ordred with lexicographic order. *)
module Pair (A : Set.OrderedType) (B : Set.OrderedType) = struct
  type t = A.t * B.t
  let compare (a1, b1) (a2, b2) =
    match compare a1 a2 with
    | 0 -> compare b1 b2
    | r -> r
end

(* Integer maps *)
module IMap = Map.Make (Integer)

(* Integer sets *)
module ISet = Set.Make (Integer)

(* Pairs of Integer maps *)
module IIMap = Map.Make (Pair (Integer) (Integer))

(* Pairs of Integer sets *)
module IISet = Set.Make (Pair (Integer) (Integer))

(* String maps *)
module SMap = Map.Make (String)

(* String sets *)
module SSet = Set.Make (String)

