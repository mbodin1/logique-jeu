
open Functors
open Formula

let map_pred fpred p = {
  pred_type = fpred p.pred_type ;
  pred_val = p.pred_val
}

let map_var fvar x = {
  var_type = fvar x.var_type ;
  var_name = x.var_name
}

let map fvar fpred ffvar =
  let map_fvar = map_var ffvar in
  let map_var = map_var fvar in
  let map_pred = map_pred fpred in
  let rec aux = function
  | True -> True
  | False -> False
  | And fs -> And (List.map aux fs)
  | Or fs -> Or (List.map aux fs)
  | Predicate (p, xs) -> Predicate (map_pred p, List.map map_var xs)
  | Not f -> Not (aux f)
  | Forall (x, f) -> Forall (map_var x, aux f)
  | Exists (x, f) -> Exists (map_var x, aux f)
  | Variable (p, fs) ->
    Variable (map_fvar p, List.map (function
      | Either.Left x -> Either.Left (map_var x)
      | Either.Right f -> Either.Right (aux f)) fs)
  | LetIn (p, xs, f1, f2) ->
    LetIn (map_fvar p, List.map (Either.map ~left:map_var ~right:map_fvar) xs, aux f1, aux f2) in
  aux

let rec show =
  let show_var x = x.var_name in
  let show_color = function
    | Red -> "🔴"
    | Green -> "🟢"
    | Blue -> "🔵" in
  let show_pred p =
    match p.pred_val with
    | PColor c -> show_color c in
  let show_list show l = String.concat ", " (List.map show l) in
  let pshow f =
    Printf.sprintf "(%s)" (show f) in function
  | True -> "⊤"
  | False -> "⊥"
  | And [] -> "∧()"
  | And [f] -> "∧" ^ pshow f
  | And fs -> String.concat " ∧ " (List.map pshow fs)
  | Or [] -> "∨()"
  | Or [f] -> "∨" ^ pshow f
  | Or fs -> String.concat " ∨ " (List.map pshow fs)
  | Predicate (p, xs) -> Printf.sprintf "%s(%s)" (show_pred p) (show_list show_var xs)
  | Not f -> "¬" ^ pshow f
  | Forall (x, f) -> Printf.sprintf "∀ %s, %s" (show_var x) (show f)
  | Exists (x, f) -> Printf.sprintf "∃ %s, %s" (show_var x) (show f)
  | Variable (p, fs) ->
    Printf.sprintf "%s(%s)" (show_var p) (show_list (function
      | Either.Left x -> show_var x
      | Either.Right f -> show f) fs)
  | LetIn (p, xs, f1, f2) ->
    let show_arg = function
      | Either.Left x -> show_var x
      | Either.Right x -> show_var x in
    Printf.sprintf "let %s %sbe %s in %s"
      (show_var p)
      (String.concat "" (List.map (fun x -> show_arg x ^ " ") xs))
      (show f1)
      (show f2)

(* Transform a formula into another by calling the function fm at each node. *)
(* induction : (('a, 'b, 'c) Formula.t -> ('a, 'b, 'c) Formula.t) -> ('a, 'b, 'c) Formula.t -> ('a, 'b, 'c) Formula.t *)
let rec induction fm = function
  | True -> fm True
  | False -> fm False
  | And fs -> fm (And (List.map (induction fm) fs))
  | Or fs -> fm (Or (List.map (induction fm) fs))
  | Predicate (p, xs) -> fm (Predicate (p, xs))
  | Not f -> fm (Not (induction fm f))
  | Forall (x, f) -> fm (Forall (x, induction fm f))
  | Exists (x, f) -> fm (Exists (x, induction fm f))
  | Variable (p, fs) ->
    fm (Variable (p, List.map (function
      | Either.Left x -> Either.Left x
      | Either.Right f -> Either.Right (induction fm f)) fs))
  | LetIn (p, xs, f1, f2) ->
    fm (LetIn (p, xs, induction fm f1, induction fm f2))

(* The drinker formula, for tests. *)
let drinker =
  let var x = {
    var_type = ((), Some Color) ;
    var_name = x
  } in
  let pred c = {
    pred_type = [Color] ;
    pred_val = PColor c
  } in
  let x = var "x" in
  let y = var "y" in
  Exists (x, Or [Not (Predicate (pred Green, [x])); Forall (y, Predicate (pred Green, [y]))])

(* For tests: wrap a formula around And [_] contexts. *)
let rec wrap_and n f =
  if n = 0 then f
  else wrap_and (n - 1) (And [f])

(* For tests: wrap a formula around Or [_] contexts. *)
let rec wrap_or n f =
  if n = 0 then f
  else wrap_or (n - 1) (Or [f])

(* For tests: wrap a formula around alternative And [_] and Or [_] contexts. *)
let rec wrap n f =
  if n = 0 then f
  else if n mod 2 = 0 then wrap (n - 1) (And [f])
  else wrap (n - 1) (Or [f])

(* Fold over all the local information of a formula.
  The function is called at each formula node, with the accumulator being carried along. *)
(* everywhere : ('acc -> ('a, 'b, 'c) Formula.t -> 'acc) -> 'acc -> ('a, 'b, 'c) Formula.t -> 'acc *)
let rec everywhere fa acc = function
  | True -> fa acc True
  | False -> fa acc False
  | And fs ->
    let acc = List.fold_left (everywhere fa) acc fs in
    fa acc (And fs)
  | Or fs ->
    let acc = List.fold_left (everywhere fa) acc fs in
    fa acc (Or fs)
  | Predicate (p, xs) -> fa acc (Predicate (p, xs))
  | Not f -> fa (everywhere fa acc f) (Not f)
  | Forall (x, f) -> fa (everywhere fa acc f) (Forall (x, f))
  | Exists (x, f) -> fa (everywhere fa acc f) (Exists (x, f))
  | Variable (p, fs) ->
    let acc =
      List.fold_left (fun acc -> function
        | Either.Left _x -> acc
        | Either.Right f -> everywhere fa acc f) acc fs in
    fa acc (Variable (p, fs))
  | LetIn (p, xs, f1, f2) ->
    let acc = everywhere fa acc f1 in
    let acc = everywhere fa acc f2 in
    fa acc (LetIn (p, xs, f1, f2))

let coalesce f =
  induction (function
    | And fs ->
      let rec grab_ands acc = function
        | [] -> if acc = [] then True else And (List.rev acc)
        | And fs' :: fs -> grab_ands acc (fs' @ fs)
        | f :: fs -> grab_ands (f :: acc) fs in
      grab_ands [] fs
    | Or fs ->
      let rec grab_ors acc = function
        | [] -> if acc = [] then False else Or (List.rev acc)
        | Or fs' :: fs -> grab_ors acc (fs' @ fs)
        | f :: fs -> grab_ors (f :: acc) fs in
      grab_ors [] fs
    | f -> f) f

let%test "coalesce" =
  let t = True in
  let f = False in
  coalesce (And [Or [t; f]; And [f; t]]) = And [Or [t; f]; f; t]

let remove_trivial f =
  induction (function
    | And fs ->
      let fs = List.filter (fun f -> f <> True) fs in
      if List.mem False fs then False
      else
        (match List.sort_uniq compare fs with
         | [] -> True
         | [f] -> f
         | fs -> And fs)
    | Or fs ->
      let fs = List.filter (fun f -> f <> False) fs in
      if List.mem True fs then True
      else
        (match List.sort_uniq compare fs with
         | [] -> False
         | [f] -> f
         | fs -> Or fs)
    | f -> f) f

let%test "remove_trivial" =
  let f = Not False in
  remove_trivial (And [f; True; f]) = f

let%test "remove_trivial deep and" =
  let f = Not (Or [True; False]) in
  remove_trivial (wrap_and 42 f) = remove_trivial f

let%test "remove_trivial deep or" =
  let f = Not (And [True; False]) in
  remove_trivial (wrap_or 42 f) = remove_trivial f

let%test "remove_trivial deep alt" =
  let f = Not (And [True; False]) in
  remove_trivial (wrap 42 f) = remove_trivial f

let rec uncut = function
  | Not (True) -> False
  | Not (False) -> True
  | Not (And fs) -> Or (List.map (fun f -> uncut (Not f)) fs)
  | Not (Or fs) -> And (List.map (fun f -> uncut (Not f)) fs)
  | Not (Not f) -> uncut f
  | Not (Forall (x, f)) -> Exists (x, uncut (Not f))
  | Not (Exists (x, f)) -> Forall (x, uncut (Not f))
  | Not (LetIn (p, xs, f1, f2)) -> LetIn (p, xs, uncut f1, uncut (Not f2))
  | Not f -> Not (uncut f)
  | True -> True
  | False -> False
  | And fs -> And (List.map uncut fs)
  | Or fs -> Or (List.map uncut fs)
  | Predicate (p, xs) -> Predicate (p, xs)
  | Forall (x, f) -> Forall (x, uncut f)
  | Exists (x, f) -> Exists (x, uncut f)
  | Variable (p, fs) -> Variable (p, fs)
  | LetIn (p, xs, f1, f2) -> LetIn (p, xs, uncut f1, uncut f2)

let%test "uncut repeating" =
  uncut (Not (Not (drinker))) = uncut (drinker)

module Env = SMap

let unfold f =
  let rec aux env = function
    | True -> True
    | False -> False
    | And fs -> And (List.map (aux env) fs)
    | Or fs -> Or (List.map (aux env) fs)
    | Predicate (p, xs) ->
      let xs =
        List.map (fun x ->
          match Env.find_opt x.var_name env with
          | None -> x
          | Some (Either.Left y) -> y
          | Some (Either.Right _) ->
            invalid_arg (Printf.sprintf "Mismatch substitution for %s." x.var_name)) xs in
      Predicate (p, xs)
    | Not f -> Not (aux env f)
    | Forall (x, f) -> Forall (x, aux (Env.remove x.var_name env) f)
    | Exists (x, f) -> Exists (x, aux (Env.remove x.var_name env) f)
    | Variable (p, fs) ->
      (match Env.find_opt p.var_name env with
      | None -> Variable (p, fs) (* The variable probably comes from a quantifier. *)
      | Some (Either.Left y) ->
        invalid_arg (Printf.sprintf "Mismatch substitution for %s and %s." p.var_name y.var_name)
      | Some (Either.Right (xs, f)) ->
        let env =
          (* We clear the environment with only the local variables. *)
          List.fold_left2 (fun env x -> function
            | Either.Left y -> Env.add x (Either.Left y) env
            | Either.Right f -> Env.add x (Either.Right ([], f)) env) Env.empty xs fs in
        aux env f)
    | LetIn (p, xs, f1, f2) ->
      let get_name =
        let name x = x.var_name in
        Either.fold ~left:name ~right:name in
      let f1 =
        (* Removing shadowed variables. *)
        let env =
          List.fold_left (fun env x -> Env.remove (get_name x) env) env xs in
        (* And substituting in the current environment the inner formula. *)
        aux env f1 in
      let env = Env.add p.var_name (Either.Right (List.map get_name xs, f1)) env in
      aux env f2 in
  aux Env.empty f

let free_variables f =
  let rec aux declared_var declared_f = function
    | True | False -> (Seq.empty, Seq.empty)
    | And fs | Or fs ->
      let (free_var, free_f) = Seq.split (Seq.map (aux declared_var declared_f) (List.to_seq fs)) in
      (Seq.concat free_var, Seq.concat free_f)
    | Predicate (_p, xs) ->
      (Seq.concat_map (fun x ->
         if SSet.mem x.var_name declared_var then Seq.empty
         else Seq.return x) (List.to_seq xs), Seq.empty)
    | Not f -> aux declared_var declared_f f
    | Forall (x, f) | Exists (x, f) ->
      let declared_var = SSet.add x.var_name declared_var in
      aux declared_var declared_f f
    | Variable (p, fs) ->
      let (free_var, free_f) =
        List.fold_left (fun (free_var, free_f) -> function
          | Either.Left x ->
            if SSet.mem x.var_name declared_var then (free_var, free_f)
            else (Seq.cons x free_var, free_f)
          | Either.Right f ->
            let (free_var', free_f') = aux declared_var declared_f f in
            (Seq.append free_var free_var', Seq.append free_f free_f')) (Seq.empty, Seq.empty) fs in
      let free_f = if SSet.mem p.var_name declared_f then free_f else Seq.cons p free_f in
      (free_var, free_f)
    | LetIn (p, xs, f1, f2) ->
      let (free_var1, free_f1) =
        let (declared_var, declared_f) =
          List.fold_left (fun (declared_var, declared_f) -> function
              | Either.Left x -> (SSet.add x.var_name declared_var, declared_f)
              | Either.Right x -> (declared_var, SSet.add x.var_name declared_f))
            (declared_var, declared_f) xs in
        aux declared_var declared_f f1 in
      let declared_f = SSet.add p.var_name declared_f in
      let (free_var2, free_f2) = aux declared_var declared_f f2 in
      (Seq.append free_var1 free_var2, Seq.append free_f1 free_f2) in
  aux SSet.empty SSet.empty f

let%test "free_variables drinker" =
  let (xs, sxs) = free_variables drinker in
  Seq.is_empty xs && Seq.is_empty sxs

let all_variables f =
  everywhere (fun (sx, sfx) -> function
    | Forall (x, _) | Exists (x, _) -> (Seq.cons x sx, sfx)
    | Variable (x, _) -> (sx, Seq.cons x sfx)
    | LetIn (x, _, _, _) -> (sx, Seq.cons x sfx)
    | _ -> (sx, sfx)) (Seq.empty, Seq.empty) f

let%test "all_variables drinker" =
  let (xs, sxs) = all_variables drinker in
  assert (Seq.is_empty sxs) ;
  let xs = List.of_seq xs in
  let xs = List.sort_uniq compare xs in
  let var x = {
    var_type = ((), Some Color) ;
    var_name = x
  } in
  xs = [var "x"; var "y"]

(* Make each variable distinct from each other (prevents shadowing). *)
let unique_variables f =
  let rec find_replacement used x i =
    let x' = Printf.sprintf "%s_%i" x i in
    if SSet.mem x' used then find_replacement used x (1 + i)
    else x' in
  let defined_var_name replacements used x =
    if SSet.mem x used then (
      (* A replacement is needed here. *)
      let x' = find_replacement used x 1 in
      (SMap.add x x' replacements, SSet.add x' used, x')
    ) else (SMap.add x x replacements, SSet.add x used, x) in
  (* Update the replacements and used sets when encountering a variable definitions.
    In particular, if it shadows another variable, find a proper replacement. *)
  let defined_var replacements used x =
    let (replacements, used, x') = defined_var_name replacements used x.var_name in
    (replacements, used, { x with var_name = x' }) in
  let used_var replacements x =
    match SMap.find_opt x.var_name replacements with
    | None -> assert false
    | Some x' -> { x with var_name = x' } in
  (* We carry two objects along the way: a map of replacements (containing only the
    objects in scope) and the used set, containing all the declared variables.
    The replacements map traverses the formula independently on each branches,
    whilst the used is propagated between branches.
    This means that a formula of the form And (Exists (x, f), Exists (x, f'))
    will rename both usages of x. *)
  let rec aux replacements used = function
    | True -> (used, True)
    | False -> (used, False)
    | And fs ->
      let (used, fs) = List.fold_left (fun (used, fs) f ->
        let (used, f) = aux replacements used f in
        (used, f :: fs)) (used, []) fs in
      (used, And (List.rev fs))
    | Or fs ->
      let (used, fs) = List.fold_left (fun (used, fs) f ->
        let (used, f) = aux replacements used f in
        (used, f :: fs)) (used, []) fs in
      (used, Or (List.rev fs))
    | Predicate (p, xs) ->
      (used, Predicate (p, List.map (used_var replacements) xs))
    | Not f ->
      let (used, f) = aux replacements used f in
      (used, Not f)
    | Forall (x, f) ->
      let (replacements, used, x) = defined_var replacements used x in
      let (used, f) = aux replacements used f in
      (used, Forall (x, f))
    | Exists (x, f) ->
      let (replacements, used, x) = defined_var replacements used x in
      let (used, f) = aux replacements used f in
      (used, Exists (x, f))
    | Variable (p, fs) ->
      let p = used_var replacements p in
      let (used, fs) =
        List.fold_left (fun (used, fs) -> function
          | Either.Left x -> (used, Either.Left (used_var replacements x) :: fs)
          | Either.Right f ->
            let (used, f) = aux replacements used f in
            (used, Either.Right f :: fs)) (used, []) fs in
      let fs = List.rev fs in
      (used, Variable (p, fs))
    | LetIn (p, xs, f1, f2) ->
      let (replacements, used, p) = defined_var replacements used p in
      let (used, xs, f1) =
        let (replacements, used, xs) =
          List.fold_left (fun (replacements, used, xs) -> function
            | Either.Left x ->
              let (replacements, used, x) = defined_var replacements used x in
              (replacements, used, Either.Left x :: xs)
            | Either.Right x ->
              let (replacements, used, x) = defined_var replacements used x in
              (replacements, used, Either.Right x :: xs)) (replacements, used, []) xs in
        let xs = List.rev xs in
        let (used, f1) = aux replacements used f1 in
        (used, xs, f1) in
      let (used, f2) = aux replacements used f2 in
      (used, LetIn (p, xs, f1, f2)) in
  (* Free variables are getting replaced first, getting the priority over any replacements.
    As variables start by lowercase and formula variables with uppercases, there shouldn't be
    any conflict at this point. *)
  let (free_var, free_f) = free_variables f in
  let adds replacements used additions =
    let additions =
      List.sort_uniq compare (List.map (fun x -> x.var_name) (List.of_seq additions)) in
    List.fold_left (fun (replacements, used) x ->
      let (replacements, used, _x) = defined_var_name replacements used x in
      (replacements, used)) (replacements, used) additions in
  let (replacements, used) = (SMap.empty, SSet.empty) in
  let (replacements, used) = adds replacements used free_var in
  let (replacements, used) = adds replacements used free_f in
  snd (aux replacements used f)

let%test "unique_variables drinker" =
  unique_variables drinker = drinker

(* Substitute a variable to a value. *)
let rec subst_val x v = function
  | True -> True
  | False -> False
  | And fs -> And (List.map (subst_val x v) fs)
  | Or fs -> Or (List.map (subst_val x v) fs)
  | Predicate (p, xs) when not (List.mem x xs) -> Predicate (p, xs)
  | Predicate (p, xs) ->
      (match p.pred_val, xs with
       | PColor c, [x'] ->
         assert (x = x') ;
         if v = c then True else False
       | _, _ -> failwith "subst_val: Unknown predicate")
  | Not f -> Not (subst_val x v f)
  | Forall (y, f) -> Forall (y, if x = y then f else subst_val x v f)
  | Exists (y, f) -> Exists (y, if x = y then f else subst_val x v f)
  | Variable (p, fs) ->
    let aux = function
      | Either.Left y ->
        if x = y then failwith "subst_val: substitution not fitting the data structure: unfold first."
        else Either.Left y
      | Either.Right f -> Either.Right (subst_val x v f) in
    Variable (p, List.map aux fs)
  | LetIn (p, xs, f1, f2) ->
    let f1 = if List.mem (Either.Left x) xs then f1 else subst_val x v f1 in
    LetIn (p, xs, f1, subst_val x v f2)

let quantifier_elim f =
  let f = unfold f in
  let all_inhab = function
    | Color -> [Red; Green; Blue]
    | Bool -> [Red; Green] in
  induction (function
    | Exists (x, f) ->
      let inhabs = all_inhab x.var_type in
      Or (List.map (fun v -> subst_val x v f) inhabs)
    | Forall (x, f) ->
      let inhabs = all_inhab x.var_type in
      And (List.map (fun v -> subst_val x v f) inhabs)
    | f -> f) f

(* Environment variables can represent either variables introduced by a quantifier
   (Forall, Exists), or by a LetIn construct. The first are supposed to be of type
   tvar, and the second of type tpformula. They are respectively stored within the
   environment as Either.Left tvar, and Either.Right tpformula. *)
type typer_env = (tvar, tpformula) Either.t Env.t

let typer f =
  (* Add the type to a predicate. *)
  let add_pred p t = {
      pred_type = (p.pred_type, t) ;
      pred_val = p.pred_val
    } in
  (* Add the type to an uppercase variable. *)
  let add_fvar x t = {
      var_type = (x.var_type, t) ;
      var_name = x.var_name
    } in
  (* Add the type to a variable. *)
  let add_var x t = {
      var_type = (fst x.var_type, t) ;
      var_name = x.var_name
    } in
  (* Check that a typed variable is indeed of this type. *)
  let check_var x t =
    if t <> snd x.var_type then
      invalid_arg (Printf.sprintf "Variable %s has conflicting types in predicate." x.var_name) in
  let rec check_vars_pred xs ts =
    match xs, ts with
    | [], [] -> ()
    | x :: xs, t :: ts ->
      check_var x t ;
      check_vars_pred xs ts
    | _ -> invalid_arg "Mismatch variable count in predicate." in
  (* Enrich a variable with its type in the environment. *)
  let type_var (env : typer_env) x =
    match Env.find_opt x.var_name env with
    | None -> invalid_arg (Printf.sprintf "Unknown variable %s." x.var_name)
    | Some (Either.Left t) -> add_var x t
    | Some _ -> invalid_arg (Printf.sprintf "Formula type for %s." x.var_name) in
  let rec check_types (env : typer_env) fs (ts : tpformula) =
    match fs, ts with
    | [], [] -> ()
    | Either.Left x :: fs, Var t :: ts ->
      check_var x t ;
      check_types env fs ts
    | Either.Right _f :: fs, Formula :: ts ->
      check_types env fs ts
    | [], _ :: _ | _ :: _, [] -> invalid_arg "Mismatch variable count in parameterised formula."
    | _ :: _, _ :: _ ->
      invalid_arg "Mismatch between formula and variable in parameterised formula." in
  (* Type a formula. *)
  let rec aux (env : typer_env) = function
    | True -> True
    | False -> False
    | And fs -> And (List.map (aux env) fs)
    | Or fs -> Or (List.map (aux env) fs)
    | Predicate (p, xs) ->
      let t =
        match p.pred_val with
        | PColor _ -> [Color] in
      let xs = List.map (type_var env) xs in
      check_vars_pred xs t ;
      Predicate (add_pred p t, xs)
    | Not f -> Not (aux env f)
    | Forall (x, f) ->
      let t =
        (* We currently don't do type inference (TODO): we just assume x to be a color
          if no indication is provided. *)
        match snd x.var_type with
        | None -> Color
        | Some t -> t in
      let env = Env.add x.var_name (Either.Left t) env in
      Forall (add_var x t, aux env f)
    | Exists (x, f) ->
      let t =
        (* We currently don't do type inference (TODO): we just assume x to be a color
          if no indication is provided. *)
        match snd x.var_type with
        | None -> Color
        | Some t -> t in
      let env = Env.add x.var_name (Either.Left t) env in
      Exists (add_var x t, aux env f)
    | Variable (p, fs) ->
      let t =
        match Env.find_opt p.var_name env with
        | None -> invalid_arg (Printf.sprintf "Unknown variable %s." p.var_name)
        | Some (Either.Left _) ->
          invalid_arg (Printf.sprintf "Variable %s used as formula." p.var_name)
        | Some (Either.Right t) -> t in
      let fs =
        List.map (function
          | Either.Left x -> Either.Left (type_var env x)
          | Either.Right f -> Either.Right (aux env f)) fs in
      check_types env fs t ;
      Variable (add_fvar p t, fs)
    | LetIn (p, xs, f1, f2) ->
      let get_name =
        let name x = x.var_name in
        Either.fold ~left:name ~right:name in
      let get_type =
        let ty x = snd x.var_type in
        Either.map ~left:ty ~right:ty in
      let xs =
        List.map (function
          | Either.Left x ->
            (* We assume x to be a color. *)
            let t = Color in
            Either.Left (add_var x t)
          | Either.Right x ->
            (* We assume x to be a simple predicate. *)
            Either.Right (add_fvar x ([] : tpformula))) xs in
      let f1 =
        let env =
          List.fold_left (fun env x -> Env.add (get_name x) (get_type x) env) env xs in
        aux env f1 in
      let t =
        List.map (fun x ->
          match get_type x with
          | Either.Left t -> Var t
          | Either.Right t -> assert (t = []); Formula) xs in
      let f2 =
        let env = Env.add p.var_name (Either.Right t) env in
        aux env f2 in
      LetIn (add_fvar p t, xs, f1, f2) in
  unique_variables (aux Env.empty f)

let%test_unit "drinker well-typed" =
  ignore (typer drinker)

