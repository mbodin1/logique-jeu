
open Functors
open GraphHelper

(* Offset around the picture. *)
let offset = 30.

(* Distance between the tip of an arrow and the pointed object, relative to the object. *)
let arrow_offset = 0.9

module Id = struct

(* Element identifier. *)
type t = string

let fresh = ref 0

let get () : t =
  incr fresh ;
  Printf.sprintf "id-%d" !fresh

end

(* Variable identifier. *)
type var = int

(* Different pre-defined classes of elements. *)
type css_class =
  | Arrow
  | InitArrow
  | PiratesArrow
  | ThievesArrow
  | Island
  | Flag of Formula.color (* A flag of this color. *)
  | Wave of int (* The n-th wave, starting from 1. *)
  | BackgroundWater
  | Fog

(* A list of path commands. *)
type path_command =
  | MoveTo of Geometry.coordinate (* Move the cursor to this point. *)
  | ArcTo of float * Geometry.coordinate (* The (possibly 0.) curvature and target coordinate. *)
  | Cycle (* Jump towards the last Move-To command. *)

(* A SVG path, as a series of path commands. *)
type path = path_command list

(* Some special nodes associated to a complex image. *)
type special_id =
  | Signal of { side : side ; variable : int ; positive : bool ; predicate : Formula.predicate_val }
      (* A condition over an arrow.
        If positive, then one must have the condition to pass, otherwise one can't have it. *)
  | Node of side
  | Treasure of side
  | Quantifier of side * var * Formula.tvar (* The player has to choose a value for this flag. *)
  | Flag of Formula.color * var (* This variable id must have this color. *)

(* Elements that we aim to build (converted to actual SVG files at the end of this file). *)
type element =
  | SpecialNode of Geometry.coordinate * special_id * css_class list
      (* Some specific node to be copy/pasted. *)
  | Rect of Geometry.coordinate * (float * float) * css_class list
      (* A rectangle starting at that position, with this width and height. *)
  | Path of path * css_class list (* A SVG path associated to classes. *)
  | G of element list (* A group of elements. *)

type t = {
  viewbox : float * float * float * float (* Viewbox coordinates (min-x, min-y, width, height). *) ;
  variables : Formula.tvar IMap.t (* The type of all variables. *) ;
  background : element list (* All background elements (water and islands). *) ;
  arrows : element list (* All arrows. *) ;
  signals : element list (* Signals to be placed on arrows. *) ;
  nodes : element list (* All nodes of the graph, including the final crosses. *) ;
  foreground : element list (* Other foreground elements. *)
}

module SpecialId = struct

let side_to_string : side -> Id.t = function
  | true -> "pirate"
  | false -> "thief"

let color_to_string = function
  | Formula.Red -> "red"
  | Formula.Green -> "green"
  | Formula.Blue -> "blue"

let predicate_to_string = function
  | Formula.PColor c -> color_to_string c

let id_signal side b var pred : Id.t =
  Printf.sprintf "signal-%s-%s-%i-%s"
    (side_to_string side)
    (if b then "must" else "cant")
    var
    (predicate_to_string pred)

let id_special : special_id -> Id.t = function
  | Signal { side ; variable ; positive ; predicate } ->
    id_signal side positive variable predicate
  | Node side -> if side then "buoy" else "shed"
  | Treasure side -> if side then "pirates_win" else "thieves_win"
  | Quantifier (side, var, _t) ->
    Printf.sprintf "choice-%s-%i" (if side then "buoy" else "shed") var
  | Flag (color, var) ->
    Printf.sprintf "flag-%i-%s" var (color_to_string color)

let size : special_id -> float = function
  | Signal _ -> Sizes.signal
  | Node _ -> Sizes.node
  | Treasure _ -> Sizes.cross
  | Quantifier _ -> max Sizes.node (4. *. Sizes.flag)
  | Flag _ -> Sizes.flag

end

let id_special = SpecialId.id_special
let size_special = SpecialId.size

let of_graph g =
  let open Ngraph in
  let (g, islands) = (g.igraph, g.islands) in
  let open Geometry in
  let (min_x, min_y, max_x, max_y) =
    let (_id, { x ; y }) = IMap.choose g.positions in
    IMap.fold (fun _id { x ; y } (min_x, min_y, max_x, max_y) ->
        (min min_x x, min min_y y, max max_x x, max max_y y))
      g.positions (x, y, x, y) in
  let (min_x, min_y, max_x, max_y) =
    (min_x -. offset, min_y -. offset, max_x +. offset, max_y +. offset) in
  let (width, height) = (max_x -. min_x, max_y -. min_y) in
  let water_background =
    Rect ({ x = min_x; y = min_y }, (width, height), [BackgroundWater]) in
  let fetch_info id =
    match IMap.find_opt id g.pgraph.infos with
    | None -> assert false
    | Some info -> info in
  let fetch_position id =
    match IMap.find_opt id g.positions with
    | None -> assert false
    | Some pos -> pos in
  let islands =
    let islands =
      List.map (fun island ->
        MoveTo (snd (List.last island))
        :: List.map (fun (curve, p) -> ArcTo (curve, p)) island
        @ [Cycle]
      ) islands in
    let path = List.concat islands in [
      Path (path, [Wave 3]) ;
      Path (path, [Wave 2]) ;
      Path (path, [Wave 1]) ;
      Path (path, [Island])
    ] in
  let arrows =
    let paths_side =
      List.map (fun ((id1, id2), (curve, _conds)) ->
        let info1 = fetch_info id1 in
        let arrow_start = fetch_position id1 in
        let arrow_offset =
          let size =
            let info2 = fetch_info id2 in
            match info2.kind with
            | True | False -> Sizes.cross
            | And | Or -> Sizes.node
            | Forall (_x, _t) | Exists (_x, _t) -> Sizes.node *. 1.1 in
          arrow_offset *. size in
        let arrow_end_full = fetch_position id2 in
        let path =
          let arc_full = build_arc arrow_start arrow_end_full curve in
          if curve = 0. || valid_arc arc_full then
            let arrow_end =
              (* We make it end slightly before the very end. *)
              if curve = 0. then
                let v = build_vector arrow_end_full arrow_start in
                move_along_direction arrow_end_full v arrow_offset
              else
                let c = get_arc_center arc_full in
                let v = build_vector c arrow_end_full in
                let v = turn v (arrow_offset *. curve) in
                add_vector c v in
            if valid_vector arrow_end then
              [MoveTo arrow_start; ArcTo (curve, arrow_end)]
            else [MoveTo arrow_start]
          else [MoveTo arrow_start] in
        (path, side_of_kind info1.kind)) (IIMap.bindings g.arcs) in
    let arrows =
      List.map (fun (path, side) ->
        let side = if side then PiratesArrow else ThievesArrow in
        Path (path, [Arrow; side])) paths_side in
    let init_arrow =
      let coord = fetch_position g.pgraph.start in
      let arrow_offset = arrow_offset *. Sizes.node in
      let arrow_start = add_vector coord { x = -.Sizes.node *. 2. ; y = 0. } in
      let arrow_end = add_vector coord { x = -.arrow_offset ; y = 0. } in
      let path = [MoveTo arrow_start; ArcTo (0., arrow_end)] in
      Path (path, [Arrow; InitArrow]) in
    init_arrow :: arrows in
  let signals =
    List.filter_map (fun ((id1, id2), (curve, poss)) ->
      let info1 = fetch_info id1 in
      let side = side_of_kind info1.kind in
      let p1 = fetch_position id1 in
      let p2 = fetch_position id2 in
      let conds =
        match IIMap.find_opt (id1, id2) g.pgraph.arc_conditions with
        | None -> assert false
        | Some conds -> conds in
      let build pos positive x predicate =
        let signal = Signal { side ; variable = x ; positive ; predicate } in
        let arc = build_arc p1 p2 curve in
        if curve = 0. then
          let p = add_vector p1 (scale (build_vector p1 p2) pos) in
          SpecialNode (p, signal, [])
        else if valid_arc arc then
          let p = point_on_arc arc pos in
          SpecialNode (p, signal, [])
        else G [] in
      let l =
        List.map2 (fun pos -> function
          | MustHave (x, p) -> build pos true x p
          | CantHave (x, p) -> build pos false x p) poss conds in
      if l = [] then None
      else Some (G l)) (IIMap.bindings g.arcs) in
  let nodes =
    List.map (fun (id, coord) ->
      let info = fetch_info id in
      let node =
        match info.kind with
        | Or -> Node true
        | And -> Node false
        | True -> Treasure true
        | False -> Treasure false
        | Exists (x, t) -> Quantifier (true, x, t)
        | Forall (x, t) -> Quantifier (false, x, t) in
      SpecialNode (coord, node, [])) (IMap.bindings g.positions) in
  {
    viewbox = (min_x, min_y, width, height) ;
    variables =
      IMap.fold (fun _ info variables ->
        match info.kind with
        | Forall (x, t) | Exists (x, t) ->
          IMap.add x t variables
        | _ -> variables) g.pgraph.infos IMap.empty ;
    background = water_background :: islands ;
    arrows ;
    signals ;
    nodes ;
    foreground = []
  }

let whiten i =
  let (min_x, min_y, width, height) = i.viewbox in
  let white =
    Rect ({ x = min_x; y = min_y }, (width, height), [Fog]) in
  { i with foreground = i.foreground @ [white] }

let whiten_background i =
  let (min_x, min_y, width, height) = i.viewbox in
  let white =
    Rect ({ x = min_x; y = min_y }, (width, height), [Fog]) in
  { i with background = i.background @ [white] }


module Tikz = struct

let preamble = {|
%% General definitions.

% Images (download them from https://gitlab.inria.fr/mbodin1/logique-jeu/-/tree/main/implem/images/ )
\pgfdeclareimage{shed}{images/shed.svg}
\pgfdeclareimage{buoy}{images/buoy.svg}

% Colors
\definecolor{colorWater}{RGB}{213,229,255}
\definecolor{colorWave}{RGB}{42,127,255}
\definecolor{colorSand}{RGB}{255,238,170}
\definecolor{colorBorder}{RGB}{160,90,44}
\definecolor{colorRed}{RGB}{141,95,211}
\definecolor{colorGreen}{RGB}{113,200,55}
\definecolor{colorBlue}{RGB}{172,167,147}

% Styles
\tikzset{
  buoy/.style {
    path picture = {
      \node at (path picture bounding box.center)
        {\pgfbox[center,center]{\pgfuseimage{buoy}}} ;
    }
  },
  shed/.style {
    path picture = {
      \node at (path picture bounding box.center)
        {\pgfbox[center,center]{\pgfuseimage{shed}}} ;
    }
  },
  pirates_win/.style {
    path picture = {
      \node at (path picture bounding box.center)
        {\pgfbox[center,center]{\pgfuseimage{pirates_win}}} ;
    }
  },
  thieves_win/.style {
    path picture = {
      \node at (path picture bounding box.center)
        {\pgfbox[center,center]{\pgfuseimage{thieves_win}}} ;
    }
  },
  pathArrow/.style = {
    dashed, ->, very thick
  },
  initArrow/.style = {
    color=black
  },
  piratesArrow/.style = {
    color=colorBorder
  },
  thievesArrow/.style = {
    color=colorWave
  },
  mainIsland/.style = {
    color=colorBorder,
    fill=colorSand,
    thick
  },
  flagred/.style = {
    fill=colorRed,
    color=colorRed!50!black
  },
  flaggreen/.style = {
    fill=colorGreen,
    color=colorGreen!50!black
  },
  flagblue/.style = {
    fill=colorBlue,
    color=colorBlue!50!black
  },
  waveI/.style = {
    color=colorWave,
    thick
  },
  waveII/.style = {
    color=colorWave,
    semithick
  },
  waveIII/.style = {
    color=colorWave,
    thin
  },
  backgroundWater/.style = {
    fill=colorWater
  },
  fog/.style = {
    fill=white, opacity=0.204569
  }
}

%% Actual image.
|}

(* Convert a string into a roman-literal representation. *)
let number_to_string i =
  (String.make (i / 1000) 'M')
  ^ (String.make ((i mod 1000) / 100) 'C')
  ^ (String.make ((i mod 100) / 10) 'X')
  ^ (String.make (i mod 10) 'I')

(* Convert a class into a Tikz declaration. *)
let class_to_string = function
  | Arrow -> "pathArrow"
  | InitArrow -> "initArrow"
  | PiratesArrow -> "piratesArrow"
  | ThievesArrow -> "thievesArrow"
  | Island -> "mainIsland"
  | Flag c -> Printf.sprintf "flag%s" (SpecialId.color_to_string c)
  | Wave i -> Printf.sprintf "wave%s" (number_to_string i)
  | BackgroundWater -> "backgroundWater"
  | Fog -> "fog"

(* Convert a list of classes to TikZ declarations. *)
let classes_to_string classes =
  String.concat ", " (List.map class_to_string classes)

(* Print a TikZ coordinate. *)
let point p =
  let open Geometry in
  Printf.sprintf "(%g, %g)" p.x p.y

(* Convert a path into a TikZ path declaration. *)
let path_to_string p =
  let command prev = function
    | MoveTo p -> (point p, Some p)
    | ArcTo (curve, p') ->
      if curve = 0. then
        (Printf.sprintf "-- %s" (point p'), Some p')
      else
        let p =
          match prev with
          | None -> assert false
          | Some prev -> prev in
        let open Geometry in
        let radius = Float.abs (1. /. curve) in
        let arc = build_arc p p' curve in
        if valid_arc arc then
          let c = get_arc_center arc in
          let angle p =
            let v = build_vector c p in
            let v = to_polar v in
            v.angle *. 180. /. Float.pi in
          let angle_begin = angle p in
          let angle_end = angle p' in
          (Printf.sprintf "arc (%g:%g:%g)" angle_begin angle_end radius, Some p')
        else (point p', Some p')
    | Cycle -> ("-- cycle", None) in
  let process (prev, l) path_c =
    let (c, new_pos) = command prev path_c in
    (new_pos, c :: l) in
  String.concat " " (List.rev (snd (List.fold_left process (None, []) p)))

(* Convert an element. *)
let rec convert_element ?(depth=0) =
  let space = String.make (2 * depth) ' ' in function
  | SpecialNode (coord, id, classes) ->
    Printf.sprintf {|%s\node [%s, %s] at %s {} ;|}
      space (id_special id) (classes_to_string classes) (point coord)
  | Rect (coord, (width, height), classes) ->
    Printf.sprintf {|%s\draw [%s] %s rectangle ++%s ;|}
      space (classes_to_string classes) (point coord) (point { x = width ; y = height })
  | Path (path, classes) ->
    Printf.sprintf {|%s\draw [%s] %s ;|} space (classes_to_string classes) (path_to_string path)
  | G l ->
    Printf.sprintf {|%s\begin{scope}%c%s%c%s\end{scope}|}
      space '\n' (String.concat "\n" (List.map (convert_element ~depth:(1 + depth)) l)) '\n' space

let to_tikz i =
  preamble
  ^ String.concat "\n"
      (List.map convert_element
        (i.background @ i.arrows @ i.signals @ i.nodes @ i.foreground))

end

let to_tikz = Tikz.to_tikz

module Convert (Xml : Xml_sigs.NoWrap) (Svg : Svg_sigs.Make(Xml).T) = struct

module Data = Svg_defs.Data(Xml)(Svg)

type svg = Svg_types.svg Svg.elt

(* Unfortunately, the support for a lot of CSS features
  (:root definitions, var(), calc(), etc.) is very poor and we
  have to keep the CSS declarations as simple as possible.
  This module aims at doing that. *)
module Style = struct

(* The different ad-hoc colors used in the CSS declaration. *)
type color =
  | Water (* The background water. *)
  | Wave (* Blue, used for arrows and waves. *)
  | Sand (* The land background. *)
  | Border (* Brown, used for arrows and land borders. *)
  | Red (* The color of the “red” flag, which is actually purple. *)
  | Green (* The color of the green flag. *)
  | Blue (* The color of the “blue” flag, which is actually brown. *)

(* Get the CSS color of these colors. *)
let string_of_color = function
  | Water -> "#d5e5ff"
  | Wave -> "#2a7fff"
  | Sand -> "#ffeeaa"
  | Border -> "#a05a2c"
  | Red -> "#8d5fd3"
  | Green -> "#71c837"
  | Blue -> "#aca793"

(* The different sizes used in the CSS declaration. *)
type size =
  | NodeSize
  | ArcWidth
  | WaveUnit
  | Multiple of float * size (* A size multiplied by a scalar. *)

(* Convert a size into an actual pixel length. *)
let rec size_to_pixel = function
  | NodeSize -> Sizes.node
  | ArcWidth -> Sizes.arc
  | WaveUnit -> Sizes.wave_unit
  | Multiple (factor, size) -> factor *. size_to_pixel size

(* Convert the size into a distance. *)
let string_of_size size =
  Printf.sprintf "%gpx" (size_to_pixel size)

(* CSS values. *)
type value =
  | Color of color
  | Size of size
  | VFloat of float
  | VList of value list (* List of values, separated by commas. *)
  | String of string (* Miscellaneous values. *)

let rec string_of_value = function
  | Color c -> string_of_color c
  | Size s -> string_of_size s
  | VFloat f -> Printf.sprintf "%g" f
  | VList l -> String.concat ", " (List.map string_of_value l)
  | String str -> str

(* The content of a CSS class. *)
type defs = (string * value) list

let string_of_defs l =
  String.concat " "
    (List.map (fun (n, v) ->
      Printf.sprintf "%s: %s ;" n (string_of_value v)) l)

(* Classes definitions. *)
type style = (string * defs) list

let string_of_style l =
  String.concat " "
    (List.map (fun (c, d) ->
      Printf.sprintf ".%s { %s }" c (string_of_defs d)) l)

(* Global CSS styles. *)
let styles : style = [
  ("marker", [
    ("stroke-width", Size ArcWidth) ;
    ("fill", String "context-stroke")
  ]) ;
  ("arrow", [
    ("stroke", String "inherit") ;
    ("fill", String "none") ;
    ("stroke-width", Size ArcWidth) ;
    ("stroke-linecap", String "butt") ;
    ("stroke-linejoin", String "miter") ;
    ("stroke-miterlimit", Size ArcWidth) ;
    ("stroke-dasharray", VList [Size (Multiple (2., ArcWidth)) ; Size ArcWidth]) ;
    ("stroke-dashoffset", VFloat 0.) ;
    ("marker-end", String "url(#arrowMarker)")
  ]) ;
  ("initArrow", [
    ("stroke", String "black")
  ]) ;
  ("thievesArrow", [
    ("stroke", Color Border)
  ]) ;
  ("piratesArrow", [
    ("stroke", Color Wave)
  ]) ;
  ("shadowArrow", [
    ("filter", String "url(#arrow-blur)") ;
    ("stroke", String "white") ;
    ("opacity", VFloat 0.6)
  ]) ;
  ("flag-bg-red", [
    ("stroke", String "black") ;
    ("stroke-width", VFloat 0.1) ;
    ("fill", Color Red)
  ]) ;
  ("flag-bg-green", [
    ("stroke", String "black") ;
    ("stroke-width", VFloat 0.1) ;
    ("fill", Color Green)
  ]) ;
  ("flag-bg-blue", [
    ("stroke", String "black") ;
    ("stroke-width", VFloat 0.1) ;
    ("fill", Color Blue)
  ]) ;
  ("circle-bg-pirates", [
    ("fill", Color Water)
  ]) ;
  ("circle-bg-thieves", [
    ("fill", Color Sand)
  ]) ;
  ("mainIsland", [
    ("stroke-width", Size (Multiple (3., WaveUnit))) ;
    ("stroke", Color Border) ;
    ("fill", Color Sand) ;
    ("fill-rule", String "evenodd")
  ]) ;
  ("wave-1-shadow", [
    ("stroke-width", Size (Multiple (2. *. 6. -. 3., WaveUnit))) ;
    ("stroke", Color Water) ;
    ("fill-opacity", VFloat 0.) ;
    ("fill-rule", String "evenodd")
  ]) ;
  ("wave-1", [
    ("stroke-width", Size (Multiple (2. *. 9. -. 3., WaveUnit))) ;
    ("stroke", Color Wave) ;
    ("fill-opacity", VFloat 0.) ;
    ("fill-rule", String "evenodd")
  ]) ;
  ("wave-2-shadow", [
    ("stroke-width", Size (Multiple (2. *. 12. -. 3., WaveUnit))) ;
    ("stroke", Color Water) ;
    ("fill-opacity", VFloat 0.) ;
    ("fill-rule", String "evenodd")
  ]) ;
  ("wave-2", [
    ("stroke-width", Size (Multiple (2. *. 14. -. 3., WaveUnit))) ;
    ("stroke", Color Wave) ;
    ("fill-opacity", VFloat 0.) ;
    ("fill-rule", String "evenodd")
  ]) ;
  ("wave-3-shadow", [
    ("stroke-width", Size (Multiple (2. *. 17. -. 3., WaveUnit))) ;
    ("stroke", Color Water) ;
    ("fill-opacity", VFloat 0.) ;
    ("fill-rule", String "evenodd")
  ]) ;
  ("wave-3", [
    ("stroke-width", Size (Multiple (2. *. 18. -. 3., WaveUnit))) ;
    ("stroke", Color Wave) ;
    ("fill-opacity", VFloat 0.) ;
    ("fill-rule", String "evenodd")
  ]) ;
  ("backgroundWater", [
    ("fill", Color Water)
  ]) ;
  ("fog", [
    ("fill", String "white") ;
    ("opacity", VFloat 0.204569)
  ])
]

let decl = string_of_style styles

end

(* Convert a class into its CSS name. *)
let class_to_string = function
  | Arrow -> "arrow"
  | InitArrow -> "initArrow"
  | ThievesArrow -> "thievesArrow"
  | PiratesArrow -> "piratesArrow"
  | Island -> "mainIsland"
  | Flag c -> Printf.sprintf "flag-bg-%s" (SpecialId.color_to_string c)
  | Wave i -> Printf.sprintf "wave-%d" i
  | BackgroundWater -> "backgroundWater"
  | Fog -> "fog"

(* Convert a list of CSS classes. *)
let convert_classes classes =
  List.map class_to_string classes

(* Convert a path into a SVG path declaration. *)
let path_to_string p =
  let point p = Printf.sprintf "%g,%g" p.Geometry.x p.Geometry.y in
  let command = function
    | MoveTo p -> Printf.sprintf "M %s" (point p)
    | ArcTo (curve, p) ->
      if curve = 0. then
        Printf.sprintf "L %s" (point p)
      else
        let radius = Float.abs (1. /. curve) in
        Printf.sprintf "A %g %g 0 0 %d %s" radius radius (if curve > 0. then 0 else 1) (point p)
    | Cycle -> "Z" in
  String.concat " " (List.map command p)

(* Convert an element. *)
let rec convert_element =
  let to_measure v = (v, None) in
  let%svg f = function
    | SpecialNode (coord, id, classes) ->
      let%svg node =
        "<use x = "(to_measure coord.x)" y = "(to_measure coord.y)"
          href = "("#" ^ id_special id)"
          class = "(convert_classes classes)" />" in
      node
    | Rect (coord, (width, height), classes) ->
      "<rect x = "(to_measure coord.x)" y = "(to_measure coord.y)"
        width = "(to_measure width)" height = "(to_measure height)"
        class = "(convert_classes classes)" />"
    | Path (path, classes) ->
      "<path d = "(path_to_string path)" class = "(convert_classes classes)" />"
    | G l -> "<g>"(List.map convert_element l)"</g>" in
  f

(* The path drawing the corresponding flag. *)
let path_flag x =
  let up = Sizes.flag /. 3.5 in
  let down = -.Sizes.flag /. 3.5 in
  let start_left = -.Sizes.flag /. 2. in
  let left = Sizes.flag /. 6. in
  let right = Sizes.flag /. 2. in
  let nb_zigzags = if x mod 2 = 0 then (1 + x / 2) else ((x + 1) / 2) in
  assert (nb_zigzags > 0) ;
  let middle_space = x mod 2 = 1 in
  let delta =
    let total = up -. down in
    let total = if middle_space then total *. 0.5 else total in
    total /. Float.of_int (2 * nb_zigzags) in
  let rec zigzags = function
    | 0 -> []
    | n ->
      let x = if n mod 2 = 1 then right else left in
      let dy = Float.of_int n *. delta in
      (x, dy) :: zigzags (n - 1) in
  let zigzags = zigzags nb_zigzags in
  let line_to (x, y) = ArcTo (0., { x ; y }) in
  MoveTo { x = start_left ; y = down }
  :: line_to (start_left, up)
  :: List.map (fun (x, dy) -> line_to (x, up -. dy)) (List.rev zigzags)
  @ List.map (fun (x, dy) -> line_to (x, down +. dy)) zigzags
  @ [Cycle]

(* Declarations associated to variables/flags. *)
let variable_declarations variables =
  let to_measure v = (v, None) in
  let all_values = function
    | Formula.Color -> Formula.[Red; Green; Blue]
    | Formula.Bool -> Formula.[Red; Green] in
  (* Drew all declarations for a specific variable. *)
  let for_var x t =
    let symbol_of_color = function
      | Formula.Red -> "rudder"
      | Formula.Green -> "parrot"
      | Formula.Blue -> "anchor" in
    let%svg flag c =
      "<g id = "(id_special (Flag (c, x)))">"
        [convert_element (Path (path_flag x, [Flag c]))]
        "<use href = "("#" ^ symbol_of_color c)"
          x = "(to_measure ((Sizes.symbol -. Sizes.flag) /. 2.2))" y = 0 />"
      "</g>" in
    let ret = List.map flag (all_values t) in
    let nb_values = List.length (all_values t) in
    let middle_value_index = (Float.of_int nb_values -. 1.) /. 2. in
    let flags =
      List.mapi (fun i v ->
        let%svg use =
          "<use href = "("#" ^ id_special (Flag (v, x)))"
            x = "(to_measure (Sizes.node *. 0.6))"
            y = "(to_measure ((Float.of_int i -. middle_value_index) *. Sizes.flag *. 0.7))" />" in
        use) (all_values t) in
    let%svg quantifier side =
      "<g id = "(id_special (Quantifier (side, x, t)))">"
        "<use href = "("#" ^ id_special (Node side))" x = 0 y = 0 />"
        flags
      "</g>" in
    let ret = ret @ List.map quantifier [true; false] in
    let condition side positive c =
      let id =
        id_special (Signal { side ; variable = x ; positive ; predicate = Formula.PColor c }) in
      let delta_flag = 0.2 in
      let bg_circle =
        ["circle-bg-" ^ if side then "pirates" else "thieves"] in
      let%svg signal =
        "<g id = "id">"
          "<circle cx = 0 cy = 0 r = "(to_measure (0.9 *. Sizes.signal /. 2.))"
            class = "bg_circle" />"
          "<use href = "("#" ^ id_special (Flag (c, x)))" x = "(to_measure delta_flag)" y = 0 />"
          "<use href = "("#" ^ if positive then "must" else "cant")" x = 0 y = 0 />"
          "<use href = "("#circle_" ^ if side then "blue" else "brown")" x = 0 y = 0 />"
          (* A clip on the right part of the flag to make it go through the signal. *)
          (* This syntax is now accepted in the main branch of TyXML, but the transform syntax
             isn't (see #328). This should be fixed in the next version of TyXML.
          "<use href = "("#" ^ id_special (Flag (c, x)))"
            x = "(to_measure delta_flag)" y = 0
            clip-path = 'url(#clip-flag)' />" *)
          Svg.[use ~a:[
            a_href ("#" ^ id_special (Flag (c, x))) ;
            a_x (to_measure delta_flag) ; a_y (to_measure 0.) ;
            Unsafe.string_attrib "clip-path" "url(#clip-flag)" ] []]
        "</g>" in
      signal in
    let ret =
      ret @ List.concat_map (fun side ->
              List.concat_map (fun positive ->
                  List.map (fun c -> condition side positive c) (all_values t))
                [true; false]) [true; false] in
    ret in
  IMap.fold (fun x t l ->
    for_var x t @ l) variables []

let to_image i =
  let to_measure v = (v, None) in
  let convert = List.map convert_element in
  (* Each arrow is associated a shadow as a <use> element. *)
  let (shadows, arrows) =
   List.split (List.map (fun arrow ->
     let id = Id.get () in
     let (classes, arrow) =
       (* This is a hack: for SVG's CSS to work, the classes need to be taken outside
         the actual path. *)
       match arrow with
       | Path (path, classes) ->
         (List.filter (fun c -> c <> Arrow) classes, Path (path, [Arrow]))
       | _ -> ([], arrow) in
     let%svg arrow =
       "<g class = "(convert_classes classes)"><g id = "id">"[convert_element arrow]"</g></g>" in
     let%svg shadow = "<use href = "("#" ^ id)" class = 'shadowArrow' />" in
     (shadow, arrow)) i.arrows) in
  let background =
    (* One could use the feMorphology dilate filters to implement waves, but these tend to
      produce unwanted artifacts. Instead, we clone the island with different stroke-widths.
      For that, we need to hack trough the generated paths. *)
    let id = "island" in
    List.concat_map (function
      | Path (path, tags) as e ->
        if List.mem Island tags then (
          let%svg island =
            "<g class = "(convert_classes [Island])"><g id = "id">"
            [convert_element (Path (path, List.rem Island tags))]"</g></g>" in
          [island]
        ) else (
          match List.find_map (function Wave n -> Some n | _ -> None) tags with
          | Some n ->
            let%svg wave = "<use href = "("#" ^ id)" class = "(convert_classes tags)" />" in
            let tags' = List.filter (function Wave _ -> false | _ -> true) tags in
            let tags' = convert_classes tags' in
            let tags' = (class_to_string (Wave n) ^ "-shadow") :: tags' in
            let%svg wave_shadow =
              "<use href = "("#" ^ id)" class = "tags'" />" in
            [wave; wave_shadow]
          | None -> [convert_element e]
        )
      | e -> [convert_element e]) i.background in
  let (_, (arrow_width, arrow_height)) = Data.arrow in
  let%svg img =
    "<svg width = '100%' height = '100%' viewBox = "i.viewbox">"
      "<style>"
        (Svg.txt Style.decl)
      "</style>"
      "<defs>"
        Data.all
        "<marker id = 'arrowMarker' orient = 'auto' markerUnits = 'strokeWidth'
           refX = "(to_measure (arrow_width /. 2.))" refY = "(to_measure (arrow_height /. 2.))"
           markerWidth = "(to_measure arrow_width)" markerHeight = "(to_measure arrow_height)"
           viewBox = "(0., 0., arrow_width, arrow_height)">"
          "<use href = '#arrow' class = 'marker' fill = 'context-stroke'
                x = "(to_measure (arrow_width /. 2.))" y = "(to_measure (arrow_height /. 2.))" />"
        "</marker>"
        "<filter id = 'arrow-blur'>"
          "<feGaussianBlur stdDeviation = 1 />"
        "</filter>"
        "<clipPath id = 'clip-flag'>"
          "<rect x = "(to_measure (Sizes.flag *. 0.35))" y = "(to_measure (-.Sizes.flag /. 2.))"
            width = "(to_measure Sizes.flag)" height = "(to_measure Sizes.flag)" />"
        "</clipPath>"
        (variable_declarations i.variables)
      "</defs>"
      "<g id = 'background'>"
        background
      "</g>"
      "<g id = 'shadows'>"
        shadows
      "</g>"
      "<g id = 'arrows'>"
        arrows
      "</g>"
      "<g id = 'signals'>"
        (convert i.signals)
      "</g>"
      "<g id = 'nodes'>"
        (convert i.nodes)
      "</g>"
      "<g id = 'foreground'>"
        (convert i.foreground)
      "</g>"
    "</svg>" in
  img

end

