(* Build the file svg_defs.ml from the content of the images/ folder. *)

let image_folder = "images"

let regexp_newline = Str.regexp_string "\n"
let regexp_name = Str.regexp {|\([^.]*\).\([^.]*\).svg|}
let regexp_forbidden_names = Str.regexp {|\(all\|image\)|}
let regexp_inner_svg =
  Str.regexp {|.*<svg[^>]*viewBox[ \t]*=[ \t]*["']\([^"']*\)["'][^>]*>\(.*\)</svg>.*|}
let regexp_four_floats =
  let floatr = {|\([0-9eE.-]+\)|} in
  Str.regexp (Printf.sprintf {| *%s +%s +%s +%s *|} floatr floatr floatr floatr)
let regexp_id = Str.regexp {|\(id[ \t]*=[ \t]*["']\|href[ \t]*=[ \t]*["']#\|url[ \t]*["']#\)|}
let regexp_spaces = Str.regexp {|>[ \t]*<|}

let all_images =
  let all_files = Array.to_list (Sys.readdir image_folder) in
  List.filter_map (fun file ->
    if Str.string_match regexp_name file 0 then (
      let kind = Str.matched_group 2 file in
      let file = Str.matched_group 1 file in
      if Str.string_match regexp_forbidden_names file 0 then
        Printf.eprintf "Warning: unexpected file name %s.%s.svg that could shadow identifier.\n" file kind ;
      Some (file, kind)
    ) else (
      Printf.eprintf "Warning: file %s not an svg file, or missing kind.\n" file ;
      None
    )) all_files

(* Read the file content and returns a modified version of it, as well as its viewbox.
  In particular, identifiers are forced to be local. *)
let process_file file kind =
  let content =
    In_channel.input_all
      (In_channel.open_text
        (Printf.sprintf "%s/%s.%s.svg" image_folder file kind)) in
  (* Remove any newline, to simplify the treatment by regular expressions. *)
  let content =
    Str.global_replace regexp_newline " " content in
  let content =
    Str.global_replace regexp_spaces "><" content in
  let (viewbox, inner) =
    if Str.string_match regexp_inner_svg content 0 then
      (Str.matched_group 1 content, Str.matched_group 2 content)
    else (
      Printf.eprintf "Warning: file %s.svg doesn't look like an svg file: %s\n" file content ;
      ("0 0 1 1", "<!-- Error during generation. -->")
    ) in
  let (minx, miny, width, height) =
    if Str.string_match regexp_four_floats viewbox 0 then
      let parse i =
        float_of_string (Str.matched_group i viewbox) in
      (parse 1, parse 2, parse 3, parse 4)
    else (
      Printf.eprintf "Warning: unrecognised viewBox in file %s.svg: %s\n" file viewbox ;
      (0., 0., 1., 1.)
    ) in
  (* Prefix ids by the file name, to avoid collisions. *)
  let inner =
    Str.global_replace regexp_id (Printf.sprintf {|\1%s-|} file) inner in
  let (shiftx, shifty) =
    (-.width /. 2. -. minx, -.height /. 2. -. miny) in
  let wrap =
    let transform =
      Printf.sprintf {|[`Scale(Sizes.%s /. %g, None); `Translate(%g, Some (%g))]|}
        kind width shiftx shifty in
    Printf.sprintf
      {|<g id="%s" transform=|%s}%s{%s|><title>%s</title>%s</g>|}
      file file transform file file inner in
  (wrap, (width, height))

(* Print a declaration of the form let%svg file = {|file content|}. *)
let print_file (file, kind) =
  Printf.printf "let %s =\n" file ;
  Printf.printf "\tlet%%svg %s = {%s|\n" file file ;
  let (content, (width, height)) = process_file file kind in
  print_endline content ;
  Printf.printf "\t|%s} in" file ;
  Printf.printf "\t(%s, (Sizes.%s, %f *. Sizes.%s /. %f))\n" file kind height kind width

let () =
  print_endline {|[@@@warning "-32"]|} ;
  print_endline {|module Data (Xml : Xml_sigs.NoWrap) (Svg : Svg_sigs.Make(Xml).T) = struct|} ;
  print_endline {|type image = Svg_types.g Svg.elt * (float * float)|} ;
  List.iter print_file all_images ;
  Printf.printf "let%%svg all = [%s]\n"
    (String.concat "; " (List.map (fun (name, _) -> "fst " ^ name) all_images)) ;
  print_endline {|end|}

