
open Logique_pirate

let _ = Random.self_init ()

(* Output file, or "-" for stdout. *)
let output = ref "-"

(* Given a string meant to be displayed for human, convert it into a name suitable for a
  usage on the terminal. *)
let terminal_name n =
  let char_list s = List.init (String.length s) (fun i -> s.[i]) in
  let n =
    String.concat "" (List.map (function
        | ' ' -> "-"
        | c when (c >= 'a' && c <= 'z')
              || (c >= 'A' && c <= 'Z')
              || (c >= '0' && c <= '9')
              || List.mem c ['-'; '_'; '+'; '/'; '.']
          -> String.make 1 c
        | '<' -> "lt"
        | '>' -> "gt"
        | _ -> ""
      ) (char_list n)) in
  Str.global_replace (Str.regexp "-+") "-" n

(* Triples of the form Options.t * string (* terminal name *) * bool ref (* chosen value *)
  for the different options of the Options module. *)
let options_ref =
  List.map (fun o ->
    let r = ref (Options.default o) in
    let n = terminal_name (Options.name o) in
    (o, n, r)) Options.all

(* Same than options_ref, but for Position's settings. *)
let settings_ref =
  List.map (fun (o, default) ->
    let r = ref default in
    let n = terminal_name o in
    (o, n, r)) Position.default_settings

(* Whether the output should be TikZ instead of SVG. *)
let tikz = ref false

let base_arguments = [
  ("-o", Arg.String (fun str -> output := str), "Output file (default to standard output)") ;
  ("--tikz", Arg.Set tikz, "Output a TikZ file instead of an SVG file (beta)") ;
  ("--determinist", Arg.Unit (fun () -> Random.init 0), "Fixing the seed to have reproductible results")
]

let arguments =
  base_arguments
  @ List.map (fun (o, n, r) ->
      if Options.default o then
        ("--no-" ^ n, Arg.Clear r, "Disable option " ^ Options.name o)
      else ("--" ^ n, Arg.Set r, "Enable option " ^ Options.name o)) options_ref
  @ List.map (fun (o, n, r) ->
      ("--" ^ n, Arg.Set_float r, "Set the weigth of " ^ o)) settings_ref

let () =
  let usage = "Available options:" in
  Arg.parse arguments
    (fun str -> Printf.eprintf "Don't know what to do with `%s'.\n" str)
    usage

let () =
  let lexbuf = Lexing.from_channel stdin in
  let out =
    if !output = "-" then stdout
    else open_out !output in
  let options =
    List.filter_map (fun (o, _n, r) -> if !r then Some o else None) options_ref in
  let settings =
    List.map (fun (o, _n, r) -> (o, !r)) settings_ref in
  let img = Options.apply ~position:settings options lexbuf in
  if !tikz then (
    let tikz = Image.to_tikz img in
    output_string out tikz
  ) else (
    let module Svg_fix = struct
      include Tyxml.Svg
      let a_href = a_xlink_href (* A fix to make the resulting files openable with Inkscape. *)
      end in
    let module Convert = Image.Convert(Tyxml_xml)(Svg_fix) in
    let img = Convert.to_image img in
    let fmt = Format.formatter_of_out_channel out in
    Format.fprintf fmt "%a@." (Tyxml.Svg.pp ~indent:true ()) img
  ) ;
  close_out out

