% Un jeu de plateau pour comprendre la dualité en logique
% Emmanuel Beffara; Martin Bodin
% Didapro, Louvain-la-Neuve, 1er février 2024

---
institute: LIG, Inria, Université Grenoble Alpes
theme: metropolis
mainfont: Fira Sans
aspectratio: 169
colortheme: owl
---

# Jeu

## Mise en situation

![](images/simple.pdf)

# Présentation de l'approche

## Lien avec la logique

### Question centrale

*Est-ce qu'une équipe peut être sure de gagner ?*

### Détermination du gagnant

- Une équipe gagne depuis une case de sa couleur ssi elle a un coup qui mène
	à une position gagnante.
- Une équipe gagne depuis une case de l'autre couleur ssi tout coup de
	l'adversaire mène à une position gagnante.

### Structure logique

- La condition de gain dans une position est un énoncé de logique
	propositionnelle.
- La condition pour une équipe est la négation de celle pour l'autre.

## Objectifs de l'activité

### Point de vue logique

- Signification des connecteurs logiques
- Dualités et / ou, pour tout / il existe
- Détermination de la véracité d'un énoncé
- Notion de démonstration comme stratégie d'argumentation

### Point de vue informatique

- Calcul de valeurs de vérité
- Verbalisation de stratégies
- Algorithmes de décision
- Recherche exhaustive
- Calcul de stratégie gagnante

## Démarche de conception de la situation

### Mise en avant de la symétrie

- Il n'y a pas un *gentil* et un *méchant*, le scénario est neutre a priori
- Les règles du jeu sont communes, la formulation logique est fonction du
	point de vue adopté

### Possibilités d'appropriation

- Écrire sur la carte pour l'analyser
- Raisonner sur des variations autour d'une carte

## Retour d'expérience

Fête de la science 2023 (élèves de Seconde)

- Différentes heuristiques
- Différents questionnements exprimés
	- propriétés logiques
	- notion de stratégie
- Appropriation des cartes: annotation, réécriture

## Heuristique: nombres de flèches

![](images/heuristique.jpg)

## Méthode: annotation des positions

![](images/positions.jpg)

## Méthode: réécriture de chemins

![](images/reecriture.jpg)

# Quantificateurs

## Règles additionnelles

![](images/drapeaux.pdf){height=4cm}

<!-- - buveur -->
<!-- - pierre-feuille-ciseaux, plusieurs en même temps -->
<!-- - SAT -->
<!-- - carte avec vides -->

# Discussion

