% Un jeu de plateau pour comprendre la dualité en logique
% Emmanuel Beffara; Martin Bodin

---
lang: fr-FR
bibliography: biblio.yaml
citation-style: apa.csl
---

------------------------- ------------------------------------------------------
Nombre de participant·e·s  8
maximal pour l'atelier                   

Matériel nécessaire
(vidéo-projection, grande  Vidéo-projection, deux grandes tables
salle, autre...)   
------------------------- ------------------------------------------------------

::: {custom-style=auteurs}
Emmanuel Beffara, Martin Bodin
:::

::: {custom-style=iartem-institution}
Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, 38000 Grenoble, France
:::

::: {custom-style=adjectif-volume}
Vol. XXX / TXXX
:::

::: {custom-style=logo}
![](images/adjectif.png)
:::

::: {custom-style=InfAbstract}
**RÉSUMÉ** <!--(10 lignes)-->

Cet atelier présente une activité sous forme de jeu de plateau à deux joueurs destinée à construire une représentation intuitive
des concepts d'opérateurs logiques, de quantificateurs, et des règles de raisonnement associées.
La conception met en avant la dualité entre opérateurs et la symétrie entre preuve et réfutation,
dans un décor inspiré de l'imaginaire pirate où des cartes au trésor scénarisent des formules logiques.
Un jeu de base correspond à la logique propositionnelle et une extension ajoute des quantificateurs et prédicats.
On présentera des retours d'expérience issus d'une utilisation du dispositif dans un contexte de médiation auprès d'élèves de lycée.

**MOTS-CLÉS**
logique,
dualité,
médiation,
jeu

<!--
(mettre ici une image (800x400 pixels) qui illustre la ou les ressource
/ activité / projet)

Légende image () : mettre votre texte
-->
:::

![Un exemple simple de carte au trésor utilisée dans l'activité.](images/simple.png){ #fig:carte:simple width=60% }

# Objectifs

<!-- ## Objectif -->
<!-- 20 lignes max. -->

Cette activité est consacrée à l'apprentissage de premières notions de logique pour des élèves de collège ou lycée.
Elle vise deux objectifs pédagogiques :
d'une part, construire une représentation intuitive des opérateurs et des quantificateurs logiques,
d'autre part, faire identifier des règles de manipulation et réécriture simples de formules logiques.

Une importance particulière est donnée à la symétrie entre preuve et réfutation et entre vérité et fausseté, tant dans la manipulation formelle des énoncés que dans les structures de raisonnement associées.
Cette dualité est à la fois au fondement du calcul booléen et au cœur de la correspondance entre preuves et programmes.
Elle est particulièrement adaptée à la présentation sous forme de jeu, déjà largement exploitée dans le domaine de la logique formelle.

# Présentation du dispositif

Le dispositif est présenté comme un jeu de plateau.
Les élèves reçoivent des cartes plastifiées représentant des cartes au trésor (voir [@fig:carte:simple])
sur lesquelles iels vont jouer.
Il y a deux équipes : les pirates et les brigands.
Le jeu consiste à déplacer un pion commun aux deux équipes jusqu'à atteindre un trésor.
Il y a deux trésors : l'un fait gagner les pirates (trésor de mer), et l'autre les brigands (trésor de terre).
Ce sont les pirates qui décident du déplacement sur les cases de mer,
et les brigands sur les cases de terre.
Une flèche noire indique la case de départ.
Ainsi, sur la figure d'exemple la case de départ (tout à gauche) est dans les terres :
ce sont donc les brigands qui commencent.

Pour la mise en situation, on explique que les deux clans, pirates et brigands, se sont associés par nécessité bien qu'ils poursuivent des objectifs antagonistes.
En effet, chacun a une compétence qui fait défaut à l'autre : se repérer sur la mer ou sur la terre.
Pourtant, un seul trésor sera remporté, et par un seul des deux clans.

Par exemple, dans la [@fig:carte:simple], les brigands commencent car la case de départ (à gauche) est dans les terres.
S'ils choisissent le chemin du nord, le groupe arrive sur une case de mer : c'est aux pirates de jouer.
Si les pirates choisissent le chemin à l'est, le groupe continue sur une case de mer : c'est encore aux pirates de jouer[^doubleJeu].
Ils choisissent alors de continuer à l'est où se trouve le trésor de la mer :
les pirates le trouvent immédiatement, laissant les brigands seuls sur leur île.
Ici, les pirates ont gagné, mais on peut se demander si les équipes ont joué au mieux[^stratégieGagnante].

[^doubleJeu]: C'est la case qui détermine qui joue, et il arrive que ça soit deux fois de suite la même équipe.

[^stratégieGagnante]: En l'occurrence non : il y avait une stratégie gagnante pour les brigands.

Les cartes sont plastifiées afin de permettre aux intervenants de noter au feutre leurs stratégies, remarques, etc.
Au cours de l'activité, différentes cartes se succèdent afin d'illustrer différents aspects du jeu.
On peut aussi montrer des cartes similaires (par exemple ne différant que par un chemin) et commenter l'effet de leurs différences sur le jeu.

![Un morceau de carte du jeu étendu avec des choix de drapeaux.](images/drapeaux.png){ #fig:drapeaux width=50% }

Une extension du jeu peut être abordée une fois le jeu de base bien compris.
Cette extension utilise des cartes plus complexes faisant intervenir des drapeaux.
Il y a différentes formes de drapeaux, chacune existant en trois variantes de couleur (vert, gris, et violet, avec un symbole différent par couleur).
Certaines cases imposent aux joueurs de choisir la couleur d'un drapeau d'une forme donnée, en l'entourant au feutre sur la carte.
C'est à l'équipe associée à l'élément de la case de choisir le drapeau.
Par exemple la case [@fig:drapeaux] demande aux brigands de choisir une des trois variantes du drapeau triangulaire.

Le choix d'un drapeau est définitif sur la partie (on le garde jusqu'à arriver à un trésor), et est commun au groupe :
même si ce sont les brigands qui choisissent le drapeau, ce choix conditionne aussi les mouvements pirates.
Il est possible d'avoir plusieurs drapeaux, mais ils seront alors de formes différentes[^formesDifférentes].
La possession d'un drapeau va bloquer ou débloquer des chemins.
Par exemple @fig:drapeaux, le chemin horizontal a un panneau rond bleu imposant la possession du drapeau triangulaire vert pour pouvoir l'emprunter[^débloquer].
Si plusieurs panneaux sont indiqués sur un même chemin, il faut satisfaire toutes les conditions pour pouvoir l'emprunter.

[^formesDifférentes]: Il n'existe pas de carte qui va demander de choisir à nouveau un drapeau d'une forme déjà choisie.
	De plus, les contraintes de chemin sur les couleurs de drapeaux ne va porter que sur des drapeaux déjà choisis.

[^débloquer]: Par contre, la possession du bon drapeau n'impose pas de prendre ce chemin.

# Fondement théorique

<!-- (quelques paragraphes avec renvoi via des URL vers les articles existants détaillant ces aspects) -->

Cette activité est directement basée sur la sémantique dialogique de @lorenzen-1978-dialogische et ses développements ultérieurs, par exemple @rahman-2005-how.
Étant donné le point de vue d'une équipe, chaque case d'une carte représente une formule logique.
Une stratégie gagnante à partir d'une position du jeu représente une preuve de la formule associée, et réciproquement une preuve permet de construire une stratégie gagnante.
Ainsi, du point de vue des pirates :

- le trésor de mer représente la formule « Vrai » notée $\top$ (les pirates y gagnent immédiatement),
	et le trésor de terre (brigand) la formule « Faux » notée $\bot$ ;
- une case de mer représente un « ou » logique ${}\lor{}$ :
	les pirates contrôlent le déplacement,
	il suffit donc qu'il y ait une case d'arrivée où les pirates gagnent pour gagner ;
- une case de terre représente un « et » logique ${}\land{}$ :
	les pirates ne sont sûrs d'y gagner que si chaque case d'arrivée est gagnante pour eux.

Les règles sont bien sûr symétriques, et une même carte représente donc deux formules distinctes
du point de vue des brigands et des pirates, l'une étant la négation de l'autre.
Ainsi la carte [@fig:carte:simple] représente la formule
$(\bot \lor (\bot \lor \top)) \land (\bot \lor (\bot \land \top))$
du point de vue des pirates,
et la formule
$(\top \land (\top \land \bot)) \lor (\top \land (\top \lor \bot))$
du point de vue des brigands.
Une seule de ces formules est prouvable :
il n'y a une stratégie gagnante que pour une seule des deux équipes.

<!-- Chaque carte représente ainsi une formule de la logique propositionnelle. -->
<!-- Elles permettent d'illustrer des concepts comme la distributivité de ${}\land{}$ sur ${}\lor{}$, le tiers exclu, etc. -->

Dans l'extension avec les drapeaux, les équipes devront parfois faire des choix qui conditionneront les mouvements ultérieurs, ce qui correspond à la notion de quantificateur.
Chaque forme de drapeau représente une variable et chaque couleur de drapeau représente une valeur possible.
Selon cette métaphore, les cases où l'on choisit des drapeaux représentent des quantificateurs :
si c'est l'adversaire qui choisit le drapeau, c'est un quantificateur universel $\forall$
(il faut être capable de gagner quel que soit le choix de l'adversaire),
si c'est notre équipe qui le choisit, c'est un quantificateur existentiel $\exists$
(il suffit de trouver une valeur pour cette variable qui nous fait gagner).
Les conditions sur les chemins sont alors des tests sur les valeurs des variables, c'est-à-dire des prédicats.

# Lien avec la recherche, choix de conception

L'importance de travailler explicitement les structures logiques, et en particulier la quantification, est attestée par plusieurs travaux [@durand-guerrier-2012-examining].
Par ailleurs, la sémantique dialogique a été effectivement utilisée dans un but didactique pour concevoir et analyser des situations d'argumentation mathématique [@barrier-2019-analyse].

La sémantique dialogique est traditionnellement présentée avec des camps différenciés : il y a le défenseur et l'adversaire, voire des représentations connotées avec un « gentil » et un « méchant ».
Nous avons fait le choix de rendre le jeu complètement symétrique entre les pirates et brigands.
La carte va bien sûr « dicter » un gagnant, mais ce dernier ne sera pas forcément évident.
Dans nos expérimentations, nous avons d'ailleurs pu constater que la prédiction de l'équipe gagnante par les élèves est souvent erronée sur certaines cartes.

La représentation visuelle des quantificateurs comme des cases associées à une équipe avec un choix de drapeaux
tend à illustrer que les quantificateurs universels se comportent similairement au « et » logique,
et réciproquement que les quantificateurs existentiels peuvent être exprimés par des « ou » logiques.
Ce sont des principes que l'on va retrouver en élimination des quantificateurs,
et ils peuvent aider à se former une intuition plus forte de la logique.

Les formes des îles sont purement décoratives,
mais parfois renforcent visuellement des symétries pour aider les raisonnements
ou aider à la reconnaissance d'une formule.

# Retour d'expérience
<!-- Expérimentations réalisées ou envisageables (public, descriptif ressource) -->

Les premiers tests, destinés à ajuster différents choix de conception, ont été menés à très petite échelle, avec des participant·es plus ou moins au fait des enjeux théoriques et de la pratique de la médiation scientifique.

Une expérimentation en « conditions réelles » a eu lieu lors de la Fête de la Science.
Nous avions des groupes de 4 élèves de seconde, tournant toutes les demi-heures avec d'autres activités.
Les retours étaient assez positifs.
L'atelier donnait par contre la sensation d'être rapide et de laisser relativement peu de temps aux élèves pour se familiariser avec l'atelier.

Notre discours a été assez variable suivant les groupes :
en fonction de ce que proposaient les élèves,
nous avons plutôt insisté sur la notion de stratégie gagnante
(qui n'était pas toujours facile à appréhender)
ou sur des propriétés logiques (notamment le tiers exclu).
Il nous est aussi arrivé de prendre une approche algorithmique en demandant comment déterminer automatiquement le vainqueur.
Il existe plusieurs approches pour cela :
on peut partir de la fin en marquant chaque case comme gagnante pour une équipe ou l'autre en fonction des cases qui la suivent,
ou on peut modifier la carte de proche en proche jusqu'à arriver à une carte triviale.
Cette approche par réécriture a été appréciée, et elle induit des règles de réécriture de formules logiques.
Nous pensons donc que cet atelier est adaptable dans plusieurs contextes.

La carte induit naturellement une équipe gagnante :
si une équipe gagne sur une carte, ce n'est pas parce que cette équipe joue mieux que l'autre,
mais parce que la carte l'induit.
Les réactions à cela ont été variées.
Certains ont été frustrés, notamment s'iels tombaient sur plusieurs cartes perdantes à la suite.
Nous avions autant de cartes faisant gagner pirates et brigands,
mais le hasard a fait que certains perdaient plusieurs fois de suite.
C'est important à considérer car cela les a fait se décourager par la suite,
indiquant que forcément la carte était perdante pour leur équipe, même lorsque ce n'était pas le cas.
D'autres groupes ont réagi en proposant des heuristiques pour prédire qui gagnera la partie,
par exemple en comptant le nombre de chemins de chaque couleur.

# Liens vers les ressources
<!-- et point de contact  -->

<!--
-   Liens vers la/les ressources ou le projet

-   Point de contact pour avoir plus de renseignements
-->

Les ressources du projet se trouvent toutes dans le répertoire <https://gitlab.inria.fr/mbodin1/logique-jeu>.
En particulier le sous-dossier [`proto/pirate/formules`](https://gitlab.inria.fr/mbodin1/logique-jeu/-/tree/didapro24/proto/pirate/formules)
contient toutes les cartes conçues pour l'atelier.
La page <https://mbodin1.gitlabpages.inria.fr/logique-jeu> propose une interface pour générer d'autres cartes.

# Références

::: {#refs custom-style="Bibliographie 1"}
:::

