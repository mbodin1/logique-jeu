#!/bin/bash

set -ex

outpng="rendus/png/"
outpdf="rendus/pdf/"

mkdir -p "$outpng"
mkdir -p "$outpdf"

for img in `ls *.svg`
do

	echo $img

	width=$(identify ${img} | sed 's/.* \([0-9]\+\)\+x.*/\1/g')
	height=$(identify ${img} | sed 's/.*x\([0-9]\+\)\+ .*/\1/g')

	# echo "$img: $width × $height"

	png="$outpng${img%.*}.png"
	pdf="$outpdf${img%.*}.pdf"

	# inkscape --batch-process --export-type=pdf --export-filename="$pdf" --export-dpi=300 $img # --export-width=2480

	wlen=$(expr ${width} \* 2)
	if [[ "$img" = "drapeaux.svg" ]]
	then
		wlen=$(expr ${width} \* 2)
	fi

	inkscape --batch-process --export-type=png --export-filename="$png" --export-background-opacity=0 --export-width=$wlen $img # --export-margin=20

	argrotate=""
	if [[ $width -gt $height ]]
	then
		argrotate="-rotate 90"
	fi
	# convert "$png" -gravity center -background transparent $argrotate -units pixelsperinch -density 300 -page a4 "$pdf" # -border 10 -auto-orient -resize '95%' -scale 96x96 -bordercolor white -border 20
	convert "$png" -background transparent $argrotate -bordercolor white -border 100 -density 300 -page a4 "$pdf"

done

